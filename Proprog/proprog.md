# Group members
* Andreas Wang
* Martin Langslet Lillesl�tten
* Sondre Fjeldheim Svarverud
 
# Discussion 
The majority of what we mention in this file is already mentioned in the �Development Process� chapter of our thesis so we recommend checking that chapter out if additional details are necessary. 
 
## Strengths and weaknesses of languages used in the project
We used C# for this project as it is the closest language syntax wise to C++ which is supported by Unity. 

### Strengths:

Coroutines are very useful for games in particular. They can be used to create individual update loops making it possible to decouple code from Unity�s Update() callback. 
 
C# does not support multiple inheritance, but interfaces are a decent alternative. It is possible to check if game objects have an interface and call the appropriate functions within the interface through the use of gameObject.GetComponent<InterfaceName>(). 
 
Unity/C# specific: it is easy to find solutions and tutorials for different problems. These are not necessarily the best examples of good code, as the bar of entry in Unity is low, meaning the code examples are probably written by non-professionals, but offers a rough idea of how things can be solved. 
 
The syntax is pretty similar to C++ so moving from C++ to C# is fairly easy. One of the primary things to worry about for C++ developers moving to C# is to remember putting the access keyword in front of all variables and class methods. 
 
### Weaknesses:

A more Unity specific issue rather than C# specifically, but there is no multithreading control in Unity. One might think that coroutines would work like multithreaded code, but these are only popped from a callstack per update rather than actually being multithreaded. 
 
Memory management is automatically handled for you which might be an issue if you really need control it. At the same time though it is nice to not worry about it as garbage collection is automatically handled in a similar way to how std::shared_ptr works in C++. 
 
## How we controlled the process and communication during development
We used Discord primarily for group communication. This includes communication through chat for general discussions and questions as well as using the voice chat for daily scrum meetings. Planning, review and retrospectives were handled in person at campus.
 
In terms of controlling the process we used [Jira](https://dev.imt.hig.no/tracker/projects/DOCKL/summary) with [Confluence](https://dev.imt.hig.no/confluence/display/2017DL/2017%3A+Dockit+League) as our hub for meeting notes. Both of them were used for our bi-weekly Scrum. We mostly used planning poker, the issue backlog and sprint handling functionalities within the Jira toolbox. 
 
## Use of version control systems, issue tracking and branching
We used Git with BitBucket for version control. The BitBucket repository is integrated with Jira by using smart commits with issue numbers. This allows us to track commits and issue progress directly within Jira.
 
In terms of branching we used a master and development branch. Each issue was given its own feature branch as well. Whenever an issue was finished we used pull requests for merging the feature branch back into our development branch. The feature branch was then closed after the merge. The pull request approach allowed all group members to review the cumulative changes made within the issue to get a better overview of all the changes. 
 
## Link to programming style guide including how to comment code.
The programming style we worked with was the [�One True Brace Style�](http://www.catb.org/jargon/html/I/indent-style.html), a variant of K&R (Kernighan & Ritchie uses this format in their examples in �The C Programming Language�) where the main difference is that brackets are not omitted for single statement control statements, and also used on functions.
 
In terms of code commenting we used [XML style commenting](https://msdn.microsoft.com/en-us/library/b2s063f7(v=vs.100).aspx), allowing the documentation to be generated by doxygen, enforced for non-trivial functions and encouraged for classes. In terms of commenting outside of this we primarily used single line comments above relevant blocks of code. 
 
## Use of libraries and integration with libraries
We did not use any direct integration with libraries in the project, but we will use this section to briefly discuss how we used two of Unity�s sample projects as a base example for lobby networking. 
 
We used a [sample project](https://www.assetstore.unity3d.com/en/#!/content/41836) (provided by Unity on the Asset Store) early on during development for prototyping a lobby for the game. We mention this in Section 8.2 of the thesis, but to reiterate: this project was horribly documented and Unity�s own lobby components had similar issues with documentation quality. There were a lot of grammatical errors in the documentation and a lack of overarching explanation of how the lobby components worked together. Regardless of this we managed to put together a initial lobby to work with.  
 
The second sample project we used was a fully implemented game called [�Tanks�](https://unity3d.com/learn/tutorials/projects/tanks-tutorial) which was provided by Unity. This code was well documented in comparison to the first, but did not use any of Unity�s own lobby components. We ended up basing our final lobby on this project, extracting the functionality and modifying it for our use. This is due to the project being better documented although we find it somewhat ironic that Unity�s best sample project didn�t even use their own lobby components. 
 
## Professionalism in our approach to software development
To sum up the professionalism in our approach to software development:

* We are using the atlassian toolset for project management, source code hosting and meeting documentation. These are integrated with each other. 
* BitBucket and Jira specifically are integrated through the use of smart commits.
* Feature implementation is done through feature branches and pull requests. 
* We use an enforced coding style �One True Brace Style� and XML style function comments which allows for doxygen generated documentation. 
* Reusing code from sample projects, like we did, is a normal way of reducing resources spent, as it is faster than creating everything from scratch. Depending on the source, it might already be used and tested, reducing risks of failures.
* In professional development it's very important to follow the project standard. The coding formatting for instance, as this seems to be a dividing issue with many different personal preferences. This wasn't much of an issue for us, as we agreed on the standard early on, but being professional is putting your personal preference aside for the common standard. Working with the same setup as everyone else may also ease the workflow, as sharing content will work on every workstation.

One thing we did not mention earlier on in this file is that we had an automated workflow for building the game. This is something we handled using Unity Cloud Build, integrated with BitBucket using SSH keys. This allows Unity hosted servers to automatically build specified branches for all specified platforms whenever the branch changes. Unity Cloud Build automatically sends an email to all group members with a shareable link to the built binaries whenever finished and is also capable of running specific unit tests within the code if wanted. The shareable links in particular are useful for deploying builds of the game for others to download. 
 
## Use of code review
We did not have any fixed times for code reviews, although we generally spent some time during our sprint review meetings to look at and evaluate each other's code. We used this time to make sure that the code formatting was consistently within the standard for all group members and that general code quality was sufficient. We also spent time on group member questions during these sessions as well as discussions around different implementation approaches to the issues we had worked on. 
 
 
## Personal reflections about professionalism in programming. 
### Andreas
I will look at some lines of code from various iterations of the [IngameMenuHandler.cs script](https://bitbucket.org/Knitram/dockitleague/src/18efec7d4e1f86bfa516ec5ef3aa37484f2bace9/Assets/Scripts/UI/IngameMenuHandler.cs?at=master&fileviewer=file-view-default) and evaluate these. 
 
The script itself has a couple of long lines due to having to dig rather deep to acquire the necessary data, but In terms of bad code I would like to highlight line 64 due to its ridiculous length. This line in particular is used to update the index to the array we acquire shop item descriptions from. While the contents of the line is not bad by itself, the issue is that the line is far too long as it combines incrementing/decrementing the line as well as range limiting the updated value. To refactor this line I would probably have preferred to separate the range limiting from the logic of the line. I would suggest solving this using a C# property for the index in this manner:

```csharp
private int shopItemDescriptionId = 0;
private int CurrentlyHighlightedId {
        get { return shopItemDescriptionId; }
        set { shopItemDescriptionId = value.LimitToRange(0, currentlySelectedShopItem.itemData.dockingKitDescriptions.Count - 1); }
}
``` 

The actual line of logic then only needs to look like this as the property handles the range limiting whenever the variable changes:

```csharp
CurrentlyHighlightedId = (CurrentlyHighlightedId + (Input.GetButtonDown("Ability0") ? -1 : 1));
``` 

While the property is private in this case, using properties for validation when updating public properties can be particularly useful as the validation code can be written within the �set� part of the property. 
 
The other piece of code I will look at is from an older version of the script and how the refactored version is better in terms of code quality. This is a function that updates the text boxes of the shop menu with a given name, additional stats and a description:

```csharp 
private void SetSelectedStrings(string name, string stats, string description) {
        shopDescriptionsContainer.transform.GetChild(2).GetComponent<Text>().text = name;
        shopDescriptionsContainer.transform.GetChild(3).GetComponent<Text>().text = stats;
        shopDescriptionsContainer.transform.GetChild(4).GetComponent<Text>().text = description;
}
```

As seen the index of the acquired child was originally hard coded which decreases the readability of the code as it is hard to tell which exact object within the hierarchy we are accessing. I later improved the readability of the function through the use of int casted enums as seen here: 

```csharp
enum ShopTextBoxTypes {
    Icons = 1,
    Name,
    Stats,
    Description
}
 
private void SetSelectedStrings(string name, string stats, string description) {
        shopDescriptionsContainer.transform.GetChild((int)ShopTextBoxTypes.Name).GetComponent<Text>().text = name;
        shopDescriptionsContainer.transform.GetChild((int)ShopTextBoxTypes.Stats).GetComponent<Text>().text = stats;
        shopDescriptionsContainer.transform.GetChild((int)ShopTextBoxTypes.Description).GetComponent<Text>().text = description;
}
```

By doing so it is immediately easier for the reader to see which text box we are accessing. Another option would be to use Transform.Find(�ObjectName�). This improves code maintainability since the order of the children no longer needs to be specific, but this function is generally more performance heavy as it performs a search on all children. The developer would also need to make sure that the names of the children are consistent with the ones used in the search as changing the names in the scene would make the search fail. Given that the order of the children would never change I personally deemed it better to use int casted enums for acquiring the correct child objects.  
 
### Martin
For the example of bad code, I want to take a look at the Slingshot ability found [here](https://bitbucket.org/Knitram/dockitleague/src/ae564abaf8bceccf46ebe47437bfdcd1f81e2afd/Assets/Scripts/Abilities/SniperKit/Slingshot.cs?at=master&fileviewer=file-view-default). This contains a few functions for handling the animation of line renderers. The reason for needing to animate this by code is that the Unity animation system can�t animate array values, which is the container for the line renderers positions. This script is difficult to follow, I believe somewhat because of the length of each animation function: LocalFiring, Firing, and FireAnimation. These functions intertwine visual elements with gameplay elements, which would be better off separated in different functions, completely separating animation code from the other functionality. This would mean that the animation function could be the same for the local client and every other client, and a change that could make the script easier to follow. In these animation functions there are also some repeating of code, they all contain while loops that runs over a timer from 0 to 1, that almost does the same thing, but evaluates different curves. 

```csharp
while(timer < 1f) {
    timer += holdCurveModifier * Time.deltaTime;
    curveValue = holdCurve.Evaluate(timer);
 
    SetProjectilePosition(projectileMaxPrecisionY + curveValue * projectileRange);
    yield return null;
}
```

These should be moved into a single function, that would take the curve to evaluate as a parameter. Thus, reducing bloat in the functions, and increasing maintainability (loop content in a single place). The reason for not doing this initially while prototyping is that this ability needs to be able to cancel these animations whenever the player performs an action (releasing the fire button for instance), and in order to do that they need to be stored whenever they are started. Looking back it�s not that difficult to keep track of an extra IEnumerator.
I did however add the helper function SetProjectilePosition in the second commit of this script for the same reason. This function is called multiple places, and means it�s content is stored in a single place, if any changes are needed.

```csharp
private void SetProjectilePosition(float position) {
    projectileVisuals.localPosition = new Vector3(0f, position, 0f);
    slingRenderer.SetPosition(1, projectileVisuals.localPosition);
}
```

For the good part I want to focus on the spawning of objects, for the abilities in particular. The first part of this was moving from a virtual function in the Ability base class, to an interface. This was part of a process to reduce the bloat of virtual functions in the base class, and rather let the abilities implement the interfaces they want. In this instance we went from the function in the Ability class display at the top, to the interface displayed below:
 
```csharp
public virtual GameObject GetSpawnablePrefab(int spawnableId) { return null; }
 
public interface ISpawnableProvider {
    GameObject GetSpawnablePrefab(int spawnableId);
}
```

Separating this function to an interface makes it really clear what an ability does. By just looking at the header of any given ability, you can see if it implements this interface to know if it can spawn an object, but with the old solution you would have to look through the whole class for the overriding function.

The second part of handling spawnables, was the addition of the [SpawnableFactory script](https://bitbucket.org/Knitram/dockitleague/src/ae564abaf8bceccf46ebe47437bfdcd1f81e2afd/Assets/Scripts/SpawnableFactory.cs?at=master&fileviewer=file-view-default).

```csharp
private List<SpawnableObject> spawnedObjects;
 
public SpawnableObject SpawnObject(GameObject obj, Vector3 position, Vector3 rotation, uint player, TeamId team) {
    SpawnableObject spawnObject = (Instantiate(obj, position, Quaternion.Euler(rotation)) as GameObject).GetComponent<SpawnableObject>();
    NetworkServer.Spawn(spawnObject.gameObject);
 
    spawnObject.SetOwner(player, team);
 
    spawnedObjects.Add(spawnObject);
    return spawnObject;
}
```

Instead of handling the spawning of objects in the Docking script, the SpawnObject function handles the spawning of objects, sets the owner, and adds it to a list. Collecting all the spawning in one place makes the management of objects easier, as this list can then be used to clean up (destroy) any spawnable, and is used to remove spawnables between rounds. In order for this list not to contain multiple null references, each spawnable object will call the SpawnableDestroyed function when destroyed during play, thus making sure they�re removed from the list:

```csharp
public void SpawnableDestroyed(SpawnableObject spawnObject) {
    spawnedObjects.Remove(spawnObject);
}
```
 
This factory was implemented as a singleton, which means it�s accessible from any script. The use of singletons is a disputed topic, and might be seen as a glorified global. But seeing as spawning an object from anywhere is something you can already do in Unity, using a singleton here to access a management class doesn�t really add options for the programmer that isn�t there already, and it makes it easier to manage spawning.

### Sondre
This is from the stealth.cpp file repository source file.

```csharp
IEnumerator FadeToStealth() {
    if (docking.hasAuthority) {
        for (float i = 1; i >= 0.5f; i -= fadeInterval / fadeTime / 2) {
            foreach (SpriteRenderer sr in visuals) {
                var faded = sr.material.color;
                faded.a = i;
                sr.material.color = faded;
            }
            yield return new WaitForSeconds(fadeInterval);
        }
    }
 
    if (!docking.hasAuthority) {
        for (float i = 1; i >= 0; i -= fadeInterval / fadeTime) {
            foreach (SpriteRenderer sr in visuals) {
                var faded = sr.material.color;
                faded.a = i;
                sr.material.color = faded;
            }
            yield return new WaitForSeconds(fadeInterval);
        }
    }
}
```
 
It's not a very complex piece of code, but it does what it should which is to make the player invisible to other players and halfway for the player. The good part is that it's fairly easy to see what the code does, and it's very simple. It takes into consideration variable fade time that can be set in the Unity editor. It's not as polished as it should be, as the "fadeInterval / fadeTime / 2" might be a bit confusing, for example, and should probably have its own variable for the change in alpha each step. It also doesn't give the option to decide the target alpha value, say 0.3 faded instead of 0.5, which also should've been something that should be able to edit in the editor in my opinion. The resulting code could look something like:

```csharp
for(float i = 1; i >= (1 - amountFaded); i -= alphaIncrements)
```

where "amountFaded" would be a float you can set in the inspector, subtracting this from 1 (since alpha = 1 is fully visible, alpha = 0 fully see-through). "alphaIncrements" would be "alphaIncrements = (fadeInterval / fadeTime) * amountFaded". This still assumes the fade time is non-zero, since we weren't using that. This could be taking into account by either making an extra check or most likely a new function, since it has no fade time it doesn't belong in a function named for stealthing with fade time. To just keep it as is and take account for this, setting the alphaIncrements to amountFaded would achieve basically zero fade time if fadeInterval is also set to 0 in the inspector, using 2 frames to go into stealth.