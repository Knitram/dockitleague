# About Dockit League #
Dockit League is a small MOBA created as part of the bachelor thesis for Martin Langslet Lilleslåtten, Andreas Wang and Sondre Fjeldheim Svarverud. 


# Installation #
The different binaries of Dockit League can be downloaded from the following links. Make note that we only have tested the Windows versions of the game:

Windows x86:
https://developer.cloud.unity3d.com/share/ZyLXc3Rm8M/

Windows x64:
https://developer.cloud.unity3d.com/share/-kA0jnAQUf/

macOS:
https://developer.cloud.unity3d.com/share/bJJ62nCX8M/

Linux:
https://developer.cloud.unity3d.com/share/-J4ys3CmLG/


# Controls #
Action     | Controller    | Keyboard
-----------| ------------- | ---------
Movement   | Left stick    | WASD
Rotate     | Right stick   | Arrow keys
Ability 1  | Left Bumper   | Left ctrl
Ability 2  | Right Bumper  | Left alt
Ability 3  | Left Trigger  | Left shift
Ability 4  | Right Trigger | Space
Interact   | A             | F
Dock       | X             | K
Undock     | Y             | L
Open shop  | Back button   | Tab
Exit game  | Start button  | Escape