﻿using UnityEngine;

public class UpdateSlingRenderer : StateMachineBehaviour {
    private LineRenderer slingRenderer;
    private Transform leftHandle;
    private Transform rightHandle;

    /// <summary>
    /// Called when the animator enters this state, set up references the first time.
    /// </summary>
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if(slingRenderer == null) {
            DockingKit dockingKit = animator.gameObject.GetComponent<DockingKit>();
            Focus focus = dockingKit.abilities[2] as Focus;
            slingRenderer = (dockingKit.abilities[0] as Slingshot).slingRenderer;
            leftHandle = focus.leftSlingHandle;
            rightHandle = focus.rightSlingHandle;
        }
    }

    /// <summary>
    /// Update loop while in this state, sets line renderer positions while the handles are animating.
    /// </summary>
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        slingRenderer.SetPosition(0, slingRenderer.transform.InverseTransformPoint(leftHandle.position));
        slingRenderer.SetPosition(2, slingRenderer.transform.InverseTransformPoint(rightHandle.position));
    }

}
