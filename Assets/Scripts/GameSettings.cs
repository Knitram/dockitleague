﻿using System;
using UnityEngine;

[Serializable]
public class GameSettings {
    public event Action<MapInfo> mapChanged;
    public event Action<ModeInfo> modeChanged;

    [SerializeField]
    private string lobbySceneName;

    [SerializeField]
    private MapList mapList;

    [SerializeField]
    private ModeList modeList;

    public MapInfo Map {
        get;
        private set;
    }

    public int MapIndex {
        get;
        private set;
    }

    public ModeInfo Mode {
        get;
        private set;
    }

    public int ModeIndex {
        get;
        private set;
    }

    public int ScoreTarget {
        get;
        private set;
    }

    public string GetLobbySceneName() {
        return lobbySceneName;
    }

    /// <summary>
    /// Sets the index of the map.
    /// </summary>
    /// <param name="index">Index.</param>
    public void SetMapIndex(int index) {
        Map = mapList[index];
        MapIndex = index;

        if(mapChanged != null) {
            mapChanged(Map);
        }
    }

    /// <summary>
    /// Sets the index of the mode.
    /// </summary>
    /// <param name="index">Index.</param>
    public void SetModeIndex(int index) {
        SetMode(modeList[index], index);
    }

    /// <summary>
    /// Sets the mode.
    /// </summary>
    /// <param name="newMode">The new mode.</param>
    /// <param name="newModeIndex">Index of the new mode.</param>
    private void SetMode(ModeInfo newMode, int newModeIndex) {
        Mode = newMode;
        ModeIndex = newModeIndex;
        if(modeChanged != null) {
            modeChanged(Mode);
        }

        //mode.GetModeProcessor().GetColorProvider().Reset();
        ScoreTarget = Mode.GetModeProcessor().ScoreWinTarget;
    }
}
