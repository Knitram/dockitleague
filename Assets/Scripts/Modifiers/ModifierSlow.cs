﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Modifier/Slow")]
public class ModifierSlow : Modifier {
    public float slowPercentage = 0.5f;

    public override void OnLocalClientStart(PlayerStatus playerStatus) {
        playerStatus.GetComponent<PlayerInput>().moveSpeed *= slowPercentage;
    }

    public override void OnLocalClientEnd(PlayerStatus playerStatus) {
        playerStatus.GetComponent<PlayerInput>().moveSpeed /= slowPercentage;
    }
}
