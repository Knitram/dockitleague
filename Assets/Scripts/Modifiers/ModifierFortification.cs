﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Modifier/Fortification")]
public class ModifierFortification : Modifier {

    public float damageMultiplier;
    public override void OnLocalClientStart(PlayerStatus playerStatus) {
        PlayerHealth pHealth = playerStatus.GetComponent<PlayerHealth>();
        pHealth.CmdSetDamageMultiplier(damageMultiplier);
    }

    public override void OnLocalClientEnd(PlayerStatus playerStatus) {
        PlayerHealth pHealth = playerStatus.GetComponent<PlayerHealth>();
        pHealth.CmdSetDamageMultiplier(1f / damageMultiplier);
    }

}
