﻿using UnityEngine;

[CreateAssetMenu(menuName = "Modifier/Silence")]
public class ModifierSilence : Modifier {
    public override void OnLocalClientStart(PlayerStatus playerStatus) {
        playerStatus.GetComponent<Docking>().CancelAbilities();
        playerStatus.GetComponent<PlayerInput>().SetInputRestrictions(true, new InputType[] { InputType.Abilities, InputType.Docking });
    }

    public override void OnLocalClientEnd(PlayerStatus playerStatus) {
        playerStatus.GetComponent<PlayerInput>().SetInputRestrictions(false, new InputType[] { InputType.Abilities, InputType.Docking });
    }
}
