﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The instance used when a modifier is active. Only exists on the server.
/// </summary>
[System.Serializable]
public class ModifierInstanceServer {
    private ModifierInfo modifierInfo;
    private PlayerStatus playerStatus;

    private Coroutine loopCoroutine;

    private int modifierId;
    private int abilityId;

    /// <summary>
    /// Constructor which starts the correct update loop as a Coroutine on the playerStatus MonoBehaviour.
    /// </summary>
    /// <param name="info">Information about this modifier.</param>
    /// <param name="plStatus">The playerStatus the modifier is applied to.</param>
    /// <param name="modId">Unique identifier for this modifier instance.</param>
    /// <param name="abId">The Id of the ability that applied the modifier if any, -1 otherwise.</param>
    public ModifierInstanceServer(ModifierInfo info, PlayerStatus plStatus, int modId, int abId) {
        modifierInfo = info;
        playerStatus = plStatus;

        modifierId = modId;
        abilityId = abId;
        modifierInfo.modifier.OnServerStart(playerStatus, abilityId);

        //Start coroutines on the PlayerStatus MonoBehaviour.
        if(modifierInfo.tickCount > 0) {
            loopCoroutine = playerStatus.StartCoroutine(TickLoop());
        } else if(modifierInfo.duration > 0) {
            loopCoroutine = playerStatus.StartCoroutine(DurationLoop());
        }
    }

    /// <summary>
    /// Update loop when the duration is used.
    /// </summary>
    public IEnumerator DurationLoop() {
        while(modifierInfo.duration > 0f) {
            modifierInfo.duration -= Time.deltaTime;
            yield return null;
        }

        OnEnd();
    }

    /// <summary>
    /// Update loop when the ticks are used.
    /// </summary>
    public IEnumerator TickLoop() {
        float timer = modifierInfo.tickInterval;

        while(modifierInfo.tickCount > 0) {
            timer += Time.deltaTime;

            if(timer >= modifierInfo.tickInterval) {
                timer -= modifierInfo.tickInterval;
                modifierInfo.tickCount--;

                modifierInfo.modifier.OnServerTick(playerStatus);
            }

            yield return null;
        }

        OnEnd();
    }

    /// <summary>
    /// Called when the modifier effect has ended.
    /// </summary>
    public void OnEnd() {
        modifierInfo.modifier.OnServerEnd(playerStatus, abilityId);

        if(loopCoroutine != null) {
            playerStatus.StopCoroutine(loopCoroutine);
        }
        playerStatus.RemoveModifier(this);
    }

    /// <summary>
    /// Called when the ability modifier effect is cancelled (e.g. undocking).
    /// </summary>
    public void OnCancel() {
        modifierInfo.modifier.OnServerEnd(playerStatus, abilityId);

        if(loopCoroutine != null) {
            playerStatus.StopCoroutine(loopCoroutine);
        }
        playerStatus.RemoveModifier(this, false);
    }

    /// <summary>
    /// Used for unique modifiers that doesn't stack. Uses the largest of the given durations.
    /// </summary>
    /// <param name="newDuration">The duration to compare the current duration with.</param>
    public void MaxDuration(float newDuration) {
        if(newDuration > modifierInfo.duration) {
            modifierInfo.duration = newDuration;
            //TODO: Better way to access connectionToClient.
            playerStatus.TargetSetUIDuration(playerStatus.GetComponent<Player>().NetworkPlayerInstance.connectionToClient, modifierId, modifierInfo.duration);
        }
    }

    /// <summary>
    /// Returns the modifier used by this instance.
    /// </summary>
    /// <returns>The active modifier.</returns>
    public Modifier GetModifier() {
        return modifierInfo.modifier;
    }

    /// <summary>
    /// Returns the ability ID in this instance, equal to or above 0 means this modifier was applied by an ability, -1 otherwise.
    /// </summary>
    /// <returns>The ability ID.</returns>
    public int GetAbilityId() {
        return abilityId;
    }

    /// <summary>
    /// Returns the unique modifier ID for this instance.
    /// </summary>
    /// <returns>The modifier ID.</returns>
    public int GetModifierId() {
        return modifierId;
    }
}
