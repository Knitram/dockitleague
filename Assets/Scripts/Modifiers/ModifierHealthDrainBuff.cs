﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Modifier/HealthDrainBuff")]
public class ModifierHealthDrainBuff : Modifier {
    public override void OnLocalClientStart(PlayerStatus playerStatus) {
    }

    public override void OnLocalClientEnd(PlayerStatus playerStatus) {
    }
}
