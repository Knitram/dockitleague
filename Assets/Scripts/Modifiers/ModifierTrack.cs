﻿using UnityEngine;


[CreateAssetMenu(menuName = "Modifier/Track")]
public class ModifierTrack : Modifier {
    public float damageMultiplier;
    public override void OnLocalClientStart(PlayerStatus playerStatus) {
        PlayerHealth pHealth = playerStatus.GetComponent<PlayerHealth>();
        pHealth.CmdSetDamageMultiplier(damageMultiplier);
    }

    public override void OnLocalClientEnd(PlayerStatus playerStatus) {
        PlayerHealth pHealth = playerStatus.GetComponent<PlayerHealth>();
        pHealth.CmdSetDamageMultiplier(1f / damageMultiplier);
    }
}