﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The instance used when a modifier is active. Only exists on the clients.
/// </summary>
[System.Serializable]
public class ModifierInstanceClient {
    private Modifier modifier;
    private PlayerStatus playerStatus;

    private GameObject playerEffect;
    private GameObject localPlayerEffect;

    private int modifierId;
    private int abilityId;

    private StatusUI statusUI;

    /// <summary>
    /// Constructor that instantiates effect objects and calls the correct modifier functions.
    /// </summary>
    /// <param name="mod">The modifier for this instance.</param>
    /// <param name="plStatus">The playerStatus the modifier is applied to.</param>
    /// <param name="playerUIHandler">Reference to the UIHandler, used by the local client to add modifier UI.</param>
    /// <param name="modId">Unique identifier for this modifier instance.</param>
    /// <param name="abId">The Id of the ability that applied the modifier if any, -1 otherwise.</param>
    /// <param name="duration">The initial modifier duration.</param>
    /// <param name="effectParent">Either null or the transform we want to put as parent for this modifier</param>
    public ModifierInstanceClient(Modifier mod, PlayerStatus plStatus, PlayerUIHandler playerUIHandler, int modId, int abId, float duration) {
        modifier = mod;
        playerStatus = plStatus;

        abilityId = abId;
        modifierId = modId;
        modifier.OnClientStart(playerStatus, abilityId);

        if(modifier.playerEffectObject != null) {
            playerEffect = GameObject.Instantiate(modifier.playerEffectObject, playerStatus.transform, false);
        }

        if(playerStatus.hasAuthority) {
            if(modifier.localPlayerEffectObject != null) {
                localPlayerEffect = GameObject.Instantiate(modifier.localPlayerEffectObject, playerStatus.transform, false);
            }
            if(modifier.icon != null) {
                statusUI = playerUIHandler.AddStatusModifier(modifier, duration);
            }

            modifier.OnLocalClientStart(playerStatus);
        }
    }

    /// <summary>
    /// Called when the modifier effect has ended.
    /// </summary>
    public void OnEnd() {
        if(playerEffect != null) {
            GameObject.Destroy(playerEffect);
        }
        if(localPlayerEffect != null) {
            GameObject.Destroy(localPlayerEffect);
        }

        modifier.OnClientEnd(playerStatus, abilityId);

        if(playerStatus.hasAuthority) {
            if(statusUI != null) {
                statusUI.Remove();
            }
            modifier.OnLocalClientEnd(playerStatus);
        }
    }

    /// <summary>
    /// Updates the UI elements with the new duration.
    /// </summary>
    /// <param name="newDuration">The new duration.</param>
    public void SetNewDuration(float newDuration) {
        if(statusUI != null) {
            statusUI.SetNewDuration(newDuration);
        }
    }

    /// <summary>
    /// Returns the modifier used by this instance.
    /// </summary>
    /// <returns>The active modifier.</returns>
    public Modifier GetModifier() {
        return modifier;
    }

    /// <summary>
    /// Returns the ability ID in this instance, equal to or above 0 means this modifier was applied by an ability, -1 otherwise.
    /// </summary>
    /// <returns>The ability ID.</returns>
    public int GetAbilityId() {
        return abilityId;
    }

    /// <summary>
    /// Returns the unique modifier ID for this instance.
    /// </summary>
    /// <returns>The modifier ID.</returns>
    public int GetModifierId() {
        return modifierId;
    }
}
