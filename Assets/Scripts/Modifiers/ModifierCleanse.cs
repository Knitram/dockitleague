﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Modifier/Cleanse")]
public class ModifierCleanse : Modifier {
    public float movespeedMulitplier;

    public override void OnServerStart(PlayerStatus playerStatus, int abilityId) {
        if (playerStatus.GetComponent<Docking>().isLocalPlayer) {
            playerStatus.GetComponent<Docking>().SetModifier(abilityId, true);
        }
        playerStatus.GetComponent<PlayerInput>().moveSpeed *= movespeedMulitplier;
        playerStatus.RemoveAllDebuffModifiers();
    }

    public override void OnServerEnd(PlayerStatus playerStatus, int abilityId) {
        if (playerStatus.GetComponent<Docking>().isLocalPlayer) {
            playerStatus.GetComponent<Docking>().SetModifier(abilityId, false);
        }
        playerStatus.GetComponent<PlayerInput>().moveSpeed /= movespeedMulitplier;
    }
}