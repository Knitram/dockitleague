﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for every modifier.
/// </summary>
public abstract class Modifier : ScriptableObject {
    public string modifierName;
    public Sprite icon;
    public GameObject playerEffectObject;
    public GameObject localPlayerEffectObject;
    public bool unique;
    public StatusType statusType;

    /// <summary>
    /// Called on the server when the modifiers starts.
    /// </summary>
    /// <param name="playerStatus">Reference to the associated PlayerStatus.</param>
    /// <param name="abilityId">The Id of the ability that applied the modifier if any, -1 otherwise.</param>
    public virtual void OnServerStart(PlayerStatus playerStatus, int abilityId) { }

    /// <summary>
    /// Called on every client when the modifiers starts.
    /// </summary>
    /// <param name="playerStatus">Reference to the associated PlayerStatus.</param>
    /// <param name="abilityId">The Id of the ability that applied the modifier if any, -1 otherwise.</param>
    public virtual void OnClientStart(PlayerStatus playerStatus, int abilityId) { }

    /// <summary>
    /// Called on the local client (the client the modifier is applied to) when the modifiers starts.
    /// </summary>
    /// <param name="playerStatus">Reference to the associated PlayerStatus.</param>
    public virtual void OnLocalClientStart(PlayerStatus playerStatus) { }

    /// <summary>
    /// Called on the server when the modifiers ends.
    /// </summary>
    /// <param name="playerStatus">Reference to the associated PlayerStatus.</param>
    /// <param name="abilityId">The Id of the ability that applied the modifier if any, -1 otherwise.</param>
    public virtual void OnServerEnd(PlayerStatus playerStatus, int abilityId) { }

    /// <summary>
    /// Called on every client when the modifiers ends.
    /// </summary>
    /// <param name="playerStatus">Reference to the associated PlayerStatus.</param>
    /// <param name="abilityId">The Id of the ability that applied the modifier if any, -1 otherwise.</param>
    public virtual void OnClientEnd(PlayerStatus playerStatus, int abilityId) { }

    /// <summary>
    /// Called on the local client (the client the modifier is applied to) when the modifiers ends.
    /// </summary>
    /// <param name="playerStatus">Reference to the associated PlayerStatus.</param>
    public virtual void OnLocalClientEnd(PlayerStatus playerStatus) { }

    /// <summary>
    /// Called on the server whenever the modifier applies a tick.
    /// </summary>
    /// <param name="playerStatus">Reference to the associated PlayerStatus.</param>
    public virtual void OnServerTick(PlayerStatus playerStatus) {
        Debug.LogError("ERROR: Tick called on modifier without implementation");
    }

    /// <summary>
    /// Looks up the Modifier with modifierName from the Resource/PlayerModifiers folder.
    /// </summary>
    /// <param name="modifierName">The modifier file name.</param>
    /// <returns>The modifier at path if found, otherwise null.</returns>
    public static Modifier GetModifierAsset(string modifierName) {
        return Resources.Load<Modifier>("PlayerModifiers/" + modifierName);
    }
}

/// <summary>
/// Struct used in abilities to store modifier information.
/// </summary>
[System.Serializable]
public struct ModifierInfo {
    public Modifier modifier;
    public float duration;
    public int tickCount;
    public float tickInterval;
}

//TESTING, possibly do something like this?
[System.Serializable]
public class ModifierInfoBase {
    public Modifier modifier;
}

[System.Serializable]
public class ModifierInfoDuration : ModifierInfoBase {
    public float duration;
}

[System.Serializable]
public class ModifierInfoTick : ModifierInfoBase {
    public int tickCount;
    public float tickInterval;
}