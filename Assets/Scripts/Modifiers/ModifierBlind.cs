﻿using UnityEngine;

[CreateAssetMenu(menuName = "Modifier/Blind")]
public class ModifierBlind : Modifier {
    public float blindLerpSpeed = 10;

    public override void OnClientStart(PlayerStatus playerStatus, int abilityId) {
        playerStatus.GetComponent<FieldOfView>().SetViewRadius(0f, blindLerpSpeed);
    }

    public override void OnClientEnd(PlayerStatus playerStatus, int abilityId) {
        playerStatus.GetComponent<FieldOfView>().ResetViewRadius(blindLerpSpeed);
    }
}
