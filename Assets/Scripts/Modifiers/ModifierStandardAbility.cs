﻿using UnityEngine;

[CreateAssetMenu(menuName = "Modifier/Standard Ability")]
public class ModifierStandardAbility : Modifier {
    public override void OnClientStart(PlayerStatus playerStatus, int abilityId) {
        playerStatus.GetComponent<Docking>().SetModifier(abilityId, true);
    }

    public override void OnClientEnd(PlayerStatus playerStatus, int abilityId) {
        playerStatus.GetComponent<Docking>().SetModifier(abilityId, false);
    }
}
