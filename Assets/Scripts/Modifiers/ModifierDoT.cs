﻿using UnityEngine;

[CreateAssetMenu(menuName = "Modifier/DoT")]
public class ModifierDoT : Modifier {
    public float damagePerTick = 5f;

    public override void OnServerTick(PlayerStatus playerStatus) {
        playerStatus.GetComponent<PlayerHealth>().TakeDamage(damagePerTick);
    }
}
