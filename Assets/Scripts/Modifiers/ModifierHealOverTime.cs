﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Modifier/HealOverTime")]
public class ModifierHealOverTime : Modifier {
    public float healthPerTick;

    public override void OnClientStart(PlayerStatus playerStatus, int abilityId) {
        //playerStatus.GetComponent<Docking>().SetModifier(abilityId, true);
    }

    public override void OnServerTick(PlayerStatus playerStatus) {
        playerStatus.GetComponent<PlayerHealth>().Heal(healthPerTick);
    }


    public override void OnClientEnd(PlayerStatus playerStatus, int abilityId) {
        //playerStatus.GetComponent<Docking>().SetModifier(abilityId, false);
    }
}
