﻿using UnityEngine;

[CreateAssetMenu(menuName = "Modifier/Stun")]
public class ModifierStun : Modifier {
    public override void OnLocalClientStart(PlayerStatus playerStatus) {
        playerStatus.GetComponent<Docking>().CancelAbilities();
        playerStatus.GetComponent<PlayerInput>().SetInputRestrictions(true, new InputType[] { InputType.Abilities, InputType.Docking, InputType.Movement, InputType.Rotation });
    }

    public override void OnLocalClientEnd(PlayerStatus playerStatus) {
        playerStatus.GetComponent<PlayerInput>().SetInputRestrictions(false, new InputType[] { InputType.Abilities, InputType.Docking, InputType.Movement, InputType.Rotation });
    }
}