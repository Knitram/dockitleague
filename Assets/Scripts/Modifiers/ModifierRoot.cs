﻿using UnityEngine;

[CreateAssetMenu(menuName = "Modifier/Root")]
public class ModifierRoot : Modifier {
    public override void OnLocalClientStart(PlayerStatus playerStatus) {
        playerStatus.GetComponent<PlayerInput>().SetInputRestrictions(true, new InputType[] { InputType.Movement });
    }

    public override void OnLocalClientEnd(PlayerStatus playerStatus) {
        playerStatus.GetComponent<PlayerInput>().SetInputRestrictions(false, new InputType[] { InputType.Movement });
    }
}