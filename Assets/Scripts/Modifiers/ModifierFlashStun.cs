﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Modifier/FlashStun")]
public class ModifierFlashStun : ModifierStun {
    public GameObject flashPrefab;

    public override void OnLocalClientStart(PlayerStatus playerStatus) {
        base.OnLocalClientStart(playerStatus);

        GameObject flashObj = Instantiate(flashPrefab);
        flashObj.transform.SetParent(GameObject.FindGameObjectWithTag("UIRoot").transform, false);
    }
}
