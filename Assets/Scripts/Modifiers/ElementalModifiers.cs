﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ElementalModifiers {
    public Transform elementEffectTransform;

    [Header("Buffs")]
    public ModifierInfo fireBuff;
    public ModifierInfo iceBuff;
    public ModifierInfo electricBuff;

    [Header("Debuffs")]
    public ModifierInfo fireDebuff;
    public ModifierInfo iceDebuff;
    public ModifierInfo electricDebuff;

    private List<ModifierInfo> elementalBuffs = new List<ModifierInfo>();
    private List<ModifierInfo> elementalDebuffs = new List<ModifierInfo>();

    private GameObject currentElementalEffect;

    private ElementalContainer.ComboableElements elementalAttribute;

    public void Initialize() {
        elementalBuffs.Add(fireBuff);
        elementalBuffs.Add(iceBuff);
        elementalBuffs.Add(electricBuff);
        elementalDebuffs.Add(fireDebuff);
        elementalDebuffs.Add(iceDebuff);
        elementalDebuffs.Add(electricDebuff);

        elementalAttribute = ElementalContainer.ComboableElements.Empty;
    }

    public void SetModifier(bool state) {
        if (!state) {
            elementalAttribute = ElementalContainer.ComboableElements.Empty;
            GameObject.Destroy(currentElementalEffect);
        }
    }

    public ModifierInfo GetModifierInfo(int modifierId) {
        return elementalBuffs[modifierId];
    }

    /// <summary>
    /// Handles the transferring of the elemental buff by applying it as a debuff to the player that was hit
    /// </summary>
    /// <param name="other">The collider we want to apply the debuff to</param>
    public void TransferElementalModifier(Collider other, Docking docking, int abilityId) {
        if (elementalAttribute != ElementalContainer.ComboableElements.Empty) {
            PlayerStatus playerStatus = other.GetComponent<PlayerStatus>();
            if (playerStatus != null) {
                playerStatus.ApplyModifier(elementalDebuffs[(int)elementalAttribute]);
            }

            IElement elemObj = other.GetComponent<IElement>();
            if (elemObj != null) {
                elemObj.ApplyElement(elementalAttribute);
            }

            docking.CmdSetModifier(abilityId, (int)elementalAttribute, false);
        }
    }

    public void ApplyElement(ElementalContainer.ComboableElements element, Docking docking, int abilityId) {
        if (elementalAttribute == ElementalContainer.ComboableElements.Empty && (int)element < elementalBuffs.Count) {
            docking.CmdSetElement(element, abilityId);
            docking.CmdSetModifier(abilityId, (int)element, true);
        }
    }

    public void SetElement(ElementalContainer.ComboableElements element) {
        if (elementalAttribute == ElementalContainer.ComboableElements.Empty) {
            currentElementalEffect = PrefabInstantiator.InstantiateComboPrefab(elementEffectTransform, element);
            currentElementalEffect.transform.parent = elementEffectTransform;

            elementalAttribute = element;
        }
    }
}