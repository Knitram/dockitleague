﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemInstance : MonoBehaviour {
    public ShopItemData itemData;

    public Image uiIcon;
    public Text priceText;
    public Image unavailableOverlay;
    public Text isEquippedText;

    private IngameMenuHandler ingameMenuHandler;

    public void Initialize(ShopItemData iData, IngameMenuHandler handler) {
        itemData = iData;
        uiIcon.sprite = itemData.icon;
        priceText.text = itemData.price.ToString();
        ingameMenuHandler = handler;
    }

    public void OnSelectionChange() {
        ingameMenuHandler.OnShopSelectionChange();
    }

    public void OnClick() {
        ingameMenuHandler.DisplayVerificationPrompt();
    }
}