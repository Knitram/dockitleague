﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MapList", menuName = "Data List/Map")]
public class MapList : ScriptableObject {
    [SerializeField]
    private List<MapInfo> maps;

    //Indexer implementation.
    public MapInfo this[int index] {
        get { return maps[index]; }
    }

    public int Count {
        get { return maps.Count; }
    }
}