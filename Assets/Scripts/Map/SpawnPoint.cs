﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class SpawnPoint : MonoBehaviour {
    [SerializeField]
    private Transform spawnPointTransform;

    [SerializeField]
    private TeamId spawnPointTeamId;

    //Lazy load transform reference.
    public Transform SpawnPointTransform {
        get {
            if(spawnPointTransform == null) {
                spawnPointTransform = transform;
            }
            return spawnPointTransform;
        }
    }

    //if multiple respawns occurs simultaneously then there will be no firing on trigger functionality to ensure zones are marked as occupied, hence the need for a dirty variable
    private bool isDirty = false;

    //number of tanks currently within the bounds of the spawn point
    private int numberOfPlayersInZone = 0;

    //Is the zone empty and not marked as dirty
    public bool isEmptyZone {
        get { return !isDirty && numberOfPlayersInZone == 0; }
    }

    public TeamId GetTeamId() {
        return spawnPointTeamId;
    }

    private void OnTriggerEnter(Collider other) {
        PlayerHealth player = other.GetComponent<PlayerHealth>();

        if(player != null) {
            numberOfPlayersInZone++;
        }
    }

    private void OnTriggerExit(Collider other) {
        PlayerHealth player = other.GetComponent<PlayerHealth>();

        if(player != null) {
            Decrement();
        }
    }

    /// <summary>
    /// Safely decrement the number of players in the zone and set isDirty to false
    /// </summary>
    public void Decrement() {
        numberOfPlayersInZone--;
        if(numberOfPlayersInZone < 0) {
            numberOfPlayersInZone = 0;
        }

        isDirty = false;
    }

    /// <summary>
    /// Used to set the spawn point to dirty to prevent simultaneous spawns from occurring at the same point 
    /// </summary>
    public void SetDirty() {
        isDirty = true;
    }

    /// <summary>
    /// Resets/cleans up the spawn point
    /// </summary>
    public void Cleanup() {
        isDirty = false;
        numberOfPlayersInZone = 0;
    }
}
