﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnManager : Singleton<SpawnManager> {
    private List<SpawnPoint> spawnPoints = new List<SpawnPoint>();

    protected override void Awake() {
        base.Awake();
        LazyLoadSpawnPoints();
    }

    private void Start() {
        LazyLoadSpawnPoints();
    }

    /// <summary>
    /// Lazy load the spawn points - this assumes that all spawn points are children of the SpawnManager
    /// </summary>
    private void LazyLoadSpawnPoints() {
        if(spawnPoints != null && spawnPoints.Count > 0) {
            return;
        }

        SpawnPoint[] foundSpawnPoints = GetComponentsInChildren<SpawnPoint>();
        spawnPoints.AddRange(foundSpawnPoints);
    }

    /// <summary>
    /// Gets index of a random empty spawn point
    /// </summary>
    /// <returns>The random empty spawn point index.</returns>
    public int GetRandomEmptySpawnPointIndex(TeamId teamId) {
        LazyLoadSpawnPoints();
        List<SpawnPoint> emptySpawnPoints = spawnPoints.Where(sp => (sp.isEmptyZone && (teamId == TeamId.Unassigned || sp.GetTeamId() == teamId))).ToList();

        //If no zones are empty, which is impossible if the setup is correct, then return the first spawnpoint in the list
        if(emptySpawnPoints.Count == 0) {
            return 0;
        }

        //Get random empty spawn point
        SpawnPoint emptySpawnPoint = emptySpawnPoints[Random.Range(0, emptySpawnPoints.Count)];
        emptySpawnPoint.SetDirty();

        Debug.Log(teamId + " player with spawnId " + spawnPoints.IndexOf(emptySpawnPoint));
        return spawnPoints.IndexOf(emptySpawnPoint);
    }

    public SpawnPoint GetSpawnPointByIndex(int i) {
        LazyLoadSpawnPoints();
        return spawnPoints[i];
    }
    
    public Transform GetSpawnPointTransformByIndex(int i) {
        return GetSpawnPointByIndex(i).SpawnPointTransform;
    }

    /// <summary>
    /// Cleans up the spawn points.
    /// </summary>
    public void CleanupSpawnPoints() {
        spawnPoints.ForEach(sp => sp.Cleanup());
    }
}
