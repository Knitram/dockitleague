﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapInfo {
    [SerializeField]
    private string mapName;

    [SerializeField]
    [Multiline]
    private string mapDescription;

    [SerializeField]
    private string sceneName;

    [SerializeField]
    private Sprite mapImage;

    public string GetName() {
        return mapName;
    }

    public string GetDescription() {
        return mapDescription;
    }

    public string GetSceneName() {
        return sceneName;
    }

    public Sprite GetMapImage() {
        return mapImage;
    }
}
