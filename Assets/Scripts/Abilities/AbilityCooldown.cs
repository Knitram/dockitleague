﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles the cooldown for abilities. Runs on the local player.
/// </summary>
public class AbilityCooldown {
    private float cooldownDuration;
    private float cooldownTimeLeft;

    private AbilityUI abilityUI;
    private bool ready;

    private Ability ability;

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="duration">Length of cooldown.</param>
    public AbilityCooldown(Ability ab, float duration, AbilityUI abUI) {
        ready = false;
        ability = ab;
        cooldownDuration = duration;
        abilityUI = abUI;
    }

    /// <summary>
    /// Reduces the current cooldown for the ability.
    /// </summary>
    /// <param name="reductionAmount">The amount deducted for the current cooldown.</param>
    public void ReduceCooldown(float reductionAmount) {
        cooldownTimeLeft = Mathf.Max(0f, cooldownTimeLeft - reductionAmount);
        abilityUI.UpdateCooldown(cooldownTimeLeft);
    }

    /// <summary>
    /// Update loop. Handles timer and ability ui update.
    /// </summary>
    public void Update() {
        if(ready) {
            return;
        }

        cooldownTimeLeft -= Time.deltaTime;

        if(cooldownTimeLeft <= 0f) {
            Ready();
        }
    }

    /// <summary>
    /// Called on ability activation. Activates cooldown.
    /// </summary>
    public void Activate() {
        ready = false;
        cooldownTimeLeft = cooldownDuration;
        abilityUI.Activate();
    }

    /// <summary>
    /// Can be called from abilities whenever they need a hidden cooldown, a simple short cooldown in addition to the standard cooldown for instance.
    /// </summary>
    public void ActivateHiddenCooldown(float hiddenCooldown) {
        ready = false;
        cooldownTimeLeft = hiddenCooldown;
    }

    /// <summary>
    /// Used for checking if the ability is on cooldown.
    /// </summary>
    /// <returns>Whether the ability is on cooldown or not.</returns>
    public bool IsReady() {
        return ready;
    }

    private void Ready() {
        cooldownTimeLeft = 0f;
        ready = true;

        ability.CooldownReady();
    }
}
