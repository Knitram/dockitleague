﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardSpawnableSpawner : Ability, ISpawnableProvider {
    public string animatorTrigger;

    public GameObject spawnablePrefab;
    public Transform spawnPoint;

    /// <summary>
    /// Callback for what this ability should do once its associated button has been pressed
    /// </summary>
    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();
            SetActive();
            docking.CmdSetActive(abilityId, true);

            docking.CmdSpawnObject(abilityId, 0, spawnPoint.position, spawnPoint.eulerAngles);
        }
    }

    public override void SetActive(bool state = false) {
        if(!string.IsNullOrEmpty(animatorTrigger)) {
            animator.SetTrigger(animatorTrigger);
        }
    }

    public GameObject GetSpawnablePrefab(int spawnableId) {
        return spawnablePrefab;
    }
}
