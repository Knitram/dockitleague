﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthDrainBuff : Ability, IModifierProvider, IServerCallback<GameObject> {
    private bool active = false;
    private float timer = 0f;
    public float duration;
    public float drainInterval;
    public float baseDrain;

    public ModifierInfo buff;
    public ModifierInfo debuff;

    private SphereCollider auraCollider;
    public List<GameObject> friendlyPlayersInAura = new List<GameObject>();
    public List<GameObject> hostilePlayersInAura = new List<GameObject>();


    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        auraCollider = GetComponent<SphereCollider>();
    }

    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();

            SetActive(true);
            docking.CmdSetActive(abilityId, true);
        }
    }
    
    /// <summary>
    /// Activates/deactivates collider.
    /// Cleans up lists when skill is over.
    /// </summary>
    /// <param name="state">current state of the skill</param>
    public override void SetActive(bool state) {
        active = state;
        auraCollider.enabled = state;
        docking.CmdSetModifier(abilityId, 0, state);
        if (state) {
            StartCoroutine(Drain());
        } else {
            StopCoroutine(Drain());
            hostilePlayersInAura.Clear();
            friendlyPlayersInAura.Clear();
        }
    }

    /// <summary>
    /// </summary>
    /// <param name="state"></param>
    public override void SetModifier(bool state) {
        
    }

    /// <summary>
    /// Override to end the skill after duration is over.
    /// </summary>
    protected override void Update() {
        base.Update();

        if(active) {
            timer += Time.deltaTime;
            if (timer > duration) {
                timer = 0f;
                ClearOnDurationEnd();
                docking.CmdSetActive(abilityId, false);
            }
        }
        
    }

    /// <summary>
    /// Adds players that enters the aura in list of players in aura
    /// </summary>
    /// <param name="other">the other collider</param>
    public void OnTriggerEnter(Collider other) {
        if(!docking.hasAuthority) {
            return;
        }
            
        if(other.GetComponent<Player>() != null && this.docking.netId.Value != other.GetComponent<Docking>().netId.Value) {
            docking.CmdServerCallbackGameObject(abilityId, 0, other.gameObject);
        }
    }

    /// <summary>
    /// Removes players who leave the aura from the list of players in aura
    /// </summary>
    /// <param name="other">the other collider</param>
    public void OnTriggerExit(Collider other) {
        if(!docking.hasAuthority) {
            return;
        }

        if (friendlyPlayersInAura.Contains(other.gameObject)) {
            friendlyPlayersInAura.Remove(other.gameObject);
            other.GetComponent<PlayerStatus>().RemoveModifier(buff.modifier);
        } else if (hostilePlayersInAura.Contains(other.gameObject)) {
            hostilePlayersInAura.Remove(other.gameObject);
            other.GetComponent<PlayerStatus>().RemoveModifier(debuff.modifier);
        }

    }

    public void ClearOnDurationEnd() {
        foreach(GameObject player in friendlyPlayersInAura) {
            player.GetComponent<PlayerStatus>().RemoveModifier(buff.modifier);
        }
        foreach(GameObject player in hostilePlayersInAura) {
            player.GetComponent<PlayerStatus>().RemoveModifier(debuff.modifier);
        }
    }

    /// <summary>
    /// Applies the drain damage / heal to the players at a set interval.
    /// </summary>
    /// <returns></returns>
    public IEnumerator Drain() {
        if(docking.hasAuthority) {
            while(active) {
                yield return new WaitForSeconds(drainInterval);

                foreach (GameObject player in friendlyPlayersInAura) {
                    float healthToHeal = (baseDrain * hostilePlayersInAura.Count) / friendlyPlayersInAura.Count;
                    player.GetComponent<PlayerHealth>().Heal(healthToHeal);
                }

                foreach (GameObject player in hostilePlayersInAura) {
                    player.GetComponent<PlayerHealth>().TakeDamage(baseDrain);
                }
            }
        }
    }
    
    void IServerCallback<GameObject>.ServerCallback(int functionId, GameObject other) {
        
        if(!docking.CheckDamagable(other.GetComponent<Player>()) && !friendlyPlayersInAura.Contains(other)) {
            friendlyPlayersInAura.Add(other);
            other.GetComponent<PlayerStatus>().ApplyModifier(buff);

        } else if (docking.CheckDamagable(other.GetComponent<Player>()) && !hostilePlayersInAura.Contains(other)) {
            hostilePlayersInAura.Add(other);
            other.GetComponent<PlayerStatus>().ApplyModifier(debuff);
        }
        
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        if(modifierId > 0) {
            return debuff;
        }
        return buff;
    }

    public int GetBuffModifierId() {
        return 0;
    }

    public int GetAbilityId() {
        return abilityId;
    }

    public bool IsActive() {
        return active;
    }
}
