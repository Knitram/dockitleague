﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingAura : Ability, IModifierProvider, IServerCallback<GameObject> {

    public ModifierInfo healBuff;
    public FortificationBuff fortificationBuff;

    private bool activationToggle = false;
    public float reapplyInterval;

    public SpriteRenderer visuals;
    private SphereCollider healingAreaCollider;


    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        healingAreaCollider = GetComponent<SphereCollider>();
    }

    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();
            activationToggle = !activationToggle;
            SetActive(activationToggle);
            docking.CmdSetActive(abilityId, activationToggle);
            docking.CmdSetModifier(abilityId, 0, activationToggle);
        }
    }

    public override void SetActive(bool state) {
        visuals.gameObject.SetActive(state);
        healingAreaCollider.enabled = state;

        if(state) {
            StartCoroutine(ApplyHealingInArea(reapplyInterval));
        } else {
            StopCoroutine(ApplyHealingInArea(reapplyInterval));
        }
    }

    /// <summary>
    /// Applies the buffs, this needs to be a servercallback
    /// </summary>
    /// <param name="functionId">Not used here</param>
    /// <param name="go">GameObject containing the PlayerStatus</param>
    void IServerCallback<GameObject>.ServerCallback(int functionId, GameObject go) {
        PlayerStatus playerStatus = go.GetComponent<PlayerStatus>();
        playerStatus.ApplyModifier(healBuff);
        if(fortificationBuff.IsActive()) {
            playerStatus.ApplyModifier(fortificationBuff.buff);
        }
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return healBuff;
    }

    public int GetAbilityId() {
        return abilityId;
    }

    public int GetBuffId() {
        return 0;
    }

    /// <summary>
    /// Applies healing buff in the area
    /// </summary>
    /// <param name="interval">How often it should be applied</param>
    /// <returns></returns>
    public IEnumerator ApplyHealingInArea(float interval) {
        if (docking.hasAuthority) {
            
            while (activationToggle) {
                yield return new WaitForSeconds(interval);
                Collider[] colliders = Physics.OverlapSphere(healingAreaCollider.transform.position, healingAreaCollider.radius);
                foreach (Collider other in colliders) {

                    PlayerStatus playerStatus = other.GetComponent<PlayerStatus>();
                    if(playerStatus != null && other.GetComponent<Player>().netId.Value == this.docking.netId.Value) {
                        docking.CmdSetModifier(abilityId, 0, activationToggle);
                        if(fortificationBuff.IsActive()) {
                            docking.CmdSetModifier(fortificationBuff.GetAbilityId(), fortificationBuff.GetBuffModifierId(), activationToggle);
                        }

                    }else if (playerStatus != null && !docking.CheckDamagable(other.GetComponent<Player>())) {
                        docking.CmdServerCallbackGameObject(abilityId, 0, playerStatus.gameObject);
                    }
                }
            }
        }
    }
}
