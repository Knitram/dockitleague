﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(MeshCollider))]
public class CleanseBuff : Ability, IModifierProvider {
    private bool active = false;
    public ModifierInfo buff;
    public string animatorTrigger;
    public List<GameObject> cleansedPlayers = new List<GameObject>();
    private MeshCollider hitbox;

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);
        hitbox = GetComponent<MeshCollider>();
        hitbox.enabled = true;
    }

    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();

            SetActive(true);
            docking.CmdSetActive(abilityId, true);
            docking.CmdSetModifier(abilityId, 0, true);
        }
    }

    public override void SetActive(bool state) {
                
        if (state) {
            animator.SetTrigger(animatorTrigger);
        }

        if (!state) {
            cleansedPlayers.Clear();
        }
        active = state;
        
    }
   
    /// <summary>
    /// Just to trigger the active state when animation ends, had some issues with Animation Events.
    /// </summary>
    protected override void Update() {
        base.Update();
        if(hitbox != null && !hitbox.enabled && active) {
            docking.CmdSetActive(abilityId, false);
        }
    }

    public void OnTriggerEnter(Collider other) {
        if(!docking.isServer) {
            return;
        }

        Player player = other.GetComponent<Player>();
        
        if(player != null && !docking.CheckDamagable(player) && !cleansedPlayers.Contains(other.gameObject)) {

            player.GetComponent<PlayerStatus>().ApplyModifier(buff);
            cleansedPlayers.Add(other.gameObject);

        }

    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return buff;
    }

    public int GetAbilityId() {
        return abilityId;
    }

    public int GetBuffModifierId() {
        return 0;
    }
}
