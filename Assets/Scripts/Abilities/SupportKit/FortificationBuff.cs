﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FortificationBuff : Ability, IModifierProvider {
    private bool active = false;
    public ModifierInfo buff;
    public float activeDuration;
    private float timer = 0;


    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);
    }

    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();
            SetActive(true);
            docking.CmdSetActive(abilityId, true);
        }
    } 
 
    public override void SetActive(bool state) {
        active = state;
    }

    protected override void Update() {
        base.Update();

        if(active) {
            timer += Time.deltaTime;
            if (timer > activeDuration) {
                timer = 0;
                SetActive(false);
                docking.CmdSetActive(abilityId, false);
            }
        }
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return buff;
    }


    public bool IsActive() {
        return active;
    }

    public int GetBuffModifierId() {
        return 0;
    }

    public int GetAbilityId() {
        return abilityId;
    }

}
