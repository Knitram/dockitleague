﻿using System.Collections.Generic;
using UnityEngine;

public class ForceField : Ability {
    public float playerForce = 10f;

    public string animatorTrigger;

    private List<IRedirectable> redirectedObjects;

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        if(dock.isServer) {
            redirectedObjects = new List<IRedirectable>();
        }
    }

    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();

            SetActive();
            docking.CmdSetActive(abilityId, true);
        }
    }

    public override void SetActive(bool state = false) {
        if(!string.IsNullOrEmpty(animatorTrigger)) {
            animator.SetTrigger(animatorTrigger);
        }

        if(docking.isServer) {
            redirectedObjects.Clear();
        }
    }

    void OnTriggerEnter(Collider other) {
        if(!docking.isServer) {
            return;
        }

        //Projectile check
        IRedirectable obj = other.GetComponent<IRedirectable>();
        if(obj != null && !redirectedObjects.Contains(obj) && other.GetComponent<SpawnableObject>().CheckDamagable(docking)) {
            obj.RedirectDirection((transform.position - other.transform.position).normalized);
            redirectedObjects.Add(obj);
            return;
        }

        //Player check
        Player player = other.GetComponent<Player>();
        if(player != null && docking.CheckDamagable(player)) {
            player.TargetAddForce(player.NetworkPlayerInstance.connectionToClient, playerForce, ForceMode.VelocityChange, transform.position);
        }
    }
}
