﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankReflectShield : Ability, IServerCallback {
    public float duration;
    public string animatorBool;
    public GameObject shieldCollider;

    private List<IRedirectable> redirectedObjects;

    private enum ServerCall { OnActivation, OnDeactivation };

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        if(dock.isServer) {
            redirectedObjects = new List<IRedirectable>();
        }
    }

    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();
            StartCoroutine(Activated());
            
            SetActive(true);
            docking.CmdSetActive(abilityId, true);
            docking.CmdServerCallback(abilityId, (int)ServerCall.OnActivation);
        }
    }

    /// <summary>
    /// Local player call, keeps track of ability duration, and deactivates ability when finished.
    /// </summary>
    private IEnumerator Activated() {
        yield return new WaitForSeconds(duration);

        SetActive(false);
        docking.CmdSetActive(abilityId, false);
        docking.CmdServerCallback(abilityId, (int)ServerCall.OnDeactivation);
    }

    public override void SetActive(bool state) {
        if(!string.IsNullOrEmpty(animatorBool)) {
            animator.SetBool(animatorBool, state);
        }
    }

    void OnTriggerEnter(Collider other) {
        if(!docking.isServer) {
            return;
        }

        IRedirectable obj = other.GetComponent<IRedirectable>();
        if(obj != null && !redirectedObjects.Contains(obj)) {
            obj.RedirectDirection(transform.forward, (int)docking.netId.Value, docking.GetComponent<Player>().GetPlayerTeamId());
            redirectedObjects.Add(obj);
        }
    }

    /// <summary>
    /// Recieves the server callback from the Docking.
    /// </summary>
    /// <param name="functionId">The function id to run on the server.</param>
    void IServerCallback.ServerCallback(int functionId) {
        switch((ServerCall)functionId) {
            case ServerCall.OnActivation:
                OnActivation();
                break;
            case ServerCall.OnDeactivation:
                OnDeactivation();
                break;
        }
    }

    /// <summary>
    /// Server call on ability activation.
    /// </summary>
    private void OnActivation() {
        shieldCollider.SetActive(true);
    }

    /// <summary>
    /// Server call on ability deactivation.
    /// </summary>
    private void OnDeactivation() {
        shieldCollider.SetActive(false);
        redirectedObjects.Clear();
    }
}
