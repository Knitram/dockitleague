﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerSaw : Ability, ISpawnableProvider, IServerCallback {
    public float triggerToSpawnTime;
    public float sawDamage;
    public string animatorBool;

    public Collider leftBlade;
    public Collider rightBlade;

    public GameObject bladePrefab;

    private List<PlayerHealth> playersHit;

    private enum ServerCall { OnActivation, OnSwingEnd };

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        if(dock.isServer) {
            playersHit = new List<PlayerHealth>();
        }
    }

    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();
            StartCoroutine(Activated());
            
            SetActive(true);
            docking.CmdSetActive(abilityId, true);
            docking.CmdServerCallback(abilityId, (int)ServerCall.OnActivation);
        }
    }

    /// <summary>
    /// Called from AbilityCooldown when the ability is ready. Setting active to false returns the sawblades to the docking kit visuals.
    /// </summary>
    public override void CooldownReady() {
        SetActive(false);
        docking.CmdSetActive(abilityId, false);
    }

    /// <summary>
    /// Local player call, started when ability activated. Delays the sawblade spawns.
    /// </summary>
    private IEnumerator Activated() {
        yield return new WaitForSeconds(triggerToSpawnTime);

        docking.CmdSpawnObject(abilityId, 0, leftBlade.transform.position, transform.eulerAngles);
        docking.CmdSpawnObject(abilityId, 0, rightBlade.transform.position, transform.eulerAngles);

        docking.CmdServerCallback(abilityId, (int)ServerCall.OnSwingEnd);
    }

    public override void SetActive(bool state) {
        if(!string.IsNullOrEmpty(animatorBool)) {
            animator.SetBool(animatorBool, state);
        }
    }

    void OnTriggerEnter(Collider other) {
        if(!docking.isServer) {
            return;
        }

        PlayerHealth health = other.GetComponent<PlayerHealth>();
        if(health != null && docking.CheckDamagable(health.GetComponent<Player>()) && !playersHit.Contains(health)) {
            health.TakeDamage(sawDamage, docking.netId.Value);
            playersHit.Add(health);
        }
    }

    GameObject ISpawnableProvider.GetSpawnablePrefab(int spawnableId) {
        return bladePrefab;
    }

    /// <summary>
    /// Recieves the server callback from the Docking.
    /// </summary>
    /// <param name="functionId">The function id to run on the server.</param>
    void IServerCallback.ServerCallback(int functionId) {
        switch((ServerCall)functionId) {
            case ServerCall.OnActivation:
                OnActivation();
                break;
            case ServerCall.OnSwingEnd:
                OnSwingEnd();
                break;
        }
    }

    /// <summary>
    /// Server call on ability activation.
    /// </summary>
    private void OnActivation() {
        leftBlade.enabled = true;
        rightBlade.enabled = true;
    }

    /// <summary>
    /// Server call on the end of the saw arm swing.
    /// </summary>
    private void OnSwingEnd() {
        leftBlade.enabled = false;
        rightBlade.enabled = false;
        playersHit.Clear();
    }
}
