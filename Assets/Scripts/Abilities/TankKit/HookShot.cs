﻿using System.Collections;
using UnityEngine;

public class HookShot : Ability, IServerCallback<Vector3, Vector3>, IClientCallback, IClientCallback<Vector3>, IClientCallback<Vector3, Vector3> {
    public string animatorBool;

    public Transform hookSpawnPoint;
    public Collider hook;

    public LineRenderer lineRenderer;

    public float hookSpeed = 40f;
    public float hookReturnSpeed = 30f;
    public float hookRange = 80f;
    public float hookPullForce = 5.8f;
    public float hookOnHitHoldTime = 0.5f;

    public ModifierInfo hookModifier;

    private Vector3 oldPosition;
    private Vector3 targetDirection;
    private bool active;

    private Coroutine activeCoroutine;
    private Transform sawblade;

    private enum ClientCall { Shoot, Return, Hit }

    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();
            
            SetActive(true);
            docking.SetPlayerInputRestriction(true, InputType.Rotation, InputType.Movement);

            docking.CmdSetActive(abilityId, true);
            Shoot(hook.transform.position, transform.forward);

            docking.CmdServerCallbackTwoVector3(abilityId, 0, hook.transform.position, transform.forward);
        }
    }

    public override void SetActive(bool state) {
        if(!string.IsNullOrEmpty(animatorBool)) {
            animator.SetBool(animatorBool, state);
        }
    }

    /// <summary>
    /// Recieves the server callback from the Docking.
    /// </summary>
    /// <param name="functionId">The function id to run on the server.</param>
    /// <param name="position">The hook's launched position.</param>
    /// <param name="direction">The launch direction.</param>
    void IServerCallback<Vector3, Vector3>.ServerCallback(int functionId, Vector3 position, Vector3 direction) {
        docking.RpcClientCallbackTwoVector3(abilityId, (int)ClientCall.Shoot, position, direction);
    }

    /// <summary>
    /// Recieves the client callback from the Docking.
    /// </summary>
    /// <param name="functionId">The function id to run on the server.</param>
    void IClientCallback.ClientCallback(int functionId) {
        if(docking.isServer) {
            return;
        }

        switch((ClientCall)functionId) {
            case ClientCall.Return:
                Return();
                break;
        }
    }

    /// <summary>
    /// Recieves the client callback from the Docking.
    /// </summary>
    /// <param name="functionId">The function id to run on the server.</param>
    /// <param name="holdPosition">The hold position for the hook.</param>
    void IClientCallback<Vector3>.ClientCallback(int functionId, Vector3 holdPosition) {
        if(docking.isServer) {
            return;
        }

        switch((ClientCall)functionId) {
            case ClientCall.Hit:
                Hit(holdPosition);
                break;
        }
    }

    /// <summary>
    /// Recieves the client callback from the Docking.
    /// </summary>
    /// <param name="functionId">The function id to run on the server.</param>
    /// <param name="position">The hook's launched position.</param>
    /// <param name="direction">The launch direction.</param>
    void IClientCallback<Vector3, Vector3>.ClientCallback(int functionId, Vector3 position, Vector3 direction) {
        switch((ClientCall)functionId) {
            case ClientCall.Shoot:
                if(!docking.hasAuthority) {
                    Shoot(position, direction);
                }
                break;
        }
    }

    void OnTriggerEnter(Collider other) {
        if(!docking.isServer || !active) {
            return;
        }

        //Player check
        PlayerStatus player = other.GetComponent<PlayerStatus>();
        if(player != null && docking.CheckDamagable(player.GetComponent<Player>())) {
            player.ApplyModifier(hookModifier);
            OnHookHit();
            return;
        }

        //Obstacle check
        if(other.CompareTag("Obstacle")) {
            OnHookHit();
            return;
        }

        //Hookable check
        IHookable blade = other.GetComponent<IHookable>();
        if(blade != null) {
            blade.Hooked(docking.gameObject, hook.transform);
            Return();
            docking.RpcClientCallback(abilityId, (int)ClientCall.Return);
            active = false;
        }
    }

    /// <summary>
    /// Server call for when the hook hit something.
    /// </summary>
    private void OnHookHit() {
        docking.GetComponent<Player>().TargetAddForce(docking.GetConnectionToClient(), (hookSpawnPoint.position - hook.transform.position).magnitude * hookPullForce, ForceMode.VelocityChange, hook.transform.position);
        Hit(hook.transform.position);
        docking.RpcClientCallbackVector3(abilityId, (int)ClientCall.Hit, hook.transform.position);
        active = false;
    }

    /// <summary>
    /// ClientCallback that launches the hook.
    /// </summary>
    /// <param name="position">The hook's launched position.</param>
    /// <param name="direction">The launch direction.</param>
    private void Shoot(Vector3 position, Vector3 direction) {
        hook.enabled = true;
        hook.transform.position = position;
        hook.transform.localEulerAngles = direction;

        active = true;
        lineRenderer.enabled = true;

        targetDirection = direction;
        activeCoroutine = StartCoroutine(Active());
    }

    /// <summary>
    /// ClientCallback called when the hook has hit something, and should freeze position for a bit.
    /// </summary>
    /// <param name="holdPosition">The hold position for the hook.</param>
    private void Hit(Vector3 holdPosition) {
        StopCoroutine(activeCoroutine);
        active = false;
        hook.enabled = false;
        StartCoroutine(HoldHit(holdPosition, hookOnHitHoldTime));
    }

    /// <summary>
    /// ClientCallback called when the hook should return.
    /// </summary>
    private void Return() {
        StopCoroutine(activeCoroutine);
        active = false;
        hook.enabled = false;
        StartCoroutine(Returning());
    }

    /// <summary>
    /// Called by every client when the hook has returned to the docking kit.
    /// </summary>
    private void Returned() {
        lineRenderer.enabled = false;
        hook.transform.position = hookSpawnPoint.position;

        if(docking.hasAuthority) {
            docking.SetPlayerInputRestriction(false, InputType.Rotation, InputType.Movement);
            SetActive(false);
            docking.CmdSetActive(abilityId, false);
        }
    }

    /// <summary>
    /// Started by every client when the hook has been launched.
    /// </summary>
    private IEnumerator Active() {
        oldPosition = hook.transform.position;
        while((hook.transform.position - hookSpawnPoint.position).sqrMagnitude < hookRange) {
            hook.transform.position = oldPosition + targetDirection * hookSpeed * Time.deltaTime;
            oldPosition = hook.transform.position;

            lineRenderer.SetPosition(0, hookSpawnPoint.position);
            lineRenderer.SetPosition(1, hook.transform.position);
            yield return null;
        }

        if(docking.isServer) {
            Return();
            docking.RpcClientCallback(abilityId, (int)ClientCall.Return);
        }
    }

    /// <summary>
    /// Started by every client when the hook should hold the same position.
    /// </summary>
    /// <param name="targetPosition">The position to keep.</param>
    /// <param name="duration">Keep position for duration.</param>
    private IEnumerator HoldHit(Vector3 targetPosition, float duration) {
        float timer = 0f;
        while(timer < duration) {
            hook.transform.position = targetPosition;
            timer += Time.deltaTime;

            lineRenderer.SetPosition(0, hookSpawnPoint.position);
            lineRenderer.SetPosition(1, hook.transform.position);
            yield return null;
        }

        Return();
    }

    /// <summary>
    /// Started by every client when the hook should return.
    /// </summary>
    private IEnumerator Returning() {
        oldPosition = hook.transform.position;
        while((hookSpawnPoint.position - hook.transform.position).sqrMagnitude > 0.2f) {
            hook.transform.position = oldPosition + (hookSpawnPoint.position - hook.transform.position).normalized * hookReturnSpeed * Time.deltaTime;
            oldPosition = hook.transform.position;

            lineRenderer.SetPosition(0, hookSpawnPoint.position);
            lineRenderer.SetPosition(1, hook.transform.position);
            yield return null;
        }

        Returned();
    }
}
