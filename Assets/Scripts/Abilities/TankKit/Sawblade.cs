﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Sawblade : SpawnableObject, IHookable {
    public float force = 30f;
    public float damage = 20f;
    public float lifetime = 10f;

    public float cooldownReduction = 3f;

    private bool active;
    private Rigidbody rigidBody;

    private Coroutine activeCheck;

    [ServerCallback]
    void Awake() {
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.AddForce(transform.forward * force, ForceMode.VelocityChange);
        active = true;
        activeCheck = StartCoroutine(ActiveCheck());
    }

    /// <summary>
    /// Keeps track of the state of the sawblade (if it's still moving).
    /// </summary>
    /// <returns></returns>
    [Server]
    private IEnumerator ActiveCheck() {
        //Delay the check for a second, firstly because velocity is 0 for the first (few) frames, thus triggering disable right away.
        //Secondly because the check is only necessary when slowing down anyways.
        yield return new WaitForSeconds(1f);

        while(active) {
            if(rigidBody.velocity.sqrMagnitude < 0.5f) {
                //Disable and enable to get new OnTriggerEnter calls.
                GetComponent<Collider>().enabled = false;
                GetComponent<Collider>().enabled = true;
                active = false;
            }
            yield return null;
        }
        yield return new WaitForSeconds(lifetime);
        NetworkServer.Destroy(gameObject);
    }

    /// <summary>
    /// Has two different behaviours depending on the sawblade state.
    /// </summary>
    /// <param name="other"></param>
    [ServerCallback]
    void OnTriggerEnter(Collider other) {
        if(active) {
            if(other.CompareTag("Obstacle")) {
                GetComponent<Rigidbody>().velocity = Vector3.zero;
                active = false;
                rigidBody.isKinematic = true;
                return;
            }

            PlayerHealth health = other.GetComponent<PlayerHealth>();
            if(health != null && CheckDamagable(health)) {
                health.TakeDamage(damage, playerId);
                NetworkServer.Destroy(gameObject);
            }
        } else {
            Docking docking = other.GetComponent<Docking>();

            if(docking != null) {
                DockingKit dockingKit = docking.GetDockingKit();
                for(int i = 0; i < dockingKit.abilities.Count; i++) {
                    if(dockingKit.abilities[i] is PowerSaw) {
                        docking.TargetReduceCooldown(docking.GetConnectionToClient(), i, cooldownReduction);
                        NetworkServer.Destroy(gameObject);
                        break;
                    }
                }
            }
        }
    }

    /// <summary>
    /// IHookable called when the sawblade has been hooked. 
    /// </summary>
    /// <param name="playerObject">The hook's assosiated player object.</param>
    /// <param name="hook">The hook transform.</param>
    [Server]
    public void Hooked(GameObject playerObject, Transform hook) {
        StopCoroutine(activeCheck);
        active = false;
        rigidBody.velocity = Vector3.zero;
        rigidBody.isKinematic = true;

        transform.SetParent(hook);
        transform.localPosition = Vector3.zero;
        RpcSetParent(playerObject);
    }

    /// <summary>
    /// ClientRpc called whenever the sawblade changes parent. Synchronizes sawblade position by setting parent.
    /// </summary>
    /// <param name="playerObject">The hook's assosiated player object.</param>
    [ClientRpc]
    private void RpcSetParent(GameObject playerObject) {
        if(playerObject != null) {
            foreach(Ability ab in playerObject.GetComponent<Docking>().GetDockingKit().abilities) {
                HookShot hookShot = ab as HookShot;
                if(hookShot != null) {
                    Rigidbody rbody = GetComponent<Rigidbody>();
                    rbody.velocity = Vector3.zero;
                    rbody.isKinematic = true;
                    transform.SetParent(hookShot.hook.transform);
                    transform.localPosition = Vector3.zero;
                    break;
                }
            }
        }
    }
}
