﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenFlash : MonoBehaviour {
    public float fadeSpeed = 5f;

    private Image whiteImage;
    private float currentAlpha = 1f;

    void Start() {
        whiteImage = GetComponent<Image>();
    }

    void Update() {
        currentAlpha -= Time.deltaTime * fadeSpeed;
        whiteImage.color = new Color(whiteImage.color.r, whiteImage.color.g, whiteImage.color.b, currentAlpha);

        if(currentAlpha < 0) {
            Destroy(gameObject);
        }
    }
}
