﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashGrenadeSpawner : Ability, ISpawnableProvider {
    public GameObject flashGrenadePrefab;
    public float offset = 5;

    /// <summary>
    /// Callback for what this ability should do once its associated button has been pressed
    /// </summary>
    public override void ButtonDown() {
        if (cooldown.IsReady()) {
            cooldown.Activate();
            SetActive();
            docking.CmdSetActive(abilityId, true);

            docking.CmdSpawnObject(abilityId, 0, transform.position + (transform.forward * offset), transform.rotation.eulerAngles);
        }
    }

    public override void SetActive(bool state = false) {
        // Empty at the moment
    }

    GameObject ISpawnableProvider.GetSpawnablePrefab(int spawnableId) {
        return flashGrenadePrefab;
    }
}
