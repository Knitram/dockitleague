﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FlashGrenade : SpawnableObject, IReflectable {
    public float timeBeforeExplosion = 2f;
    public float initialSpeed = 5f;
    public SphereCollider explosionCollider;
    public GameObject visuals;
    public int lifeTimeAfterExplosion = 1;
    public float visionRadius = 20;
    public float lerpSpeed = 10f;
    public ParticleSystem explosionParticles;
    public ModifierInfo stunInfo;

    private float explosionTimer = 0f;
    private Rigidbody grenadeRB;
    private FieldOfView fov;
    private float lerpTimer = 0f;
    private bool isLerping = false;

    void Start() {
        explosionCollider.enabled = false;
        grenadeRB = GetComponent<Rigidbody>();
        grenadeRB.AddForce(transform.forward * initialSpeed, ForceMode.Impulse);
        fov = GetComponent<FieldOfView>();
        fov.enabled = false;
    }

    void Update() {
        explosionTimer += Time.deltaTime;

        if(explosionTimer >= timeBeforeExplosion) {
            if (!isLerping) {
                explosionCollider.enabled = true;
                fov.enabled = true;
                visuals.SetActive(false);
                isLerping = true;
                explosionParticles.Play();

                Destroy(gameObject, lifeTimeAfterExplosion);
            } else {
                lerpTimer += Time.deltaTime * lerpSpeed;
                fov.viewRadius = Mathf.Lerp(0, visionRadius, lerpTimer);
            }
        }
    }

    /// <summary>
    /// Callback for when the grenade explodes and finds players within its explosion radius.
    /// The function fires a raycast towards the player and makes sure there are no obstructions before checking if the player was facing the grenade.
    /// The player is stunned if they look towards the grenade as it explodes.
    /// </summary>
    /// <param name="other">The collider that has triggered this callback</param>
    [ServerCallback]
    void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            // If the player is facing towards the grenade it will stun the player
            var hit = new RaycastHit();
            var raycastSuccess = Physics.Raycast(transform.position, other.transform.position - transform.position, out hit, explosionCollider.radius);
            if (raycastSuccess && hit.transform.tag == other.tag && Vector3.Dot(other.transform.forward, transform.position - other.transform.position) > 0) {
                var playerStatus = other.GetComponent<PlayerStatus>();
                if (playerStatus != null && playerStatus.isServer) {
                    playerStatus.ApplyModifier(stunInfo);
                }
            }
        }
    }

    [Server]
    void IReflectable.ReflectVelocity() {
        grenadeRB.velocity = -grenadeRB.velocity;
    }
}
