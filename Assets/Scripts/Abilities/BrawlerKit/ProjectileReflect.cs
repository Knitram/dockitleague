﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileReflect : Ability, IModifierProvider {
    public Transform shieldTransform;

    public float fadeSpeed = 5f;
    public float fadeOutTimeOffset = 0.5f;

    public ModifierInfo buff;

    private bool shieldActive = false;
    private float lerpTimer = 0;
    private bool fadingIn = false;
    private float initialAlpha;

    private SpriteRenderer[] shieldVisuals;

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        initialAlpha = shieldTransform.gameObject.GetComponentInChildren<SpriteRenderer>().color.a;
        shieldVisuals = shieldTransform.gameObject.GetComponentsInChildren<SpriteRenderer>();
    }

    protected override void Update() {
        base.Update();

        if(shieldActive) {
            LerpShieldAlpha();
        }
    }

    /// <summary>
    /// Callback for what this ability should do once its associated button has been pressed
    /// </summary>
    public override void ButtonDown() {
        if (cooldown.IsReady()) {
            cooldown.Activate();
            SetActive();
            docking.CmdSetActive(abilityId, true);
            docking.CmdSetModifier(abilityId, 0, true);
        }
    }


    public override void SetActive(bool state = false) {
    }

    /// <summary>
    /// Callback for what this ability is supposed to do depending on given state. 
    /// </summary>
    /// <param name="state">Whether the ability is to be active or now</param>
    public override void SetModifier(bool state = false) {
        if(state) {
            ActivateBuff();
        } else {
            DisableBuff();
        }
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return buff;
    }

    private void ActivateBuff() {
        shieldActive = true;
        shieldTransform.gameObject.SetActive(true);
        fadingIn = true;
        StartCoroutine(StartFading(buff.duration - fadeOutTimeOffset));
    }

    private void DisableBuff() {
        shieldActive = false;
        shieldTransform.gameObject.SetActive(false);
        lerpTimer = 0;
    }

    /// <summary>
    /// Callback that triggers when any colliders intersect with the collider of the shield.
    /// Acquires the IReflectable interface from the intersected collider and calls its ReflectVelocity() if possible.
    /// </summary>
    /// <param name="other">The intersected collider</param>
    void OnTriggerEnter(Collider other) {
        if (docking.isServer) {
            IReflectable reflectableObj = other.attachedRigidbody.GetComponent<IReflectable>();
            SpawnableObject spawnableObj = other.GetComponent<SpawnableObject>();
            if (reflectableObj != null && ((spawnableObj != null && spawnableObj.GetOwnerPlayerId() != docking.netId.Value) || spawnableObj == null)) {
                reflectableObj.ReflectVelocity();
            }
        }
    }

    /// <summary>
    /// A function for interpolating the alpha value of the colors that are used in the shield's SpriteRenderers
    /// </summary>
    private void LerpShieldAlpha() {
        lerpTimer += Time.deltaTime * (fadingIn ? 1 : -1) * fadeSpeed;

        if (lerpTimer >= 0 && lerpTimer <= 1) {
            foreach (var renderer in shieldVisuals) {
                renderer.color = new Color(renderer.color.r, renderer.color.g, renderer.color.b, Mathf.Lerp(0, initialAlpha, lerpTimer));
            }
        }
    }

    /// <summary>
    /// A simple coroutine that tells this ability to fade out the shield after a given time
    /// </summary>
    /// <param name="inTime">The time we want to wait</param>
    /// <returns>WaitForSeconds(inTime)</returns>
    IEnumerator StartFading(float inTime) {
        yield return new WaitForSeconds(inTime);
        lerpTimer = 1f;
        fadingIn = false;
    }
}
