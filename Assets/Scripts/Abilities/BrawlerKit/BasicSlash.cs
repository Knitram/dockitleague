﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicSlash : Ability, IElement, IModifierProvider {
    public float damageDealt = 20f;
    public string animatorTrigger;
    public LifeStealBuff lifeStealBuff;

    private List<GameObject> playersHitBySwing = new List<GameObject>();

    public ElementalModifiers elementalModifiers = new ElementalModifiers();

    [HideInInspector]
    public bool swingActive;

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        elementalModifiers.Initialize();
    }

    protected override void Update() {
        base.Update();

#if UNITY_EDITOR 
        if (Input.GetKeyDown(KeyCode.E)) {
            GetComponent<IElement>().ApplyElement(ElementalContainer.ComboableElements.Electricity);
        } else if (Input.GetKeyDown(KeyCode.I)) {
            GetComponent<IElement>().ApplyElement(ElementalContainer.ComboableElements.Ice);
        } else if (Input.GetKeyDown(KeyCode.F)) {
            GetComponent<IElement>().ApplyElement(ElementalContainer.ComboableElements.Fire);
        } else if (Input.GetKeyDown(KeyCode.V)) {
            GetComponent<IElement>().ApplyElement(ElementalContainer.ComboableElements.Water);
        }
#endif
    }

    /// <summary>
    /// Method that calls the necessary commands to apply an elemental buff to this ability
    /// </summary>
    /// <param name="element">A new element that we want to apply</param>
    void IElement.ApplyElement(ElementalContainer.ComboableElements element) {
        elementalModifiers.ApplyElement(element, docking, abilityId);
    }

    /// <summary>
    /// Callback for what this ability should do once its associated button has been pressed
    /// </summary>
    public override void ButtonDown() {
        if (cooldown.IsReady()) {
            playersHitBySwing.Clear();
            cooldown.Activate();
            SetActive();
            docking.CmdSetActive(abilityId, true);
        }
    }

    /// <summary>
    /// Callback for what this ability is supposed to do depending on given state. 
    /// State is always false here
    /// </summary>
    /// <param name="state">Whether the ability is to be active or now</param>
    public override void SetActive(bool state = false) {
        swingActive = true;
        if (!string.IsNullOrEmpty(animatorTrigger)) {
            animator.SetTrigger(animatorTrigger);
        }
    }

    public override void SetModifier(bool state = false) {
        elementalModifiers.SetModifier(state);
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return elementalModifiers.GetModifierInfo(modifierId);
    }

    /// <summary>
    /// Callback that triggers when any colliders intersect with the compound collider of the axe.
    /// Handles dealing damage to whatever is hit and resetting any buffs.
    /// </summary>
    /// <param name="other">The intersected collider</param>
    void OnTriggerStay(Collider other) {
        // We only want to damage something if we are not hitting ourselves and we haven't hit this object in this swing already.
        if (swingActive && docking.isServer && other.gameObject != transform.parent.parent.gameObject && !playersHitBySwing.Contains(other.gameObject)) {
            playersHitBySwing.Add(other.gameObject);

            PlayerHealth phObj = other.GetComponent<PlayerHealth>();
            if (phObj != null && docking.CheckDamagable(phObj.GetComponent<Player>())) {
                phObj.TakeDamage(damageDealt * (lifeStealBuff.IsBuffActive() ? lifeStealBuff.damageMultiplier : 1), docking.netId.Value);
                // Heal player if lifestealing
                if (lifeStealBuff.IsBuffActive()) {
                    transform.parent.parent.GetComponent<PlayerHealth>().Heal(damageDealt * lifeStealBuff.damageMultiplier * lifeStealBuff.healPercentage);
                    docking.CmdSetModifier(lifeStealBuff.GetAbilityId(), lifeStealBuff.GetBuffModifierId(), false);
                }

                elementalModifiers.TransferElementalModifier(other, docking, abilityId);
            }
        }
    }

    /// <summary>
    /// Callback for what this ability is supposed to do locally when applying a element
    /// </summary>
    /// <param name="element"></param>
    public override void SetElement(ElementalContainer.ComboableElements element) {
        elementalModifiers.SetElement(element);
    }
}