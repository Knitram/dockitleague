﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeStealBuff : Ability, IModifierProvider {
    public float damageMultiplier = 1.5f;
    public float healPercentage = 0.5f;

    public SpriteRenderer[] axeVisuals;
    public ParticleSystem activeParticles;
    public Color axeColorWhileActive;

    public ModifierInfo buff;

    private Color initialColor;
    private bool buffActive = false;

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        initialColor = axeVisuals[0].color;
    }

    /// <summary>
    /// Callback for what this ability should do once its associated button has been pressed
    /// </summary>
    public override void ButtonDown() {
        if (cooldown.IsReady()) {
            cooldown.Activate();
            SetActive();
            docking.CmdSetActive(abilityId, true);
            docking.CmdSetModifier(abilityId, 0, true);
        }
    }

    public override void SetActive(bool state = false) {
    }

    /// <summary>
    /// Callback for what this ability should do when a new modifier state is set
    /// </summary>
    /// <param name="state">The modifier state</param>
    public override void SetModifier(bool state = false) {
        buffActive = state;
        activeParticles.gameObject.SetActive(state);

        foreach (var renderer in axeVisuals) {
            renderer.color = state ? axeColorWhileActive : initialColor;
        }
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return buff;
    }

    /// <summary>
    /// A simple getter function for whether the life steal buff is currently active
    /// </summary>
    /// <returns>Whether the buff is currently active</returns>
    public bool IsBuffActive() {
        return buffActive;
    }
    
    public int GetAbilityId() {
        return abilityId;
    }

    public int GetBuffModifierId() {
        return 0;
    }
}
