﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAbility : Ability {
    public float damage = 10f;
    public string animatorTrigger;
    public AudioClip audioClip;

    private AudioSource audioSource;

    void Start() {
        audioSource = GetComponent<AudioSource>();
    }

    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();

            //Debug.Log("SetActive locally");
            SetActive();
            docking.CmdSetActive(abilityId, true);
        }
    }

    public override void SetActive(bool state = false) {
        if(!string.IsNullOrEmpty(animatorTrigger)) {
            animator.SetTrigger(animatorTrigger);
        }
        audioSource.PlayOneShot(audioClip);
    }

    void OnTriggerEnter(Collider other) {
        if(!docking.isServer) {
            return;
        }

        PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();
        if(playerHealth != null && docking.CheckDamagable(playerHealth.GetComponent<Player>())) {
            playerHealth.TakeDamage(damage, docking.netId.Value);
        }
    }
}
