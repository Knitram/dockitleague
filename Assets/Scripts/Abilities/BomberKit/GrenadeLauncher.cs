﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeLauncher : Ability, ISpawnableProvider {
    public string animatorTrigger;
    public float spawnOffset;
    public GameObject shellPrefab;
    
    public override void ButtonDown() {
        if (cooldown.IsReady()) {
            cooldown.Activate();

            SetActive();
            docking.CmdSetActive(abilityId, true);
            Fire();
        }
    }

    public override void SetActive(bool state = false) {
        if (!string.IsNullOrEmpty(animatorTrigger)) {
            animator.SetTrigger(animatorTrigger);
        }
    }

    /// <summary>
    /// </summary>
    public void Fire() {
        docking.CmdSpawnObject(abilityId, 0, transform.position + transform.forward * spawnOffset, transform.rotation.eulerAngles);
    }

    GameObject ISpawnableProvider.GetSpawnablePrefab(int spawnableId) {
        return shellPrefab;
    }
}
