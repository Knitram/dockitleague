﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ExplosiveMine : SpawnableObject {
    public float baseDamage;
    public float maxDamageTapering;
    public float explosionForce;
    public float explosionRadius;
    public float activationTime;
    public string animationTrigger;
    public GameObject explodeSprite;
    public GameObject mineSprite;
    public Animator animator;

    [HideInInspector] public int myId;
    [HideInInspector] public GameObject spawnerReference;

    private SpriteRenderer visuals;
    
    public void Initialize(GameObject owner) {
        spawnerReference = owner;
        animator = GetComponent<Animator>();
    }

    public void Start() {
        StartCoroutine(ActivationDelay(activationTime));
    }

    [ServerCallback]
    public void OnTriggerEnter(Collider other) {
        Player player = other.GetComponent<Player>();
        if (player != null && CheckDamagable(other.GetComponent<Player>())) {
            Explode();
        }
    }

    /// <summary>
    /// Explode the mine and affect the valid colliders/rigidbodies within explosionRadius
    /// </summary>
    private void Explode() {
        if(hasAuthority) {
            mineSprite.SetActive(false);
        }

        explodeSprite.SetActive(true);
        RpcExplodeVisualState(true);
        RpcPlayAnimation();
        Collider[] bodies = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach (Collider body in bodies) {
            if (body.CompareTag("Player")) {
                Player other = body.GetComponent<Player>();
                PlayerHealth otherHealth = body.GetComponent<PlayerHealth>();

                if (other != null && otherHealth != null && (other.netId.Value == this.playerId || CheckDamagable(other))) {

                    other.TargetAddExplosionForce(other.NetworkPlayerInstance.connectionToClient, explosionForce, transform.position, explosionRadius);
                    float damageToTake = Mathf.Clamp(baseDamage / (other.transform.position - transform.position).sqrMagnitude, baseDamage * maxDamageTapering, baseDamage);

                    if(other.netId.Value == this.playerId) {
                        damageToTake /= 2;
                    }
       
                    otherHealth.TakeDamage(damageToTake, playerId);
                }
            }
        }
        //RpcRemoveMine();
    }

    /// <summary>
    /// Destroy the mine and remove it from the list of mines.
    /// </summary>
    [ClientRpc]
    public void RpcRemoveMine() {
        if(spawnerReference != null) {
            spawnerReference.GetComponent<ExplosiveMineSpawner>().RemoveMine(myId);
        }
    }

    [ClientRpc]
    public void RpcPlayAnimation() {
        if(!string.IsNullOrEmpty(animationTrigger)) {
            animator.SetTrigger(animationTrigger);
        }
    }

    [ClientRpc]
    public void RpcMineVisualState(bool state) {
        mineSprite.SetActive(state);
    }

    [ClientRpc]
    public void RpcExplodeVisualState(bool state) {
        explodeSprite.SetActive(state);
    }

    /// <summary>
    /// Delay beforee the mine is active after placement.
    /// </summary>
    /// <param name="activationTime">Time until activation</param>
    /// <returns></returns>
    IEnumerator ActivationDelay(float activationTime) {
        yield return new WaitForSeconds(activationTime);
        GetComponent<SphereCollider>().enabled = true;
        RpcMineVisualState(false);
    }
}
