﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RemoteMine : SpawnableObject {
    public float baseDamage;
    public float explosionRadius;
    public float activationTime;
    private bool active = false;
    public ModifierInfo stunInfo;
    public GameObject mineSprite;
    private GameObject spawnerReference;

    public void Initialize(GameObject owner) {
        spawnerReference = owner;
    }
    
    private void Start() {
        StartCoroutine(ActivationDelay(activationTime));
    }
    

    /// <summary>
    /// Called when the remote mine is triggered, checking for enemy players in a sphere.
    /// </summary>
    [ServerCallback]
    public void Explode() {
        Collider[] bodies = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach(Collider body in bodies) {
            if(body.CompareTag("Player")) {
                PlayerHealth playerHealth = body.GetComponent<PlayerHealth>();
                PlayerStatus playerStatus = body.GetComponent<PlayerStatus>();

                if (playerHealth != null && playerStatus != null && CheckDamagable(body.GetComponent<Player>())) {
                    playerHealth.TakeDamage(baseDamage, playerId);
                    playerStatus.ApplyModifier(stunInfo);
                }
            }
        }
        Destroy(this.gameObject);
    }
    
    [ClientRpc]
    public void RpcMineVisualState(bool state) {
        if (!hasAuthority) {
            mineSprite.SetActive(state);
        }
    }
    
    /// <summary>
    /// Delay beforee the mine is active after placement.
    /// </summary>
    /// <param name="activationTime">Time until activation</param>
    /// <returns></returns>
    IEnumerator ActivationDelay(float activationTime) {
        yield return new WaitForSeconds(activationTime);
        active = true;
        RpcMineVisualState(active);
    }

    public bool IsActive() {
        return active;
    }

}
