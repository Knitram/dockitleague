﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ExplosiveMineSpawner : Ability, ISpawnableReferenceProvider {
  
    public string animatorTrigger;
    public GameObject[] minePrefab;
    public int maxMineAmount;
    private List<GameObject> mines = new List<GameObject>();
    
    public override void ButtonDown() {
        if (cooldown.IsReady()) {
            cooldown.Activate();

            SetActive();
            docking.CmdSetActive(abilityId, true);

            docking.CmdSpawnObjectReference(abilityId, 0, transform.position, (transform.rotation * minePrefab[0].transform.rotation).eulerAngles);
        }
    }

    public override void SetActive(bool state = false) {
        if (!string.IsNullOrEmpty(animatorTrigger)) {
            animator.SetTrigger(animatorTrigger);
        }
    }
    
    /// <summary>
    /// Set the mine's ID and reference to this gameobject containing the list.
    /// Removes the first placed mine if mine limit is exceeded.
    /// </summary>
    /// <param name="spawnedObject">the mine that got spawned</param>
    void ISpawnableReferenceProvider.SetSpawnedObjectReference(GameObject spawnedObject) {
        spawnedObject.GetComponent<ExplosiveMine>().myId = spawnedObject.GetInstanceID();;
        spawnedObject.GetComponent<ExplosiveMine>().Initialize(this.gameObject);

        mines.Add(spawnedObject.gameObject);

        if(mines.Count > maxMineAmount) {
            docking.CmdDestroyObject(mines[0]);
            mines.RemoveAt(0);
        }
    }

    GameObject ISpawnableProvider.GetSpawnablePrefab(int spawnableId) {
        return minePrefab[spawnableId];
    }

    /// <summary>
    /// Removes the mine that got triggered
    /// </summary>
    /// <param name="mineId">The ID of the mine.</param>
    public void RemoveMine(int mineId) {
        int index = mines.FindIndex(i => i.GetComponent<ExplosiveMine>().myId.Equals(mineId));
        docking.CmdDestroyObject(mines[index]);
        mines.Remove(mines[index]);
    }

    /// <summary>
    /// Clean up mines when docking kit is not equipped anymore.
    /// </summary>
    public void OnDestroy() {
        while (mines.Count > 0) {
            docking.CmdDestroyObject(mines[0]);
            mines.RemoveAt(0);
        }
    }
}
