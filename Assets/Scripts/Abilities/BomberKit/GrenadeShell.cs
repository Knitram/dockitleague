﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GrenadeShell : SpawnableObject {
    public float launchForce;
    public float lifetime;
    public float explosionRadius;
    public float explosionForce;
    public float baseDamage;

	void Start () {
        GetComponent<Rigidbody>().AddForce(transform.forward * launchForce, ForceMode.Impulse);
        StartCoroutine(Expire(lifetime));
	}
	
    /// <summary>
    /// On collision explode if enemy player
    /// </summary>
    /// <param name="other">the other collider</param>
    private void OnCollisionEnter(Collision other) {
        foreach(ContactPoint contactPoint in other.contacts) {
            Player player = contactPoint.otherCollider.GetComponent<Player>();
            if(player != null && CheckDamagable(contactPoint.otherCollider.GetComponent<Player>())) {
                Explode();
            }
        }
    }

    /// <summary>
    /// Apply explosion to all enemy players within explosionRadius and self.
    /// </summary>
    [ServerCallback]
    private void Explode() {
        Collider[] bodies = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach (Collider body in bodies) {
            Player player = body.GetComponent<Player>();
            PlayerHealth playerHealth = body.GetComponent<PlayerHealth>();
            if (player != null && playerHealth != null) {

                if (player.netId.Value == this.playerId) {
                    playerHealth.TakeDamage(baseDamage / 2f, playerId);
                    player.TargetAddExplosionForce(player.NetworkPlayerInstance.connectionToClient, explosionForce, transform.position, explosionRadius);

                } else if (player.GetPlayerTeamId() != this.teamId) {
                    playerHealth.TakeDamage(baseDamage, playerId);
                    player.TargetAddExplosionForce(player.NetworkPlayerInstance.connectionToClient, explosionForce, transform.position, explosionRadius);
                }
            }
        }
        Destroy(this.gameObject);
    }

    /// <summary>
    /// Explode and destroy the grenade shell when lifetime runs out.
    /// </summary>
    /// <param name="lifetime">How long the shell should live</param>
    /// <returns></returns>
    IEnumerator Expire(float lifetime) {
        yield return new WaitForSeconds(lifetime);
        Explode();
        Destroy(this.gameObject);
    }
}
