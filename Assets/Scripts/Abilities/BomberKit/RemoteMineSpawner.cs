﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteMineSpawner : Ability, ISpawnableReferenceProvider, IServerCallback<GameObject> {
    public string animatorTrigger;
    public GameObject[] minePrefab;
    public GameObject remoteMineReference;

    private enum ServerCall { Explode };

    public override void ButtonDown() {
 
        if (cooldown.IsReady() && remoteMineReference == null) {
            cooldown.Activate();

            SetActive();
            docking.CmdSetActive(abilityId, true);

            docking.CmdSpawnObjectReference(abilityId, 0, transform.position, (transform.rotation * minePrefab[0].transform.rotation).eulerAngles);
        }

        if (remoteMineReference != null) {
            if (!remoteMineReference.GetComponent<RemoteMine>().IsActive()) {
                Debug.Log("remote mine is not active yet...");
                return;
            }
            docking.CmdServerCallbackGameObject(abilityId, (int)ServerCall.Explode, remoteMineReference);
            docking.CmdDestroyObject(remoteMineReference);
        }
    }

    public override void SetActive(bool state = false) {
        if (!string.IsNullOrEmpty(animatorTrigger)) {
            animator.SetTrigger(animatorTrigger);
        }
    }

    /// <summary>
    /// Spawns the remote mine with reference
    /// </summary>
    /// <param name="spawnedObject">the mine that got spawned</param>
    void ISpawnableReferenceProvider.SetSpawnedObjectReference(GameObject spawnedObject) {
        remoteMineReference = spawnedObject;
        var mine = remoteMineReference.GetComponent<RemoteMine>();
        mine.Initialize(this.gameObject);

        if(remoteMineReference == null) {
            Debug.Log("remoteMineReference is null");
        }
    }

    GameObject ISpawnableProvider.GetSpawnablePrefab(int spawnableId) {
        return minePrefab[spawnableId];
    }

    void IServerCallback<GameObject>.ServerCallback(int functionId, GameObject go) {
        var mine = go.GetComponent<RemoteMine>();
        if(mine.IsActive()) {
            mine.Explode();
        }
    }
    
}
