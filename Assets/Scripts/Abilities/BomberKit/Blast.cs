﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blast : Ability {
    public float blastForce;
    public string animatorTrigger;
    private BoxCollider blastArea;

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        blastArea = GetComponent<BoxCollider>();
    }

    public override void ButtonDown() {
        if (cooldown.IsReady()) {
            cooldown.Activate();

            SetActive();
            docking.CmdSetActive(abilityId, true);

        }
    }

    public override void SetActive(bool state = false) {
        if (!string.IsNullOrEmpty(animatorTrigger)) {
            animator.SetTrigger(animatorTrigger);
        }
        StartCoroutine(activateCollider());
    }
    
    /// <summary>
    /// Handles blast area, ignore friendly players (still applies to self)
    /// </summary>
    /// <param name="other"></param>
    public void OnTriggerEnter(Collider other) {
        if(!docking.isServer) {
            return;
        }
        
        Player player = other.GetComponent<Player>();
        if (player != null) {
            if(!docking.CheckDamagable(other.GetComponent<Player>()) && player.netId.Value != this.docking.netId.Value) {
                return;
            }
            float distance = (other.transform.position - blastArea.transform.position).sqrMagnitude;

            player.TargetAddForce2(player.NetworkPlayerInstance.connectionToClient, blastForce / distance, ForceMode.Impulse, blastArea.transform.position);
        }
    }

    /// <summary>
    /// Activate the collider for a frame so that the collisions trigger once.
    /// </summary>
    /// <returns>wait for next frame</returns>
    private IEnumerator activateCollider() {
        blastArea.enabled = true;
        yield return null;
        blastArea.enabled = false;
    }
}
