﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangVision : Ability, IModifierProvider {
    public BoomerangThrow boomerangThrowScript;
    public MultiBoomerangBuff boomerangBuffScript;
    public GameObject visionIndicator;

    public float visionRadiusWhileActive = 10f;
    public float visionRadiusExtraBoomerangs = 5f;
    public float visionLerpSpeed = 5f;

    public ModifierInfo visionModifier;

    private FieldOfView[] boomerangFovs = new FieldOfView[BoomerangThrow.NUM_BOOMERANGS];

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        for (int i = 0; i < BoomerangThrow.NUM_BOOMERANGS; i++) {
            boomerangFovs[i] = boomerangThrowScript.boomerangObjs[i].GetComponentInChildren<FieldOfView>(true);
            if (boomerangFovs[i] != null && docking.hasAuthority) {
                boomerangFovs[i].enabled = true;
            }
        }
    }

    /// <summary>
    /// Callback for what this ability does locally when its associated button is pressed
    /// </summary>
    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();
            SetActive();
            docking.CmdSetModifier(abilityId, 0, true);
            docking.CmdSetActive(abilityId, true);
        }
    }

    public override void SetActive(bool state = false) {
    }
    
    /// <summary>
    /// Callback for what this ability is supposed to do when a modifier state changes
    /// </summary>
    /// <param name="state">The new modifier state</param>
    public override void SetModifier(bool state) {
        visionIndicator.SetActive(state);
        for (int i = 0; i < BoomerangThrow.NUM_BOOMERANGS; i++) {
            if (boomerangFovs[i].gameObject.activeInHierarchy && boomerangFovs[i].enabled) {
                if (state) {
                    boomerangFovs[i].SetViewRadius((i == 0) ? visionRadiusWhileActive : visionRadiusExtraBoomerangs, visionLerpSpeed);
                } else {
                    boomerangFovs[i].ResetViewRadius(visionLerpSpeed);
                }
            }
        }
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return visionModifier;
    }
}
