﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangThrow : Ability, IElement, IModifierProvider, IReflectable, IClientCallback {
    public List<LineRenderer> approximatePathRenderers = new List<LineRenderer>();
    public BoomerangDataContainer[] boomerangData = new BoomerangDataContainer[NUM_BOOMERANGS];
    public List<TrailRenderer> trailRenderers = new List<TrailRenderer>();

    public GameObject[] boomerangObjs = new GameObject[NUM_BOOMERANGS];
    public BoomerangRoot boomerangRootScript;
    public MultiBoomerangBuff boomerangBuffScript;
    public AnimationCurve velocityCurve;

    public float damageDealt = 10f;
    public float boomerangSpeed = 5f;
    public float spinMultiplierWhileActive = 4f;

    public const int NUM_BOOMERANGS = 3;

    public ElementalModifiers elementalModifiers = new ElementalModifiers();

    public AudioClip audioClip;

    private const int VERTEX_COUNT = 50;

    private bool interpolating = false;
    private float interpolationTimer = 0f;

    private bool keyHeld = false;
    private float spinSpeedWhileInactive;

    private AudioSource audioSource;

    #if UNITY_EDITOR
    // Performance testing variables
    private float animationCurveTime = 0f;
    private float mathematicalCurveTime = 0f;
    private int interpIterations = 0;
    #endif

    private bool reflected = false;

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        spinSpeedWhileInactive = boomerangObjs[0].GetComponentInChildren<ObjectSpinner>().rotationSpeed;
        elementalModifiers.Initialize();
        audioSource = GetComponent<AudioSource>();
    }

    protected override void Update() {
        base.Update();

        // Making sure that the lineRenderers are active if the key is held and the cooldown is ready
        if (cooldown != null && cooldown.IsReady()) {
            SetPathRendererVisibility(keyHeld);
        } else {
            SetPathRendererVisibility(false);
        }

        if (keyHeld) {
            DrawBezierCurve(0);
            if(boomerangBuffScript.buffActive) {
                DrawBezierCurve(1);
                DrawBezierCurve(2);
            }
        }

        if(interpolating) {
            interpolationTimer += Time.deltaTime * boomerangSpeed * (reflected ? -1 : 1);

            #if UNITY_EDITOR
            // Performance Testing
            /////////////////////////
            float startTime;
            float endTime;

            startTime = Time.realtimeSinceStartup;
            MathInterp(interpolationTimer);
            endTime = Time.realtimeSinceStartup;
            mathematicalCurveTime += endTime - startTime;

            startTime = Time.realtimeSinceStartup;
            velocityCurve.Evaluate(interpolationTimer);
            endTime = Time.realtimeSinceStartup;
            animationCurveTime += endTime - startTime;

            interpIterations++;
            /////////////////////////
            #endif

            if (velocityCurve.Evaluate(interpolationTimer) >= 1f || velocityCurve.Evaluate(interpolationTimer) <= 0) {
                #if UNITY_EDITOR
                Debug.Log("AnimationCurve average performance time in μs: " + ((animationCurveTime / interpIterations) * 1000000).ToString("f2"));
                Debug.Log("MathematicalCurve average performance time in μs: " + ((mathematicalCurveTime / interpIterations) * 1000000).ToString("f2"));
                #endif

                ResetBoomerang();
            } else {
                InterpolateBoomerang(0);
                if(boomerangBuffScript.buffApplied) {
                    InterpolateBoomerang(1);
                    InterpolateBoomerang(2);
                }
            }
        }
    }

    /// <summary>
    /// An alternative to AnimationCurve.Evaluate(t);
    /// </summary>
    /// <param name="t">The input time</param>
    /// <returns>The smoothed time</returns>
    private float MathInterp(float t) {
        return t + Mathf.Sin(6.28f * t) / 9;
    }

    /// <summary>
    /// Callback for what this ability does locally when its associated button is pressed
    /// </summary>
    public override void ButtonDown() {
        keyHeld = true;
    }

    /// <summary>
    /// Callback for what this ability does locally when its associated button is released
    /// </summary>
    public override void ButtonUp() {
        keyHeld = false;
        if (cooldown.IsReady()) {
            cooldown.Activate();
            SetActive(true);
            docking.CmdSetActive(abilityId, true);
        }
    }

    /// <summary>
    /// Callback for what this ability is supposed to do locally on all clients when the ability state is changed
    /// </summary>
    /// <param name="state">The new ability state</param>
    public override void SetActive(bool state = false) {
        if (state) {
            interpolating = true;
            audioSource.PlayOneShot(audioClip);

            for (int i = 0; i < NUM_BOOMERANGS; i++) {
                for (int j = 0; j < BoomerangDataContainer.NUM_CONTROL_POINTS; j++) {
                    boomerangData[i].storedPositions[j] = boomerangData[i].bezierControlPoints[j].transform.position;
                }
                boomerangObjs[i].GetComponentInChildren<ObjectSpinner>().rotationSpeed = spinSpeedWhileInactive * spinMultiplierWhileActive;
            }

            foreach (var trailRenderer in trailRenderers) {
                trailRenderer.enabled = true;
            }

            // Setting variables for the additional boomerangs
            if (docking.isServer && boomerangBuffScript.buffActive) {
                boomerangBuffScript.buffApplied = true;
                docking.RpcClientCallback(boomerangBuffScript.GetAbilityId(), (int)MultiBoomerangBuff.ClientCallback.BuffApplied);
                docking.CmdSetModifier(boomerangBuffScript.GetAbilityId(), boomerangBuffScript.GetBuffModifierId(), false);
            }
        }
    }

    /// <summary>
    /// Callback for what this ability is supposed to do when a modifier state changes
    /// </summary>
    /// <param name="state">The new modifier state</param>
    public override void SetModifier(bool state = false) {
        elementalModifiers.SetModifier(state);
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return elementalModifiers.GetModifierInfo(modifierId);
    }

    /// <summary>
    /// Handles application of the root modifier, player damage and transferring elemental modifiers
    /// </summary>
    /// <param name="other">The collider that triggered this callback</param>
    void OnTriggerEnter(Collider other) {
        if(docking != null && docking.isServer && other.gameObject != transform.parent.parent.gameObject && other.CompareTag("Player")) {
            if (boomerangRootScript.rootActive) {
                other.GetComponent<PlayerStatus>().ApplyModifier(boomerangRootScript.rootInfo);
                return;
            }
            if (interpolating) {
                PlayerHealth phObj = other.GetComponent<PlayerHealth>();
                if (phObj != null && docking.CheckDamagable(phObj.GetComponent<Player>())) {
                    phObj.TakeDamage(damageDealt, docking.netId.Value);
                    elementalModifiers.TransferElementalModifier(other, docking, abilityId);
                }
            }
        }
    }

    // Based on http://www.theappguruz.com/blog/bezier-curve-in-games
    /// <summary>
    /// Generates positions on the lineRenderer by using a loop to interpolate through the bezier curve
    /// </summary>
    private void DrawBezierCurve(int index) {
        for (int i = 1; i <= VERTEX_COUNT; i++) {
            float t = i / (float)VERTEX_COUNT;

            Vector3 pixel = BezierCurve.CubicInterpolation(boomerangData[index].bezierControlPoints[0].position,
                                                           boomerangData[index].bezierControlPoints[1].position,
                                                           boomerangData[index].bezierControlPoints[2].position,
                                                           boomerangData[index].bezierControlPoints[3].position, 
                                                           t);

            approximatePathRenderers[index].positionCount = i;
            approximatePathRenderers[index].SetPosition((i - 1), pixel);
        }
    }

    /// <summary>
    /// Resets all member variables to initial values
    /// </summary>
    private void ResetBoomerang() {
        interpolationTimer = 0;
        interpolating = false;
        reflected = false;
        audioSource.Stop();

        for (int i = 0; i < NUM_BOOMERANGS; i++) {
            boomerangObjs[i].GetComponentInChildren<ObjectSpinner>().rotationSpeed = spinSpeedWhileInactive;
            boomerangObjs[i].transform.position = boomerangData[i].bezierControlPoints[3].position;
        }
        foreach (var trailRenderer in trailRenderers) {
            trailRenderer.enabled = false;
        }

        if(boomerangBuffScript.buffApplied) {
            StartCoroutine(boomerangBuffScript.ResetBuff());
        }
    }

    /// <summary>
    /// Method that calls the necessary commands to apply an elemental buff to this ability
    /// </summary>
    /// <param name="element">A new element that we want to apply</param>
    void IElement.ApplyElement(ElementalContainer.ComboableElements element) {
        elementalModifiers.ApplyElement(element, docking, abilityId);
    }

    /// <summary>
    /// Callback for what this ability is supposed to do locally when applying a element
    /// </summary>
    /// <param name="element"></param>
    public override void SetElement(ElementalContainer.ComboableElements element) {
        elementalModifiers.SetElement(element);
    }

    /// <summary>
    /// Interpolates the position of a boomerang on a cubic bezier curve
    /// </summary>
    /// <param name="index">The array index of the boomerang we want to interpolate</param>
    private void InterpolateBoomerang(int index) {
        // We interpolate using our current position as the start/end point combined with the stored handles at the time of throwing.
        // This provides a decent looking curve interpolation regardless of how the player moves.
        boomerangObjs[index].transform.position = BezierCurve.CubicInterpolation(boomerangData[index].bezierControlPoints[0].position,
                                                                                 boomerangData[index].storedPositions[1],
                                                                                 boomerangData[index].storedPositions[2],
                                                                                 boomerangData[index].bezierControlPoints[3].position,
                                                                                 velocityCurve.Evaluate(interpolationTimer));
    }

    /// <summary>
    /// Sets the visibility of the lineRenderers to the given parameter
    /// </summary>
    /// <param name="state">The visibility state of the lineRenderers</param>
    private void SetPathRendererVisibility(bool state) {
        approximatePathRenderers[0].gameObject.SetActive(state);
        if (boomerangBuffScript.buffActive) {
            approximatePathRenderers[1].gameObject.SetActive(state);
            approximatePathRenderers[2].gameObject.SetActive(state);
        }
    }

    void IReflectable.ReflectVelocity() {
        reflected = true;
        docking.RpcClientCallback(abilityId, 0);
    }

    void IClientCallback.ClientCallback(int functionId) {
        reflected = true;
    }
}

// We cannot see 2d/jagged arrays in the Unity inspector so this tiny class is a workaround for that
[System.Serializable]
public class BoomerangDataContainer {
    public const int NUM_CONTROL_POINTS = 4;

    public Transform[] bezierControlPoints = new Transform[NUM_CONTROL_POINTS];
    public Vector3[] storedPositions = new Vector3[NUM_CONTROL_POINTS];
}
