﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangRoot : Ability {
    public ModifierInfo rootInfo;
    public SpriteRenderer[] rootIndicators;
    public Animator animationController;

    public float activeDuration = 0.5f;
    public string animationTrigger = "Root";

    public AudioClip audioClip;

    public Color activeColor;
    private Color defaultColor;

    private AudioSource audioSource;

    [HideInInspector]
    public bool rootActive = false;

    private float durationTimer = 0f;

    void Start() {
        defaultColor = rootIndicators[0].color;
        audioSource = GetComponent<AudioSource>();
    }

    protected override void Update() {
        base.Update();

        if(rootActive) {
            durationTimer += Time.deltaTime;
            if(durationTimer >= activeDuration) {
                ResetAbility();
            }
        }

        if(docking != null && docking.hasAuthority && cooldown.IsReady()) {
            foreach (var rootIndicator in rootIndicators) {
                rootIndicator.enabled = true;
            }
        } 
    }

    /// <summary>
    /// Callback for what this ability does locally when its associated button is pressed
    /// </summary>
    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();
            SetActive();
            docking.CmdSetActive(abilityId, true);
        }
    }

    /// <summary>
    /// Callback for what this ability is supposed to do locally on all clients when the ability state is changed
    /// </summary>
    /// <param name="state">The new ability state</param>
    public override void SetActive(bool state = false) {
        rootActive = true;
        audioSource.PlayOneShot(audioClip);
        if (!string.IsNullOrEmpty(animationTrigger)) {
            animationController.SetTrigger(animationTrigger);
        }
        foreach (var rootIndicator in rootIndicators) {
            rootIndicator.GetComponent<SphereCollider>().enabled = true;
            rootIndicator.color = activeColor;
            rootIndicator.transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// Resets all member variables to initial values
    /// </summary>
    private void ResetAbility() {
        rootActive = false;
        durationTimer = 0;
        foreach (var rootIndicator in rootIndicators) {
            rootIndicator.gameObject.GetComponent<SphereCollider>().enabled = false;
            rootIndicator.enabled = false;
            rootIndicator.color = defaultColor;
            rootIndicator.transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}
