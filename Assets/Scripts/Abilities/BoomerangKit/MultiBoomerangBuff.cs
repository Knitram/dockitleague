﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiBoomerangBuff : Ability, IClientCallback, IModifierProvider {
    public BoomerangThrow boomerangScript;
    public ModifierInfo buff;

    public GameObject[] otherBoomerangVisuals;

    public Animator boomerangAnimator;
    public AnimationClip boomerangAnimationClip;
    public string animationTrigger;

    public AudioClip audioClip;

    public enum ClientCallback { BuffApplied }

    [HideInInspector]
    public bool buffActive = false;
    [HideInInspector]
    public bool buffApplied = false;

    private AudioSource audioSource;

    void Start() {
        audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Callback for what this ability does locally when its associated button is pressed
    /// </summary>
    public override void ButtonDown() {
        if (cooldown.IsReady()) {
            cooldown.Activate();
            SetActive();
            docking.CmdSetActive(abilityId, true);
            docking.CmdSetModifier(abilityId, 0, true);
        }
    }

    public override void SetActive(bool state = false) {
    }

    /// <summary>
    /// Callback for what this ability is supposed to do when a modifier state changes
    /// </summary>
    /// <param name="state">The new modifier state</param>
    public override void SetModifier(bool state = false) {
        buffActive = state;

        if(state) {
            audioSource.PlayOneShot(audioClip);
            foreach (var boomerang in otherBoomerangVisuals) {
                boomerang.SetActive(true);
            }
            if (!string.IsNullOrEmpty(animationTrigger)) {
                animator.SetTrigger(animationTrigger);
            }
        } else if (!state && !buffApplied) {
            // If the buff naturally wears off we have to make sure we properly reset everything
            StartCoroutine(ResetBuff());
            boomerangScript.approximatePathRenderers[1].gameObject.SetActive(false);
            boomerangScript.approximatePathRenderers[2].gameObject.SetActive(false);
            audioSource.Stop();
        } else {
            audioSource.Stop();
        }
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return buff;
    }

    /// <summary>
    /// Callback that gets called on every client when the server calls docking.RpcClientCall(funcId) 
    /// </summary>
    /// <param name="functionId"></param>
    void IClientCallback.ClientCallback(int functionId) {
        if (functionId == (int)ClientCallback.BuffApplied) {
            buffApplied = true;
        }
    }

    /// <summary>
    /// Coroutine used for resetting any visuals to default state. 
    /// It waits for the end of the deactivation animation before doing anything.
    /// </summary>
    public IEnumerator ResetBuff() {
        animator.SetTrigger("MultiBuffDeactivate");
        yield return new WaitForSeconds(boomerangAnimationClip.length);
        foreach (var boomerang in otherBoomerangVisuals) {
            boomerang.SetActive(false);
        }
        buffApplied = false;
    }

    public int GetAbilityId() {
        return abilityId;
    }

    public int GetBuffModifierId() {
        return 0;
    }
}
