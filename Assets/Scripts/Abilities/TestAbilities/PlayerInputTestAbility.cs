﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputTestAbility : Ability {
    public Transform target;
    public float moveSpeed = 4f;
    public float maxDistance = 10f;

    private Vector3 targetDefaultPosition;

    private PlayerInput playerInput;
    private DockingKit dockingKit;

    private bool isActive;

    public override void InitializeLocalPlayer(AbilityUI abilityUI) {
        base.InitializeLocalPlayer(abilityUI);

        targetDefaultPosition = target.position;
        playerInput = docking.GetComponent<PlayerInput>();
        dockingKit = docking.GetDockingKit();
    }

    void LateUpdate() {
        if(isActive) {
            Vector3 directionVector = playerInput.GetDirectionVector();

            if(directionVector.sqrMagnitude > 0.01f) {
                target.Translate(directionVector * moveSpeed * Time.deltaTime, Space.World);

                //Restrict distance.
                Vector3 distanceVector = (target.position - transform.position);
                if(distanceVector.sqrMagnitude > maxDistance * maxDistance) {
                    target.position = transform.position + distanceVector.normalized * maxDistance;
                }
            }
        }
    }

    public override void ButtonDown() {
        if(!isActive) {
            dockingKit.SetAbilityLock(true, 0, 1, 2);
            docking.SetPlayerInputRestriction(true, InputType.Movement, InputType.Rotation);
            target.position = targetDefaultPosition;
            target.gameObject.SetActive(true);
        } else {
            dockingKit.SetAbilityLock(false, 0, 1, 2);
            docking.SetPlayerInputRestriction(false, InputType.Movement, InputType.Rotation);
            target.gameObject.SetActive(false);
        }

        isActive = !isActive;
    }

    public override void CancelAbility() {
        if(isActive) {
            dockingKit.SetAbilityLock(false, 0, 1, 2);
            docking.SetPlayerInputRestriction(false, InputType.Movement, InputType.Rotation);
            target.gameObject.SetActive(false);

            isActive = false;
        }
    }

    public override void SetActive(bool state = false) {
    }
}
