﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTestAbility : Ability, ISpawnableReferenceProvider {
    //If the ability has multiple spawnable prefabs, use list instead. (GetSpawnablePrefab() can use spawnableId to return correct prefab from list.) 
    public GameObject spawnTestPrefab;
    public string animatorTrigger;

    public int maxObjects = 5;
    public List<GameObject> spawnedObjects;

    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();

            //Debug.Log("SetActive locally");
            SetActive();
            docking.CmdSetActive(abilityId, true);

            docking.CmdSpawnObjectReference(abilityId, 0, transform.position, transform.rotation.eulerAngles);
        }
    }

    public override void SetActive(bool state = false) {
        if(!string.IsNullOrEmpty(animatorTrigger)) {
            animator.SetTrigger(animatorTrigger);
        }
    }

    void ISpawnableReferenceProvider.SetSpawnedObjectReference(GameObject spawnedObject) {
        spawnedObjects.Add(spawnedObject);

        if(spawnedObjects.Count > maxObjects) {
            docking.CmdDestroyObject(spawnedObjects[0]);
            spawnedObjects.RemoveAt(0);
        }
    }

    GameObject ISpawnableProvider.GetSpawnablePrefab(int spawnableId) {
        return spawnTestPrefab;
    }
}
