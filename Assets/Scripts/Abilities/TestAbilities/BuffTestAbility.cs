﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffTestAbility : Ability, IModifierProvider {
    public SpriteRenderer[] visuals;

    public Color activeColor;
    private Color initialColor;

    public ModifierInfo buff;

    private bool isActive;

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        initialColor = visuals[0].color;
    }

    public override void ButtonDown() {
        if(isActive) {
            docking.CmdSetModifier(abilityId, 0, false);
        } else {
            if(cooldown.IsReady()) {
                cooldown.Activate();

                docking.CmdSetModifier(abilityId, 0, true);
            }
        }
    }

    public override void SetActive(bool state = false) {  
    }

    public override void SetModifier(bool state = false) {
        if(state) {
            ActivateBuff();
        } else {
            DisableBuff();
        }
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return buff;
    }

    private void ActivateBuff() {
        isActive = true;
        foreach(var renderer in visuals) {
            renderer.color = activeColor;
        }
    }

    private void DisableBuff() {
        isActive = false;
        foreach(var renderer in visuals) {
            renderer.color = initialColor;
        }
    }
}
