﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTestAbility : Ability {
    public Transform target;
    public float targetOrthoSize;

    private PlayerCamera playerCamera;
    private FieldOfView playerFOV;

    public float targetViewAngle;
    public float targetViewRadius;
    public float lerpSpeed;

    private bool isActive;

    private DockingKit dockingKit;

    public override void InitializeLocalPlayer(AbilityUI abilityUI) {
        base.InitializeLocalPlayer(abilityUI);

        playerCamera = docking.GetComponent<Player>().PlayerCameraInstance;
        playerFOV = docking.GetComponent<Player>().GetPlayerFOV();

        dockingKit = docking.GetDockingKit();
    }

    public override void ButtonDown() {
        if(!isActive) {
            dockingKit.SetAbilityLock(true, 0, 1, 3);
            docking.SetPlayerInputRestriction(true, InputType.Movement);

            playerCamera.SetTarget(target, true);
            playerCamera.SetOrthoSizeTarget(targetOrthoSize);

            playerFOV.SetViewAngle(targetViewAngle, lerpSpeed);
            playerFOV.SetViewRadius(targetViewRadius, lerpSpeed);
        } else {
            dockingKit.SetAbilityLock(false, 0, 1, 3);
            docking.SetPlayerInputRestriction(false, InputType.Movement);

            playerCamera.ReturnToPlayer(true);
            playerFOV.ResetViewAngle(lerpSpeed);
            playerFOV.ResetViewRadius(lerpSpeed);
        }

        isActive = !isActive;
    }

    public override void CancelAbility() {
        if(isActive) {
            dockingKit.SetAbilityLock(false, 0, 1, 3);
            docking.SetPlayerInputRestriction(false, InputType.Movement);

            playerCamera.ReturnToPlayer(true);
            playerFOV.ResetViewAngle(lerpSpeed);
            playerFOV.ResetViewRadius(lerpSpeed);
            isActive = false;
        }
    }

    public override void SetActive(bool state = false) {

    }
}
