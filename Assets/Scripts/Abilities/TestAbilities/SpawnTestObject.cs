﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SpawnTestObject : SpawnableObject, IRedirectable {
    public float moveSpeed;
    public float damage;

    void Update() {
        transform.Translate(transform.forward * moveSpeed * Time.deltaTime, Space.World);
    }

    public void RedirectDirection(Vector3 newDirection, int newPlayerId = -1, TeamId newTeamId = TeamId.Unassigned) {
        transform.forward = newDirection;

        if(newPlayerId >= 0) {
            playerId = (uint)newPlayerId;
        }

        if(newTeamId != TeamId.Unassigned) {
            teamId = newTeamId;
        }
    }

    [ServerCallback]
    void OnTriggerEnter(Collider other) {
        PlayerHealth health = other.GetComponent<PlayerHealth>();
        if(health != null && health.netId.Value != playerId) {
            health.TakeDamage(damage, playerId);
            NetworkServer.Destroy(gameObject);
        }
    }
}
