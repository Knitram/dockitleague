﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for all abilities. 
/// </summary>
public abstract class Ability : MonoBehaviour {
    public float cooldownDuration;
    public Sprite icon;

    protected Docking docking;
    protected Animator animator;
    protected int abilityId;

    protected AbilityCooldown cooldown;
    private bool abilityLocked;

    /// <summary>
    /// Initialization that happens locally on every client.
    /// </summary>
    /// <param name="dock">Reference to the associated Docking.</param>
    /// <param name="anim">Reference to the DockingKit animator.</param>
    /// <param name="abId">The ability's id in DockingKit abilities list.</param>
    public virtual void Initialize(Docking dock, Animator anim, int abId) {
        docking = dock;
        animator = anim;
        abilityId = abId;
    }

    /// <summary>
    /// Initialization that only happens for the local player (Player controlling this ability). Called after Initialize, so the references are already set up.
    /// </summary>
    public virtual void InitializeLocalPlayer(AbilityUI abilityUI) {
        cooldown = new AbilityCooldown(this, cooldownDuration, abilityUI);
    }

    /// <summary>
    /// Runs on every client, but only the local player has cooldown initialized.
    /// </summary>
    protected virtual void Update() {
        if(cooldown != null) {
            cooldown.Update();
        }
    }

    /// <summary>
    /// Called by the cooldown whenever it's ready.
    /// </summary>
    public virtual void CooldownReady() { }

    /// <summary>
    /// Called when the associated ability button is pressed. Must be overriden.
    /// </summary>
    public abstract void ButtonDown();

    /// <summary>
    /// Called when the associated ability button is released.
    /// </summary>
    public virtual void ButtonUp() { }

    /// <summary>
    /// Call for cancelling abilities. Override in abilities that may be interrupted.
    /// </summary>
    public virtual void CancelAbility() { }

    /// <summary>
    /// Get and Set ability lock. Lock prevents the player from using abilities.
    /// </summary>
    public bool AbilityLock {
        get { return abilityLocked; }
        set {
            abilityLocked = value;

            if(abilityLocked) {
                CancelAbility();
            }
        }
    }

    /// <summary>
    /// Synchronized state call between clients. Appropriate place for starting local animations/sounds.
    /// </summary>
    /// <param name="state">If the ability should be activated or deactivated.</param>
    public abstract void SetActive(bool state);

    /// <summary>
    /// Called by the Modifier. Appropriate place for doing local changes.
    /// </summary>
    /// <param name="state">If the modifier should be activated or deactivated.</param>
    public virtual void SetModifier(bool state) { }

    /// <summary>
    /// Reduces the current cooldown for the ability.
    /// </summary>
    /// <param name="reductionAmount">The amount deducted for the current cooldown.</param>
    public void ReduceCooldown(float reductionAmount) {
        cooldown.ReduceCooldown(reductionAmount);
    }

    /// Used for local spawning of elemental effect prefabs
    /// </summary>
    /// <param name="element">The element we want to set</param>
    public virtual void SetElement(ElementalContainer.ComboableElements element) { }
}
