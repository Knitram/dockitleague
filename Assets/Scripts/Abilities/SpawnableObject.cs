﻿using UnityEngine;
using UnityEngine.Networking;

public class SpawnableObject : NetworkBehaviour {
    protected uint playerId;
    protected TeamId teamId;

    public uint GetOwnerPlayerId() {
        return playerId;
    }

    public TeamId GetOwnerTeamId() {
        return teamId;
    }

    public void SetOwner(uint player, TeamId team) {
        playerId = player;
        teamId = team;
    }

    /// <summary>
    /// Check if the other player is damagable by this spawnable. Unassigned team id means teams aren't used.
    /// </summary>
    /// <param name="otherObject">The other player object.</param>
    /// <returns>True if damagable, false otherwise.</returns>
    public bool CheckDamagable(NetworkBehaviour otherObject) {
        return (playerId != otherObject.netId.Value && (teamId == TeamId.Unassigned || teamId != otherObject.GetComponent<Player>().GetPlayerTeamId()));
    }

    [ServerCallback]
    protected virtual void OnDestroy() {
        if(SpawnableFactory.InstanceExists) {
            SpawnableFactory.Instance.SpawnableDestroyed(this);
        }
    }
}
