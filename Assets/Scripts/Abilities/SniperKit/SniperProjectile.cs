﻿using UnityEngine;
using UnityEngine.Networking;

public class SniperProjectile : SpawnableObject, IRedirectable {
    public float moveSpeed = 60f;
    public float damage = 50f;
    public float lifetime = 8f;

    private bool active;

    /// <summary>
    /// Server call for initializing the projectile based on the forceModifier.
    /// </summary>
    /// <param name="forceModifier">Modifier in the 0-1 range which affects the stats.</param>
    [Server]
    public void Initialize(float forceModifier) {
        active = true;
        moveSpeed *= forceModifier;
        damage *= forceModifier;
        lifetime *= forceModifier;

        Invoke("Lifetime", lifetime);
        RpcInitialize(forceModifier);
    }

    /// <summary>
    /// ClientRpc for synchronizing the forceModifier.
    /// </summary>
    /// <param name="forceModifier">Modifier in the 0-1 range which affects the stats.</param>
    [ClientRpc]
    public void RpcInitialize(float forceModifier) {
        if(isServer) {
            //This already happened on the server.
            return;
        }

        active = true;
        moveSpeed *= forceModifier;
        damage *= forceModifier;
        lifetime *= forceModifier;
    }

    [Server]
    private void Lifetime() {
        NetworkServer.Destroy(gameObject);
    }

    void Update() {
        if(active) {
            transform.Translate(transform.forward * moveSpeed * Time.deltaTime, Space.World);
        }
    }

    [Server]
    void IRedirectable.RedirectDirection(Vector3 newDirection, int newPlayerId, TeamId newTeamId) {
        transform.forward = newDirection;
        RpcNewDirection(transform.position, newDirection);

        if(newPlayerId >= 0) {
            playerId = (uint)newPlayerId;
        }

        if(newTeamId != TeamId.Unassigned) {
            teamId = newTeamId;
        }
    }

    /// <summary>
    /// ClientRpc for synchronizing a direction change.
    /// </summary>
    /// <param name="positionAtChange">The position where the change happened.</param>
    /// <param name="newDirection">The new direction to use.</param>
    [ClientRpc]
    private void RpcNewDirection(Vector3 positionAtChange, Vector3 newDirection) {
        transform.position = positionAtChange;
        transform.forward = newDirection;
    }

    [ServerCallback]
    void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Obstacle")) {
            NetworkServer.Destroy(gameObject);
            return;
        }

        PlayerHealth health = other.GetComponent<PlayerHealth>();
        if(health != null && CheckDamagable(health)) {
            health.TakeDamage(damage, playerId);
            NetworkServer.Destroy(gameObject);
        }
    }
}
