﻿using UnityEngine;
using UnityEngine.Networking;

public class ZiplineGun : Ability, IServerCallback<Vector3, Vector3>, ITargetClientCallback {
    public GameObject ziplinePrefab;
    public Transform spawnPoint;

    public GameObject radiusObject;

    private Zipline zipline;

    private bool active;

    public override void InitializeLocalPlayer(AbilityUI abilityUI) {
        base.InitializeLocalPlayer(abilityUI);

        radiusObject.transform.localScale *= ziplinePrefab.GetComponent<Zipline>().maxFireRange;
    }

    /// <summary>
    /// Activate the radius indicator if cooldown is ready.
    /// </summary>
    public override void ButtonDown() {
        if(!cooldown.IsReady()) {
            return;
        }

        radiusObject.SetActive(true);
    }

    /// <summary>
    /// Fire the zipline if the radiusObject is active, this means ButtonDown was called when the cooldown was ready.
    /// </summary>
    public override void ButtonUp() {
        if(!radiusObject.activeSelf) {
            return;
        }

        radiusObject.SetActive(false);
        cooldown.ActivateHiddenCooldown(1f);
        docking.CmdServerCallbackTwoVector3(abilityId, 0, spawnPoint.position, spawnPoint.forward);
    }

    public override void SetActive(bool state) {
    }

    /// <summary>
    /// ServerCallback for running the FirePoint function on the server.
    /// </summary>
    /// <param name="functionId">Only used if multiple functions are needed.</param>
    /// <param name="position">The position to fire from.</param>
    /// <param name="direction">The direction to fire.</param>
    void IServerCallback<Vector3, Vector3>.ServerCallback(int functionId, Vector3 position, Vector3 direction) {
        FirePoint(position, direction);
    }

    /// <summary>
    /// TargetClientCallback which activates the local player cooldown.
    /// </summary>
    /// <param name="functionId"></param>
    void ITargetClientCallback.TargetClientCallback(int functionId) {
        cooldown.Activate();
    }

    /// <summary>
    /// Server call which either fires the first or second point of a zipline. Spawns the actual zipline on the first shot.
    /// </summary>
    /// <param name="position">The position to fire from.</param>
    /// <param name="direction">The direction to fire.</param>
    private void FirePoint(Vector3 position, Vector3 direction) {
        //If zipline has a reference fire the second point.
        if(zipline != null) {
            zipline.FirePoint(docking.gameObject, position, direction);
            docking.TargetClientCallback(docking.GetConnectionToClient(), abilityId, 0);
            zipline = null;
            return;
        }

        //No reference means we need to spawn one.
        SpawnableObject spawnObject = SpawnableFactory.Instance.SpawnObject(ziplinePrefab, position, Vector3.zero, docking.netId.Value, docking.GetComponent<Player>().GetPlayerTeamId());
        zipline = spawnObject.GetComponent<Zipline>();

        //If the zipline FirePoint returns false the shot attempt failed, and the zipline will be deleted.
        if(!zipline.FirePoint(docking.gameObject, position, direction)) {
            zipline = null;
        }
    }
}
