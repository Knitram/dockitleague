﻿using UnityEngine;
using UnityEngine.Networking;

public class Bola : SpawnableObject, IRedirectable {
    public float moveSpeed = 8f;
    public float moveSpeedOnHit = 20f;

    public float lifetime = 10f;
    public float hitRadius = 2f;
    public float rotationSpeed = 500f;

    public ModifierInfo slowModifier;
    public ModifierInfo stunModifier;

    public Transform visuals;

    public Transform leftBall;
    public Transform rightBall;

    public LineRenderer lineRenderer;

    private bool active;

    private enum BolaState { Moving, Slowing, StunPlayers, StunObtacle }
    private BolaState bolaState;

    private Transform leftBallTarget;
    private Transform rightBallTarget;
    private Vector3 obstaclePoint;

    void Start() {
        if(isServer) {
            Invoke("Lifetime", lifetime);
            active = true;
        }

        bolaState = BolaState.Moving;
        lineRenderer.enabled = true;
        lineRenderer.SetPosition(0, leftBall.position);
        lineRenderer.SetPosition(1, rightBall.position);
    }

    [Server]
    private void Lifetime() {
        NetworkServer.Destroy(gameObject);
    }

    /// <summary>
    /// Update based on the bolaState.
    /// </summary>
    void Update() {
        switch(bolaState) {
            case BolaState.Moving:
                transform.Translate(transform.forward * moveSpeed * Time.deltaTime, Space.World);
                visuals.Rotate(0f, 0f, rotationSpeed * Time.deltaTime);
                break;
            case BolaState.Slowing:
                visuals.Rotate(0f, 0f, rotationSpeed * Time.deltaTime);
                transform.position = Vector3.MoveTowards(transform.position, leftBallTarget.position, moveSpeedOnHit * Time.deltaTime);
                break;
            case BolaState.StunPlayers:
                leftBall.position = Vector3.MoveTowards(leftBall.position, leftBallTarget.position, moveSpeedOnHit * Time.deltaTime);
                rightBall.position = Vector3.MoveTowards(rightBall.position, rightBallTarget.position, moveSpeedOnHit * Time.deltaTime);
                break;
            case BolaState.StunObtacle:
                leftBall.position = Vector3.MoveTowards(leftBall.position, leftBallTarget.position, moveSpeedOnHit * Time.deltaTime);
                rightBall.position = Vector3.MoveTowards(rightBall.position, obstaclePoint, moveSpeedOnHit * Time.deltaTime);
                break;
        }
        lineRenderer.SetPosition(0, leftBall.position);
        lineRenderer.SetPosition(1, rightBall.position);
    }

    [ServerCallback]
    void OnTriggerEnter(Collider other) {
        if(!active) {
            return;
        }

        if(other.CompareTag("Obstacle")) {
            NetworkServer.Destroy(gameObject);
            active = false;
            return;
        }

        PlayerStatus player = other.GetComponent<PlayerStatus>();
        if(player != null && CheckDamagable(player)) {
            CancelInvoke("Lifetime");
            //Stun if walls/players behind player hit.
            RaycastHit hit;
            if(Physics.Raycast(player.transform.position, transform.forward, out hit, hitRadius)) {
                //Obstacle check
                if(hit.collider.CompareTag("Obstacle")) {
                    player.ApplyModifier(stunModifier);
                    RpcOnStunHitObstacle(player.gameObject, hit.point);
                    Invoke("Lifetime", stunModifier.duration);
                    active = false;
                    return;
                }

                //Other player check
                PlayerStatus otherPlayer = hit.collider.GetComponent<PlayerStatus>();
                if(otherPlayer != null && CheckDamagable(otherPlayer)) {
                    player.ApplyModifier(stunModifier);
                    otherPlayer.ApplyModifier(stunModifier);
                    RpcOnStunHitPlayers(player.gameObject, otherPlayer.gameObject);
                    Invoke("Lifetime", stunModifier.duration);
                    active = false;
                    return;
                }
            } 

            //Slow otherwise.
            player.ApplyModifier(slowModifier);
            RpcOnSlowHit(player.gameObject);
            Invoke("Lifetime", slowModifier.duration);

            active = false;
            GetComponent<Collider>().enabled = false;
        }
    }

    /// <summary>
    /// ClientRpc for synchronizing stunning two players against each other.
    /// </summary>
    /// <param name="firstPlayer">The first player to stun.</param>
    /// <param name="secondPlayer">The second player to stun.</param>
    [ClientRpc]
    private void RpcOnStunHitPlayers(GameObject firstPlayer, GameObject secondPlayer) {
        bolaState = BolaState.StunPlayers;

        leftBallTarget = firstPlayer.transform;
        rightBallTarget = secondPlayer.transform;
    }

    /// <summary>
    /// ClientRpc for synchronizing stunning one player against an obstacle.
    /// </summary>
    /// <param name="firstPlayer">The first player to stun.</param>
    /// <param name="point">The point to stun against.</param>
    [ClientRpc]
    private void RpcOnStunHitObstacle(GameObject firstPlayer, Vector3 point) {
        bolaState = BolaState.StunObtacle;

        leftBallTarget = firstPlayer.transform;
        obstaclePoint = point;
    }

    /// <summary>
    /// ClientRpc for synchronizing slowing one player.
    /// </summary>
    /// <param name="player">The player slowed.</param>
    [ClientRpc]
    private void RpcOnSlowHit(GameObject player) {
        bolaState = BolaState.Slowing;
        leftBallTarget = player.transform;
    }

    [Server]
    void IRedirectable.RedirectDirection(Vector3 newDirection, int newPlayerId, TeamId newTeamId) {
        transform.forward = newDirection;
        RpcNewDirection(transform.position, newDirection);

        if(newPlayerId >= 0) {
            playerId = (uint)newPlayerId;
        }

        if(newTeamId != TeamId.Unassigned) {
            teamId = newTeamId;
        }
    }

    /// <summary>
    /// ClientRpc for synchronizing a direction change.
    /// </summary>
    /// <param name="positionAtChange">The position where the change happened.</param>
    /// <param name="newDirection">The new direction to use.</param>
    [ClientRpc]
    private void RpcNewDirection(Vector3 positionAtChange, Vector3 newDirection) {
        transform.position = positionAtChange;
        transform.forward = newDirection;
    }

#if UNITY_EDITOR
    void OnDrawGizmosSelected() {
        Gizmos.DrawWireSphere(transform.position, hitRadius);
    }
#endif
}
