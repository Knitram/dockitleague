﻿using UnityEngine;

public class Shackle : Ability, ISpawnableProvider {
    public string animatorTrigger;

    public GameObject spawnablePrefab;
    public Transform spawnPoint;

    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();

            SetActive(false);
            docking.CmdSetActive(abilityId, false);

            docking.CmdSpawnObject(abilityId, 0, spawnPoint.position, spawnPoint.eulerAngles);
        }
    }

    /// <summary>
    /// Called from AbilityCooldown when the ability is ready. Setting active to true returns the bola to the docking kit visuals.
    /// </summary>
    public override void CooldownReady() {
        SetActive(true);
        docking.CmdSetActive(abilityId, true);
    }

    /// <summary>
    /// State is here the active of the bola visuals (Opposite of normal).
    /// </summary>
    /// <param name="state">Visual state of the bola.</param>
    public override void SetActive(bool state) {
        if(state) {
            animator.SetTrigger(animatorTrigger);
        }
        spawnPoint.gameObject.SetActive(state);
    }

    GameObject ISpawnableProvider.GetSpawnablePrefab(int spawnableId) {
        return spawnablePrefab;
    }
}
