﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Zipline : SpawnableObject, IInteractable {
    public Transform wallEndPoint;

    public Transform lineStartPoint;
    public Transform lineEndPoint;

    public SphereCollider sphereCollider;
    public LineRenderer lineRenderer;

    public Transform handles;
    public Transform radiusTransform;

    public LayerMask interruptionLayerMask;

    public float maxFireRange = 10f;
    public float maxLineDistance = 20f;

    public float hookPointFireSpeed = 40f;
    public float normalRotationSpeed = 10f;

    public float playerMoveSpeed = 20f;
    public int uses = 3;

    private Transform lineEndTransform;

    private Coroutine interruptionCoroutine;

    /// <summary>
    /// Server call from ZiplineGun whenever a point is fired.
    /// </summary>
    /// <param name="player">The player firing.</param>
    /// <param name="position">Fired from this position.</param>
    /// <param name="direction">Fired in this direction.</param>
    /// <returns>If the shot was successful.</returns>
    [Server]
    public bool FirePoint(GameObject player, Vector3 position, Vector3 direction) {
        RaycastHit hit;

        //Successful hit.
        if(Physics.Raycast(position, direction, out hit, maxFireRange) && hit.collider.CompareTag("Obstacle")) {
            //First point.
            if(lineEndTransform == null) {
                Docking localPlayer = player.GetComponent<Docking>();
                lineEndTransform = (localPlayer.GetDockingKit().abilities[3] as ZiplineGun).spawnPoint;
                interruptionCoroutine = StartCoroutine(InterruptionCheck());

                TargetInitializeZiplineOwner(localPlayer.GetConnectionToClient());
                RpcFirstPointHit(player, position, hit.point, hit.normal);
                return true;
            } 

            //Second point. 
            StopCoroutine(interruptionCoroutine);

            //Check distance, hit.point might still be out of range.
            float ziplineEndDistance = (hit.point - lineStartPoint.position).magnitude;
            if(ziplineEndDistance <= maxLineDistance) {
                lineEndTransform = lineEndPoint;
                //Calculate the time the end point will spend travelling, with a 0.5s buffer.
                StartCoroutine(TimedInterruptionCheck(ziplineEndDistance / hookPointFireSpeed + 0.5f));
                RpcSecondPointHit(position, hit.point, hit.normal);
                return true;
            }

            //Calculate travel time, to and back from the hit point.
            Invoke("DestroyZipline", (position - hit.point).magnitude / hookPointFireSpeed * 2f);
            RpcSecondPointMissed(position, hit.point);
            return false;
        }
        
        //If we hit something other than an obstacle we can still use hit.point as the end position.
        Vector3 endPosition = (hit.collider != null) ? hit.point : position + direction * maxFireRange;

        //First or second point.
        if(lineEndTransform == null) {
            RpcFirstPointMissed(player, position, endPosition);
        } else {
            RpcSecondPointMissed(position, endPosition);
        }

        Invoke("DestroyZipline", (position - endPosition).magnitude / hookPointFireSpeed * 2f);
        return false;
    }

    /// <summary>
    /// TargetRpc for initializing the owner.
    /// </summary>
    /// <param name="connection"></param>
    [TargetRpc]
    private void TargetInitializeZiplineOwner(NetworkConnection connection) {
        radiusTransform.gameObject.SetActive(true);
        radiusTransform.localScale *= maxLineDistance;
    }

    /// <summary>
    /// ClientRpc called when the first point is successful.
    /// </summary>
    /// <param name="player">The zipline owner.</param>
    /// <param name="startPosition">Start position of zipline shot.</param>
    /// <param name="endPosition">End position of zipline shot.</param>
    /// <param name="hitNormal">The normal of the surface hit.</param>
    [ClientRpc]
    private void RpcFirstPointHit(GameObject player, Vector3 startPosition, Vector3 endPosition, Vector3 hitNormal) {
        lineEndTransform = (player.GetComponent<Docking>().GetDockingKit().abilities[3] as ZiplineGun).spawnPoint;
        lineRenderer.enabled = true;

        transform.position = startPosition;
        transform.forward = (endPosition - startPosition).normalized;

        StartCoroutine(PointHit(transform, endPosition, hitNormal));
    }

    /// <summary>
    /// ClientRpc called when the first point misses.
    /// </summary>
    /// <param name="player">The zipline owner.</param>
    /// <param name="startPosition">Start position of zipline shot.</param>
    /// <param name="endPosition">End position of zipline shot.</param>
    [ClientRpc]
    private void RpcFirstPointMissed(GameObject player, Vector3 startPosition, Vector3 endPosition) {
        lineEndTransform = (player.GetComponent<Docking>().GetDockingKit().abilities[3] as ZiplineGun).spawnPoint;
        lineRenderer.enabled = true;

        transform.position = startPosition;
        transform.forward = (endPosition - startPosition).normalized;

        StartCoroutine(PointMissed(transform, startPosition, endPosition));
    }

    /// <summary>
    /// ClientRpc called when the second point is successful.
    /// </summary>
    /// <param name="startPosition">Start position of zipline shot.</param>
    /// <param name="endPosition">End position of zipline shot.</param>
    /// <param name="hitNormal">The normal of the surface hit.</param>
    [ClientRpc]
    private void RpcSecondPointHit(Vector3 startPosition, Vector3 endPosition, Vector3 hitNormal) {
        wallEndPoint.gameObject.SetActive(true);
        wallEndPoint.position = startPosition;
        wallEndPoint.forward = (endPosition - startPosition).normalized;

        lineEndTransform = lineEndPoint;

        StartCoroutine(PointHit(wallEndPoint, endPosition, hitNormal, true));
    }

    /// <summary>
    /// ClientRpc called when the second point misses.
    /// </summary>
    /// <param name="startPosition">Start position of zipline shot.</param>
    /// <param name="endPosition">End position of zipline shot.</param>
    [ClientRpc]
    private void RpcSecondPointMissed(Vector3 startPosition, Vector3 endPosition) {
        wallEndPoint.gameObject.SetActive(true);
        wallEndPoint.position = startPosition;
        wallEndPoint.forward = (endPosition - startPosition).normalized;

        lineEndTransform = lineEndPoint;

        StartCoroutine(PointMissed(wallEndPoint, transform.position, endPosition));
    }

    /// <summary>
    /// Runs on every client, updates the line renderer for if valid end transform.
    /// </summary>
    void LateUpdate() {
        if(lineEndTransform != null) {
            lineRenderer.SetPosition(0, lineStartPoint.position);
            lineRenderer.SetPosition(1, lineEndTransform.position);
        }
    }

    /// <summary>
    /// Server coroutine used between the first and second point fired. Destroys zipline if the sightline is broken, or out of range.
    /// </summary>
    private IEnumerator InterruptionCheck() {
        float distance = (lineEndTransform.position - lineStartPoint.position).magnitude;
        Vector3 direction = (lineEndTransform.position - lineStartPoint.position) / distance;

        while(distance < maxLineDistance && !Physics.Raycast(lineStartPoint.position, direction, distance, interruptionLayerMask)){
            distance = (lineEndTransform.position - lineStartPoint.position).magnitude;
            direction = (lineEndTransform.position - lineStartPoint.position) / distance;
            yield return null;
        }

        NetworkServer.Destroy(gameObject);
    }

    /// <summary>
    /// Server coroutine used after the second point is fired. Destroys zipline if the sightline is broken.
    /// </summary>
    private IEnumerator TimedInterruptionCheck(float time) {
        float timer = 0f;
        float distance = (lineEndTransform.position - lineStartPoint.position).magnitude;
        Vector3 direction = (lineEndTransform.position - lineStartPoint.position) / distance;

        while(timer < time) {
            timer += Time.deltaTime;

            if(Physics.Raycast(lineStartPoint.position, direction, distance, interruptionLayerMask)) {
                NetworkServer.Destroy(gameObject);
            }

            distance = (lineEndTransform.position - lineStartPoint.position).magnitude;
            direction = (lineEndTransform.position - lineStartPoint.position) / distance;
            yield return null;
        }
        lineEndTransform = null;
    }

    /// <summary>
    /// Runs on every client. Updates the point visuals.
    /// </summary>
    /// <param name="point">The point to move.</param>
    /// <param name="endPosition">The point end position.</param>
    /// <param name="endNormal">The points end normal.</param>
    /// <param name="activateCollider">If this is the second point, and the collider should be activated.</param>
    private IEnumerator PointHit(Transform point, Vector3 endPosition, Vector3 endNormal, bool activateCollider = false) {
        yield return MoveTransformToPosition(point, endPosition);
        yield return RotateTransformToNormal(point, -endNormal);

        if(activateCollider) {
            sphereCollider.enabled = true;

            //Used for the zipline owner.
            if(radiusTransform.gameObject.activeSelf) {
                radiusTransform.gameObject.SetActive(false);
            }

            StartCoroutine(RotateTransformToNormal(handles, (lineStartPoint.position - lineEndTransform.position).normalized));

            if(!isServer) {
                lineEndTransform = null;
            }
        }
    }

    /// <summary>
    /// Runs on every client. Updates the point visuals.
    /// </summary>
    /// <param name="point">The point to move.</param>
    /// <param name="endPosition">The point start position.</param>
    /// <param name="endPosition">The point end position.</param>
    private IEnumerator PointMissed(Transform point, Vector3 startPosition, Vector3 endPosition) {
        yield return MoveTransformToPosition(point, endPosition);
        yield return MoveTransformToPosition(point, startPosition);
    }

    /// <summary>
    /// Moves the given transform to the target position over time.
    /// </summary>
    /// <param name="tr">The Transform to move.</param>
    /// <param name="targetPosition">The position it will be moved to.</param>
    private IEnumerator MoveTransformToPosition(Transform tr, Vector3 targetPosition) {
        while((tr.position - targetPosition).sqrMagnitude > 0.2f) {
            tr.position = Vector3.MoveTowards(tr.position, targetPosition, hookPointFireSpeed * Time.deltaTime);
            yield return null;
        }
        tr.position = targetPosition;
    }

    /// <summary>
    /// Rotates the given transform forward to point in the target normal over time.
    /// </summary>
    /// <param name="tr">The Transform to move.</param>
    /// <param name="targetNormal">The normal it will be moved to.</param>
    private IEnumerator RotateTransformToNormal(Transform tr, Vector3 targetNormal) {
        while(Vector3.Angle(tr.forward, targetNormal) > 1f) {
            tr.forward = Vector3.RotateTowards(tr.forward, targetNormal, normalRotationSpeed * Time.deltaTime, 0f);
            yield return null;
        }
        tr.forward = targetNormal;
    }

    /// <summary>
    /// Server call whenever a player interacts with the zipline.
    /// </summary>
    /// <param name="player">The player interacting.</param>
    [Server]
    void IInteractable.Interact(Player player) {
        if(uses > 0) {
            TargetInteract(player.NetworkPlayerInstance.connectionToClient, player.gameObject);
            uses--;
            RpcRemainingUses(uses);
            
            if(uses <= 0) {
                //Calculate the time the last player will spend travelling the zipline, with a 0.5s buffer.
                Invoke("DestroyZipline", (lineEndPoint.position - lineStartPoint.position).magnitude / playerMoveSpeed + 0.5f);
            }
        }
    }

    /// <summary>
    /// Server call for destroying the zipline.
    /// </summary>
    [Server]
    private void DestroyZipline() {
        NetworkServer.Destroy(gameObject);
    }

    /// <summary>
    /// ClientRpc for syncing the uses remaining.
    /// </summary>
    /// <param name="remainingUses">The amount remaining.</param>
    [ClientRpc]
    private void RpcRemainingUses(int remainingUses) {
        handles.GetChild(remainingUses).gameObject.SetActive(false);
    }

    /// <summary>
    /// TargetRpc for the server to allow a player to interact.
    /// </summary>
    /// <param name="connection">Needed so TargetRpc finds the correct client.</param>
    /// <param name="player">The player interacting.</param>
    [TargetRpc]
    private void TargetInteract(NetworkConnection connection, GameObject player) {
        StartCoroutine(MovePlayer(player.GetComponent<Player>()));
    }

    /// <summary>
    /// Local coroutine for moving the player along the zipline.
    /// </summary>
    /// <param name="player">The player to be moved.</param>
    private IEnumerator MovePlayer(Player player) {
        Rigidbody rbody = player.GetComponent<Rigidbody>();
        player.GetComponent<Docking>().SetPlayerInputRestriction(true, InputType.Docking, InputType.Interact, InputType.Movement);

        //Move along zipline.
        Vector3 desiredPosition = transform.TransformPoint(sphereCollider.center);
        Vector3 direction = (lineEndPoint.position - lineStartPoint.position).normalized;
        while((rbody.position - lineEndPoint.position).sqrMagnitude > 0.5f && !Physics.Raycast(rbody.position, direction, 2f)) {
            desiredPosition = Vector3.MoveTowards(desiredPosition, lineEndPoint.position, playerMoveSpeed * Time.deltaTime);
            rbody.MovePosition(Vector3.MoveTowards(rbody.position, desiredPosition, playerMoveSpeed * Time.deltaTime));
            yield return null;
        }

        player.GetComponent<Docking>().SetPlayerInputRestriction(false, InputType.Docking, InputType.Interact, InputType.Movement);
    }

#if UNITY_EDITOR
    void OnDrawGizmosSelected() {
        Gizmos.DrawWireSphere(transform.position, maxLineDistance);
    }
#endif
}
