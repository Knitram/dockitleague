﻿using System.Collections;
using UnityEngine;

public class Slingshot : Ability, IServerCallback<float>, IServerCallback<GameObject, float>, IClientCallback<float>, ISpawnableReferenceProvider, IModifierProvider {
    public GameObject projectilePrefab;

    public Transform projectileSpawnPoint;
    public Transform leftFireIndicator;
    public Transform rightFireIndicator;

    public ModifierInfo snipingSlow;

    public Transform projectileVisuals;
    public float projectileMaxPrecisionY = -1.5f;
    private float projectileDefaultY;

    public LineRenderer slingRenderer;

    public float startCurveModifier = 0.5f;
    public float holdCurveModifier = 0.125f;
    public float resetSpeed = 2f;

    public AnimationCurve startCurve;
    public AnimationCurve holdCurve;

    public AnimationCurve projectileFireAnimation;

    private float currentMaxAngle;
    private float fireAngle;
    private float forceModifier;

    private bool active;
    private Coroutine fireCoroutine;
    private Coroutine resetCoroutine;

    private FieldOfView playerFOV;

    private enum ServerCall { Firing, Reset };
    private enum ClientCall { Firing, Reset };

    void Start() {
        projectileDefaultY = projectileVisuals.localPosition.y;
    }

    public override void InitializeLocalPlayer(AbilityUI abilityUI) {
        base.InitializeLocalPlayer(abilityUI);

        playerFOV = docking.GetComponent<Player>().GetPlayerFOV();
    }

    /// <summary>
    /// Start the firing process if cooldown is ready.
    /// </summary>
    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            active = true;

            if(resetCoroutine != null) {
                StopCoroutine(resetCoroutine);
            }
            fireCoroutine = StartCoroutine(LocalFiring());

            docking.CmdSetModifier(abilityId, 0, true);
            docking.CmdSetActive(abilityId, true);
        }
    }

    /// <summary>
    /// Fires the projectile if the ability is active.
    /// </summary>
    public override void ButtonUp() {
        if(active) {
            active = false;
            cooldown.Activate();

            if(fireCoroutine != null) {
                StopCoroutine(fireCoroutine);
            }

            float angle = Random.Range(-fireAngle, fireAngle);
            //Calculate forceModifier by getting average from using max against fireAngle, and max against actual angle.
            forceModifier = 1f - (Mathf.Pow(fireAngle / currentMaxAngle, 5) + Mathf.Abs(angle) / currentMaxAngle) * 0.5f;
            StartCoroutine(Fire(angle));
            
            resetCoroutine = StartCoroutine(ResetIndicators());
            docking.CmdSetModifier(abilityId, 0, false);
            docking.CmdServerCallbackFloat(abilityId, 0, angle);

            StartCoroutine(FireAnimation(-angle));
        }
    }

    /// <summary>
    /// Cancel the firing process if active.
    /// </summary>
    public override void CancelAbility() {
        if(active) {
            active = false;
            cooldown.Activate();

            leftFireIndicator.gameObject.SetActive(false);
            rightFireIndicator.gameObject.SetActive(false);

            if(fireCoroutine != null) {
                StopCoroutine(fireCoroutine);
            }
            if(resetCoroutine != null) {
                StopCoroutine(resetCoroutine);
            }
            StartCoroutine(ResetSling());

            docking.CmdSetModifier(abilityId, 0, false);
            docking.CmdSetActive(abilityId, false);
        }
    }

    /// <summary>
    /// Synchronizing states, either fires or resets.
    /// </summary>
    /// <param name="fire">If true fire, otherwise reset.</param>
    public override void SetActive(bool fire) {
        if(docking.hasAuthority) {
            return;
        }

        if(fire) {
            if(resetCoroutine != null) {
                StopCoroutine(resetCoroutine);
            }

            fireCoroutine = StartCoroutine(ClientFiring());
            return;
        }

        if(resetCoroutine != null) {
            StopCoroutine(resetCoroutine);
        }
        resetCoroutine = StartCoroutine(ResetSling());
    }

    /// <summary>
    /// Local player call, started when ability activated. Delays the projectile spawns.
    /// </summary>
    private IEnumerator Fire(float angle) {
        yield return new WaitForSeconds(projectileFireAnimation.keys[1].time);
        docking.CmdSpawnObjectReference(abilityId, 0, projectileSpawnPoint.position, transform.eulerAngles + new Vector3(0f, angle, 0f));
    }

    /// <summary>
    /// ServerCallback for synchronizing the fire angle to all clients.
    /// </summary>
    /// <param name="functionId">Not used.</param>
    /// <param name="angle">The angle fired at.</param>
    void IServerCallback<float>.ServerCallback(int functionId, float angle) {
        docking.RpcClientCallbackFloat(abilityId, functionId, angle);
    }

    /// <summary>
    /// ClientCallback for synchronizing the fire angle on clients other than the local player.
    /// </summary>
    /// <param name="functionId">Not used.</param>
    /// <param name="angle">The angle fired at.</param>
    void IClientCallback<float>.ClientCallback(int functionId, float angle) {
        if(docking.hasAuthority) {
            return;
        }

        if(fireCoroutine != null) {
            StopCoroutine(fireCoroutine);
        }

        StartCoroutine(FireAnimation(-angle));
    }

    /// <summary>
    /// ServerCallback for initializing the spawned projectile.
    /// </summary>
    /// <param name="functionId">Not used.</param>
    /// <param name="projectile">The spawned projectile.</param>
    /// <param name="forceModifier">Modifier in the 0-1 range which affects the projectile stats.</param>
    void IServerCallback<GameObject, float>.ServerCallback(int functionId, GameObject projectile, float forceModifier) {
        projectile.GetComponent<SniperProjectile>().Initialize(forceModifier);
    }

    /// <summary>
    /// LocalPlayer call for setting initial values at firing start.
    /// </summary>
    private void InitializeFiring() {
        currentMaxAngle = playerFOV.viewAngle * 0.5f;
        fireAngle = currentMaxAngle;
        SetIndicatorRotation(currentMaxAngle);
    }

    /// <summary>
    /// LocalPlayer call for updating the angle range indicators.
    /// </summary>
    /// <param name="rotation">The new rotation for the indicators.</param>
    private void SetIndicatorRotation(float rotation) {
        Vector3 newRotation = new Vector3(0f, rotation, 0f);
        leftFireIndicator.localEulerAngles = -newRotation;
        rightFireIndicator.localEulerAngles = newRotation;
    }

    /// <summary>
    /// Updates the projectile visuals on the docking kit. Called on every client.
    /// </summary>
    /// <param name="position">The new projectile y position.</param>
    private void SetProjectilePosition(float position) {
        projectileVisuals.localPosition = new Vector3(0f, position, 0f);
        slingRenderer.SetPosition(1, projectileVisuals.localPosition);
    }

    /// <summary>
    /// Update loop when firing for the local player.
    /// </summary>
    private IEnumerator LocalFiring() {
        InitializeFiring();
        leftFireIndicator.gameObject.SetActive(true);
        rightFireIndicator.gameObject.SetActive(true);

        float timer = 0f;
        float curveValue;
        float projectileRange = Mathf.Abs(projectileDefaultY - projectileMaxPrecisionY);

        //Using start curve up to max precision.
        while(timer < 1f) {
            timer += startCurveModifier * Time.deltaTime;
            curveValue = startCurve.Evaluate(timer);
            fireAngle = curveValue * currentMaxAngle;

            SetIndicatorRotation(fireAngle);
            SetProjectilePosition(projectileMaxPrecisionY + curveValue * projectileRange);
            yield return null;
        }

        //Using holdCurve after max precision.
        timer = 0f;
        while(timer < 1f) {
            timer += holdCurveModifier * Time.deltaTime;
            curveValue = holdCurve.Evaluate(timer);
            fireAngle = curveValue * currentMaxAngle;

            SetIndicatorRotation(fireAngle);
            SetProjectilePosition(projectileMaxPrecisionY + curveValue * projectileRange);
            yield return null;
        }

        curveValue = holdCurve.Evaluate(1f);
        fireAngle = curveValue * currentMaxAngle;

        SetIndicatorRotation(fireAngle);
        SetProjectilePosition(projectileMaxPrecisionY + curveValue * projectileRange);

        active = false;
        cooldown.Activate();
        resetCoroutine = StartCoroutine(ResetIndicators());
        StartCoroutine(ResetSling());
        docking.CmdSetModifier(abilityId, 0, false);
    }

    /// <summary>
    /// Update loop when firing for clients other than the local player.
    /// </summary>
    private IEnumerator ClientFiring() {
        float timer = 0f;
        float curveValue;
        float projectileRange = Mathf.Abs(projectileDefaultY - projectileMaxPrecisionY);

        //Using start curve up to max precision.
        while(timer < 1f) {
            timer += startCurveModifier * Time.deltaTime;
            curveValue = startCurve.Evaluate(timer);

            SetProjectilePosition(projectileMaxPrecisionY + curveValue * projectileRange);
            yield return null;
        }

        //Using holdCurve after max precision.
        timer = 0f;
        while(timer < 1f) {
            timer += holdCurveModifier * Time.deltaTime;
            curveValue = holdCurve.Evaluate(timer);

            SetProjectilePosition(projectileMaxPrecisionY + curveValue * projectileRange);
            yield return null;
        }

        SetProjectilePosition(projectileMaxPrecisionY + holdCurve.Evaluate(1f) * projectileRange);
        resetCoroutine = StartCoroutine(ResetSling());
    }

    /// <summary>
    /// LocalPlayer update loop for resetting the angle range indicators.
    /// </summary>
    private IEnumerator ResetIndicators() {
        while(Mathf.Abs(fireAngle - currentMaxAngle) < 0.5f) {
            fireAngle = Mathf.Lerp(fireAngle, currentMaxAngle, resetSpeed * Time.deltaTime);
            SetIndicatorRotation(fireAngle);
            yield return null;
        }

        leftFireIndicator.gameObject.SetActive(false);
        rightFireIndicator.gameObject.SetActive(false);
    }

    /// <summary>
    /// Update loop on every client for resetting the sling visuals.
    /// </summary>
    private IEnumerator ResetSling() {
        float newPosition = projectileVisuals.localPosition.y;

        while(Mathf.Abs(newPosition - projectileDefaultY) > 0.1f) {
            newPosition = Mathf.Lerp(newPosition, projectileDefaultY, resetSpeed * Time.deltaTime);
            SetProjectilePosition(newPosition);
            yield return null;
        }

        SetProjectilePosition(projectileDefaultY);
    }

    /// <summary>
    /// Update loop on every client for playing the fire animation.
    /// </summary>
    /// <param name="angle">The angle fired at.</param>
    private IEnumerator FireAnimation(float angle) {
        float timer = 0f;
        float animationLength = projectileFireAnimation.keys[projectileFireAnimation.length - 1].time;
        float timeToSpawn = projectileFireAnimation.keys[1].time;

        float currentAngle = projectileVisuals.localEulerAngles.z;
        float anglePerSecond =  Mathf.Abs(angle - currentAngle) / timeToSpawn;

        float projectileStartPosition = projectileVisuals.localPosition.y;
        float projectileRange = Mathf.Abs(projectileDefaultY - projectileStartPosition);
        
        while(timer < timeToSpawn) {
            timer += Time.deltaTime;

            SetProjectilePosition(projectileStartPosition + projectileFireAnimation.Evaluate(timer) * projectileRange);

            currentAngle = Mathf.MoveTowardsAngle(currentAngle, angle, anglePerSecond * Time.deltaTime);
            projectileVisuals.localEulerAngles = new Vector3(0f, 0f, currentAngle);
            yield return null;
        }

        projectileVisuals.gameObject.SetActive(false);

        //Move the visuals even if the projectile is hidden, since the sling position is set to projectile base.
        while(timer < animationLength) {
            timer += Time.deltaTime;
            SetProjectilePosition(projectileStartPosition + projectileFireAnimation.Evaluate(timer) * projectileRange);
            yield return null;
        }

        SetProjectilePosition(projectileDefaultY);
        projectileVisuals.localEulerAngles = Vector3.zero;

        //Return visuals after a second.
        yield return new WaitForSeconds(1f);
        projectileVisuals.gameObject.SetActive(true);
    }

    GameObject ISpawnableProvider.GetSpawnablePrefab(int prefabId) {
        return projectilePrefab;
    }

    void ISpawnableReferenceProvider.SetSpawnedObjectReference(GameObject spawnedObject) {
        docking.CmdServerCallbackGameObjectFloat(abilityId, 0, spawnedObject, forceModifier);
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return snipingSlow;
    }
}
