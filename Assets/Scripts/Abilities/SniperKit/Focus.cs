﻿using System.Collections;
using UnityEngine;

public class Focus : Ability, IModifierProvider {
    public string animatorBool;

    public float maxDuration = 15f;

    public Transform target;
    public float targetOrthoSize;

    public Slingshot slingshot;
    public Transform leftSlingHandle;
    public Transform rightSlingHandle;

    public ModifierInfo focusModifier;

    private PlayerCamera playerCamera;
    private FieldOfView playerFOV;

    public float targetViewAngle;
    public float targetViewRadius;
    public float lerpSpeed;

    private bool isActive;
    private Coroutine activeCoroutine;

    public override void InitializeLocalPlayer(AbilityUI abilityUI) {
        base.InitializeLocalPlayer(abilityUI);

        playerCamera = docking.GetComponent<Player>().PlayerCameraInstance;
        playerFOV = docking.GetComponent<Player>().GetPlayerFOV();
    }

    public override void ButtonDown() {
        if(!cooldown.IsReady()) {
            return;
        }

        SetAbilityState(!isActive);
    }

    public override void CancelAbility() {
        if(isActive) {
            SetAbilityState(false);
        }
    }

    public override void SetActive(bool state) {
        if(!string.IsNullOrEmpty(animatorBool)) {
            animator.SetBool(animatorBool, state);
        }
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return focusModifier;
    }

    /// <summary>
    /// Limit the duration to max duration.
    /// </summary>
    private IEnumerator Active() {
        yield return new WaitForSeconds(maxDuration);
        SetAbilityState(false);
    }

    /// <summary>
    /// Toggle the ability active state.
    /// </summary>
    /// <param name="newState">The new active state.</param>
    private void SetAbilityState(bool newState) {
        isActive = newState;
        SetActive(newState);
        docking.CmdSetActive(abilityId, newState);
        docking.CmdSetModifier(abilityId, 0, newState);

        docking.SetPlayerInputRestriction(newState, InputType.Movement);
        slingshot.CancelAbility();

        if(newState) {
            playerCamera.SetTarget(target, true);
            playerCamera.SetOrthoSizeTarget(targetOrthoSize);

            playerFOV.SetViewAngle(targetViewAngle, lerpSpeed);
            playerFOV.SetViewRadius(targetViewRadius, lerpSpeed);

            activeCoroutine = StartCoroutine(Active());
        } else {
            if(activeCoroutine != null) {
                StopCoroutine(activeCoroutine);
            }
            cooldown.Activate();

            playerCamera.ReturnToPlayer(true);
            playerFOV.ResetViewAngle(lerpSpeed);
            playerFOV.ResetViewRadius(lerpSpeed);
        }
    }
}
