﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Track : Ability, IServerCallback {
    public float castRange;
    public LayerMask layerMask;
    public ModifierInfo trackInfo;

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);
    }

    //TOTO: need animation from player to target
    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();
            docking.CmdSetActive(abilityId, true);
            docking.CmdServerCallback(abilityId, 0);
        }

    }

    void IServerCallback.ServerCallback(int functionId) {
        var hits = Physics.RaycastAll(transform.position, transform.forward, castRange).OrderBy(h => h.distance);
        
        foreach(RaycastHit hit in hits) {
            if (hit.collider != null && hit.collider.CompareTag("Obstacle")) {
                return;
            }
            if (hit.collider != null && this.docking.CheckDamagable(hit.collider.GetComponent<Player>())) {
                PlayerStatus playerStatus = hit.collider.GetComponent<PlayerStatus>();
                if (playerStatus != null) {
                    playerStatus.ApplyModifier(trackInfo);
                    return;
                }
            }
        } 
    }

    public override void SetActive(bool state = false) {
     
    }
}
