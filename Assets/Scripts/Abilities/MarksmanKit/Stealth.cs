﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stealth : Ability, IModifierProvider {
    private Player player;
    public float stealthDamageBonus;

    public List<string> namesOfVisuals;
    public List<SpriteRenderer> visuals;

    public ModifierInfo buffInfo;
    public ModifierInfo[] modifierInfos;
    
    private bool stealthed = false;

    public float fadeTime;
    private float fadeInterval = 0.1f; // used for yielding.

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        player = docking.GetComponent<Player>();
        FindPlayerSpriteRenderers(namesOfVisuals);
    }

    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();

            SetActive();
            docking.CmdSetActive(abilityId, true);

            if(IsStealthed()) {
                docking.CmdSetModifier(abilityId, 0, false);
            } else {
                docking.CmdSetModifier(abilityId, 0, true);
            }
        } else {
            docking.CmdSetModifier(abilityId, 0, false);
        }

    }

    public override void SetActive(bool state = false) {
    }

    public override void SetModifier(bool state = false) {
        if(state) {
            stealthed = true;
            StartCoroutine(FadeToStealth());
        } else {
            stealthed = false;
            StopCoroutine(FadeToStealth());
            StealthBreak();
        }
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return buffInfo;
    }

    /// <summary>
    /// Breaks stealth and returning visuals back to normal
    /// </summary>
    private void StealthBreak() {
        foreach (SpriteRenderer sr in visuals) {
            var faded = sr.material.color;
            faded.a = 1f;
            sr.material.color = faded;
        }
    }

    /// <summary>
    /// Coroutine to fade player visual alpha value to 0, making it invisible to enemies.
    /// Fades the visuals to 0.5f for the local player to indicate being stealthed.
    /// </summary>
    /// <returns></returns>
    IEnumerator FadeToStealth() {
        if (docking.hasAuthority) {
            for (float i = 1; i >= 0.5f; i -= fadeInterval / fadeTime / 2) {
                foreach (SpriteRenderer sr in visuals) {
                    var faded = sr.material.color;
                    faded.a = i;
                    sr.material.color = faded;
                }
                yield return new WaitForSeconds(fadeInterval);
            }
        }

        if (!docking.hasAuthority) {
            for (float i = 1; i >= 0; i -= fadeInterval / fadeTime) {
                foreach (SpriteRenderer sr in visuals) {
                    var faded = sr.material.color;
                    faded.a = i;
                    sr.material.color = faded;
                }
                yield return new WaitForSeconds(fadeInterval);
            }
        }
       
    }
   
    /// <summary>
    /// Function to find the sprite rendererers relevant to fading into stealth
    /// </summary>
    /// <param name="name">The name of the parent</param>
    /// <returns></returns>
    public void FindPlayerSpriteRenderers(List<string> names) {
        foreach (string name in names) {
            var visualBase = player.transform.Find(name).GetComponentsInChildren<SpriteRenderer>();
            foreach(SpriteRenderer sr in visualBase) {
                visuals.Add(sr);
            }
        }
    }

    public bool IsStealthed() {
        return stealthed;
    }

    public int GetAbilityId() {
        return abilityId;
    }

    public int GetBuffId() {
        return 0;
    }
}
