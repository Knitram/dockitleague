﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Projectile : SpawnableObject, IRedirectable {
    public float projectileSpeed;
    public float lifetime;
    public bool hasStealthBonus;
    public float projectileDamage;
    public Stealth stealthBuff;

    public void Initialize(Stealth stealthRef, bool firedFromStealth = false) {
        stealthBuff = stealthRef;
        hasStealthBonus = firedFromStealth;

        if (hasStealthBonus) {
            projectileDamage += stealthBuff.stealthDamageBonus;
        }
        GetComponent<Rigidbody>().AddForce(transform.forward * projectileSpeed, ForceMode.Impulse);

        Destroy(gameObject, lifetime);
    }
    
    [ServerCallback]
    public void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Obstacle")) {
            Destroy(gameObject);
            return;
        }
        PlayerHealth health = other.GetComponent<PlayerHealth>();
        if (health != null && this.CheckDamagable(other.GetComponent<Player>())) {
            health.TakeDamage(projectileDamage, playerId);
            var playerStatus = other.GetComponent<PlayerStatus>();
            if (playerStatus != null && hasStealthBonus) {
                foreach (ModifierInfo mi in stealthBuff.modifierInfos) {
                    playerStatus.ApplyModifier(mi);
                }
            }
            Destroy(gameObject);
        }
    }

    [Server]
    void IRedirectable.RedirectDirection(Vector3 newDirection, int newPlayerId, TeamId newTeamId) {
        transform.forward = newDirection;

        GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
        GetComponent<Rigidbody>().AddForce(transform.forward * projectileSpeed, ForceMode.Impulse);

        if (newPlayerId >= 0) {
            playerId = (uint)newPlayerId;
        }

        if (newTeamId != TeamId.Unassigned) {
            teamId = newTeamId;
        }
    }
}
