﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class ProjectileSpawner : Ability, IServerCallback, ISpawnableProvider {
    public GameObject projectilePrefab;
    public float spawnOffset;
    public Stealth stealthBuff;

    public override void ButtonDown() {
        if (cooldown.IsReady()) {
            cooldown.Activate();

            SetActive();
            docking.CmdSetActive(abilityId, true);
            docking.CmdServerCallback(abilityId, 0);
            docking.CmdSetModifier(stealthBuff.GetAbilityId(), stealthBuff.GetBuffId(), false);
        }
    }
     
    public override void SetActive(bool state = false) {
        
    }
    
    GameObject ISpawnableProvider.GetSpawnablePrefab(int spawnableId) {
        return projectilePrefab;
    }
    
    /// <summary>
    /// Need to run code on the server to apply modifiers to projectile initialization
    /// </summary>
    /// <param name="functionId">not used at the moment</param>
    void IServerCallback.ServerCallback(int functionId) {
        SpawnableObject spawnObject = SpawnableFactory.Instance.SpawnObject(projectilePrefab, transform.position + transform.forward * spawnOffset, transform.rotation.eulerAngles, docking.netId.Value, docking.GetComponent<Player>().GetPlayerTeamId());
        spawnObject.GetComponent<Projectile>().Initialize(stealthBuff, stealthBuff.IsStealthed());

    }

}
