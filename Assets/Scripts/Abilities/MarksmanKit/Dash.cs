﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : Ability {
    private Rigidbody rigidBody;
    private PlayerInput playerInput;

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);

        rigidBody = docking.GetComponent<Rigidbody>();
        playerInput = docking.GetComponent<PlayerInput>();
    }

    public override void ButtonDown() {
        if (cooldown.IsReady()) {
            cooldown.Activate();
            rigidBody.AddForce(playerInput.GetDirectionVector() * 100f, ForceMode.Impulse);
            SetActive();
            docking.CmdSetActive(abilityId, true);
        }
    }

    public override void SetActive(bool state = false) {
    }
    
}