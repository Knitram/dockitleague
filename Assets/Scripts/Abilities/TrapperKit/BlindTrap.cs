﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlindTrap : Trap {
    public ModifierInfo blindInfo;

    /// <summary>
    /// Callback that allows this trap to do whatever it wants whenever it is triggered
    /// This one simply applies the member structs containing modifier info
    /// </summary>
    /// <param name="playerStatus">The PlayerStatus component of the player that is in the trap</param>
    public override void HandleTrigger(PlayerStatus playerStatus) {
        playerStatus.ApplyModifier(blindInfo);
    }
}
