﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapSpawner : Ability, ISpawnableReferenceProvider {
    public GameObject trapPrefab;
    public float trapActiveAlpha = 0.2f;
    public List<SpriteRenderer> trapActiveSprites = new List<SpriteRenderer>();
    public float lerpSpeed = 5f;

    private GameObject trapReference;

    /// <summary>
    /// Callback for what the local client is supposed to do when this ability's button is pressed
    /// </summary>
    public override void ButtonDown() {
        if (cooldown.IsReady()) {
            if (trapReference != null) {
                docking.CmdDestroyObject(trapReference);
            }
            cooldown.Activate();
            docking.CmdSpawnObjectReference(abilityId, 0, transform.position, Vector3.zero);
        }
    }

    /// <summary>
    /// Callback for synchronising visual state based on the given parameter
    /// </summary>
    /// <param name="state">The state of the ability.</param>
    public override void SetActive(bool state = false) {
        StopAllCoroutines();
        foreach (var sprite in trapActiveSprites) {
            if (state) {
                StartCoroutine(ColorLerp(true, lerpSpeed, sprite));
            } else {
                StartCoroutine(ColorLerp(false, lerpSpeed, sprite));
            }
        }
    }

    /// <summary>
    /// Coroutine that interpolates the alpha value of a sprite
    /// </summary>
    /// <param name="fadeOut">A boolean for whether the alpha value is going to fade out or not</param>
    /// <param name="speed">The speed of the interpolation</param>
    /// <param name="sprite">The sprite we want to interpolate the alpha on</param>
    /// <returns></returns>
    private IEnumerator ColorLerp(bool fadeOut, float speed, SpriteRenderer sprite) {
        float targetAlpha = (fadeOut ? trapActiveAlpha : 1);
        while (Mathf.Abs(targetAlpha - sprite.color.a) > 0.1f) {
            sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, Mathf.Lerp(sprite.color.a, targetAlpha, speed * Time.deltaTime));
            yield return null;
        }

        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, targetAlpha);
    }

    /// <summary>
    /// Callback that gives the player who spawned the trap a reference to it
    /// </summary>
    /// <param name="spawnedObject">A reference to the spawned object</param>
    void ISpawnableReferenceProvider.SetSpawnedObjectReference(GameObject spawnedObject) {
        trapReference = spawnedObject;
        var trap = trapReference.GetComponent<Trap>();
        trap.Initialize(this);
        trap.SetVisualState(true);
        DisplayTrapState(true);
    }

    /// <summary>
    /// Returns the spawnable prefab that is a member of this component
    /// </summary>
    /// <param name="spawnableId">The id of the spawnable. This is only used if an ability has several spawnables in one script</param>
    /// <returns>The member prefab</returns>
    GameObject ISpawnableProvider.GetSpawnablePrefab(int spawnableId) {
        return trapPrefab;
    }

    /// <summary>
    /// A public function that allows traps to update the visual state of the docking kit's placed trap indicator
    /// </summary>
    /// <param name="state">The display state</param>
    public void DisplayTrapState(bool state) {
        SetActive(state);
        docking.CmdSetActive(abilityId, state);
    }
}
