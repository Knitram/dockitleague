﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CaptureTrap : Trap {
    public GameObject walls;
    public float timeBeforeWallsSpawn = 1f;
    public float pullForce = 10f;
    public float fadeSpeed = 10f;
    public float fadeTimeOffsetMultiplier = 1.5f;

    private bool pullActive;

    /// <summary>
    /// Callback for when the trap is triggered. 
    /// Sets relevant gameobjects as active to display visuals and starts a coroutine for spawning the walls.
    /// </summary>
    /// <param name="playerStatus">The PlayerStatus component of the player that is in the trap</param>
    [ServerCallback]
    public override void HandleTrigger(PlayerStatus playerStatus) {
        pullActive = true;
        StartCoroutine(SpawnWalls(timeBeforeWallsSpawn));
    }

    /// <summary>
    /// Unity callback for physics updates. Adds a force towards the center of the trap for as long as the pull is active.
    /// </summary>
    [ServerCallback]
    void FixedUpdate() {
        if (pullActive) {
            for(int i = 0; i < appliedToList.Count; i++) {
                appliedToList[i].TargetAddForce(appliedToList[i].NetworkPlayerInstance.connectionToClient, pullForce, ForceMode.Force, transform.position);
                appliedToListRbodies[i].AddForce((transform.position - appliedToListRbodies[i].transform.position).normalized * pullForce, ForceMode.Force);
            }
        }
    }

    /// <summary>
    /// Coroutine that handles the display state of the walls and visuals
    /// </summary>
    /// <param name="waitTime">The time before this coroutine continues</param>
    /// <returns></returns>
    private IEnumerator SpawnWalls(float waitTime) {
        yield return new WaitForSeconds(waitTime);
        pullActive = false;
        extraVisuals.SetActive(false);
        RpcSetExtraVisualsState(false);
        walls.SetActive(true);
        RpcSetActiveWallState(true);

        foreach (var sprite in walls.GetComponentsInChildren<SpriteRenderer>()) {
            StartCoroutine(ColorLerp(true, fadeSpeed, sprite, timeAfterTriggerDestroy - timeBeforeWallsSpawn * fadeTimeOffsetMultiplier));
        }
        RpcLerpWalls();
    }

    /// <summary>
    /// ClientRpc used for synchronising the visual state of the trap
    /// </summary>
    /// <param name="state">The state of the visuals</param>
    [ClientRpc]
    private void RpcSetActiveWallState(bool state) {
        walls.SetActive(state);
    }

    [ClientRpc]
    private void RpcLerpWalls() {
        foreach (var sprite in walls.GetComponentsInChildren<SpriteRenderer>()) {
            StartCoroutine(ColorLerp(true, fadeSpeed, sprite, timeAfterTriggerDestroy - timeBeforeWallsSpawn * fadeTimeOffsetMultiplier));
        }
    }

    /// <summary>
    /// Coroutine that interpolates the alpha value of a sprite
    /// </summary>
    /// <param name="fadeOut">A boolean for whether the alpha value is going to fade out or not</param>
    /// <param name="speed">The speed of the interpolation</param>
    /// <param name="sprite">The sprite we want to interpolate the alpha on</param>
    /// <returns></returns>
    private IEnumerator ColorLerp(bool fadeOut, float speed, SpriteRenderer sprite, float waitTime) {
        yield return new WaitForSeconds(waitTime);
        float targetAlpha = (fadeOut ? 0 : 1);
        while (Mathf.Abs(targetAlpha - sprite.color.a) > 0.1f) {
            sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, Mathf.Lerp(sprite.color.a, targetAlpha, speed * Time.deltaTime));
            yield return null;
        }

        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, targetAlpha);
    }
}
