﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flamethrower : Ability, IModifierProvider {
    public SpriteRenderer head;

    public ModifierInfo buff;
    public ModifierInfo dot;

    public GameObject flamethrowerContainer;
    public Color headColorWhileActive;

    private Color oldColor;

    public override void Initialize(Docking dock, Animator anim, int abId) {
        base.Initialize(dock, anim, abId);
        oldColor = head.color;
    }

    public override void ButtonDown() {
        if(cooldown.IsReady()) {
            cooldown.Activate();
            SetActive();
            docking.CmdSetActive(abilityId, true);
            docking.CmdSetModifier(abilityId, 0, true);
        }
    }

    public override void SetActive(bool state = false) {
    }

    public override void SetModifier(bool state) {
        SetBuffState(state);
    }

    ModifierInfo IModifierProvider.GetModifierInfo(int modifierId) {
        return buff;
    }

    /// <summary>
    /// Sets the visual state of the flamethrower to the given parameter state
    /// </summary>
    /// <param name="state">The state of the flamethrower</param>
    public void SetBuffState(bool state) {
        flamethrowerContainer.SetActive(state);
        head.color = state ? headColorWhileActive : oldColor;
    }

    /// <summary>
    /// Unity callback for when the flamethrower sphere collider is triggered. 
    /// Applies the dot modifier to any affected players
    /// </summary>
    /// <param name="other">The collider of the triggered player</param>
    void OnTriggerEnter(Collider other) {
        if(!docking.isServer) {
            return;
        }

        if (other.CompareTag("Player") && other.gameObject != gameObject.transform.parent.parent.gameObject) {
            var otherPlayerStatus = other.GetComponent<PlayerStatus>();
            if (otherPlayerStatus != null && docking.CheckDamagable(other.GetComponent<Player>())) {
                otherPlayerStatus.ApplyModifier(dot, abilityId);
            }
        }
        if (other.attachedRigidbody != null) {
            IElement elemObj = other.attachedRigidbody.GetComponent<IElement>();
            if (elemObj != null) {
                elemObj.ApplyElement(ElementalContainer.ComboableElements.Fire);
            }
        }
    }
}
