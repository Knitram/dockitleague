﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public abstract class Trap : SpawnableObject {
    public GameObject visuals;
    public GameObject extraVisuals;
    public float timeAfterTriggerDestroy = 1;

    public string animatorTrigger;
    public Animator animator;

    private TrapSpawner trapOwner;
    protected List<Player> appliedToList = new List<Player>();
    protected List<Rigidbody> appliedToListRbodies = new List<Rigidbody>();

    /// <summary>
    /// An initialisation function for caching the script reference to this trap's owner 
    /// </summary>
    /// <param name="owner"></param>
    public void Initialize(TrapSpawner owner) {
        trapOwner = owner;
    }

    /// <summary>
    /// Unity callback for when this trap's collider is triggered. 
    /// Makes sure that trap effects are only applied once to each player that enters by placing a reference to them in a list.
    /// Also handles visual animations for when the trap is triggered.
    /// </summary>
    /// <param name="other">The collider that triggered this callback</param>
    [ServerCallback]
    void OnTriggerEnter(Collider other) {
        if (!appliedToList.Find(player => player == other.GetComponent<Player>())) {
            if (other.CompareTag("Player") && CheckDamagable(other.GetComponent<NetworkBehaviour>())) {
                var playerStatus = other.GetComponent<PlayerStatus>();
                HandleTrigger(playerStatus);
                appliedToList.Add(other.GetComponent<Player>());
                appliedToListRbodies.Add(other.GetComponent<Rigidbody>());

                extraVisuals.SetActive(true);
                RpcSetExtraVisualsState(true);
                RpcAnimationPlay();
                RpcSetVisualState(true);

                Destroy(gameObject, timeAfterTriggerDestroy);
            }
        }
    }

    /// <summary>
    /// ClientRpc for synchronising the triggering of animations
    /// </summary>
    [ClientRpc]
    void RpcAnimationPlay() {
        if (!string.IsNullOrEmpty(animatorTrigger)) {
            animator.SetTrigger(animatorTrigger);
        }
    }

    /// <summary>
    /// ClientRpc for synchronising the visual state of this trap
    /// </summary>
    /// <param name="state"></param>
    [ClientRpc]
    private void RpcSetVisualState(bool state) {
        visuals.SetActive(state);
    }

    /// <summary>
    /// ClientRpc used for synchronising the visual state of the trap
    /// </summary>
    /// <param name="state">The state of the visuals</param>
    [ClientRpc]
    protected void RpcSetExtraVisualsState(bool state) {
        extraVisuals.SetActive(state);
    }

    /// <summary>
    /// Unity callback for when this trap is destroyed.
    /// Tells the owner that this trap is being destroyed
    /// </summary>
    protected override void OnDestroy() {
        base.OnDestroy();

        if (trapOwner != null) {
            trapOwner.DisplayTrapState(false);
        }
    }

    /// <summary>
    /// Sets the visual state of this trap
    /// </summary>
    /// <param name="state">The visual state of this trap</param>
    public void SetVisualState(bool state) {
        visuals.SetActive(state);
    }

    /// <summary>
    /// A virtual function that allows children of this class to handle what they want to do when a trap is triggered.
    /// </summary>
    /// <param name="playerStatus">The PlayerStatus component of the triggered player</param>
    public virtual void HandleTrigger(PlayerStatus playerStatus) { }
}
