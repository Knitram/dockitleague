﻿using UnityEngine;
using UnityEngine.Networking;

public class DockingKitPickup : NetworkBehaviour {
    [SyncVar(hook = "OnSetDockingKitId")]
    public DockingKitId dockingKitId = DockingKitId.Empty;
    private bool available = true;

    /// <summary>
    /// Calls the SyncVar hook manually to get the correct initial state. Used by clients connecting after pickup already spawned. 
    /// </summary>
    public override void OnStartClient() {
        if(!isServer && dockingKitId != DockingKitId.Empty) {
            OnSetDockingKitId(dockingKitId);
        }
    }

    /// <summary>
    /// Server call from the Docking called when a player tries to dock.
    /// </summary>
    /// <param name="player">Reference to the player docking.</param>
    [Server]
    public void OnPlayerDocking(GameObject player) {
        if(available) {
            available = false;
            player.GetComponent<Docking>().CmdSetDockingKitId(dockingKitId);
            NetworkServer.Destroy(gameObject);
        }
    }

    /// <summary>
    /// SyncVar hook for spawning the DockingKit visuals for the given new DockingKitId.
    /// </summary>
    /// <param name="kitId">The DockingKitId for this pickup.</param>
    private void OnSetDockingKitId(DockingKitId kitId) {
        dockingKitId = kitId;
        GameObject pickup = Instantiate(GameManager.Instance.GetDockingKit(dockingKitId), transform) as GameObject;
        pickup.transform.localPosition = Vector3.zero;
        pickup.transform.localRotation = Quaternion.identity;
    }
}
