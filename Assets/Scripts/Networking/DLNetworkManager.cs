﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

// Based on https://www.assetstore.unity3d.com/en/#!/content/41836
public class DLNetworkManager : NetworkLobbyManager {
    [HideInInspector]
    public int playerCount = 0;

    /// <summary>
    /// Updates the playerCount variable by adding the parameter
    /// </summary>
    /// <param name="count">The amount of new players</param>
    public void OnPlayerNumberModified(int count) {
        playerCount += count;
    }

    // --- Server Callbacks ---

    /// <summary>
    /// A callback for when all players are ready and the game is about to start.
    /// It takes each lobby player and applies the saved data of those to the actual game players
    /// </summary>
    /// <param name="lobbyPlayer">The lobby player</param>
    /// <param name="gamePlayer">The game player that we are transferring data to</param>
    /// <returns></returns>
    public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer) {
        base.OnLobbyServerSceneLoadedForPlayer(lobbyPlayer, gamePlayer);

        return true;
    }

    public override void OnLobbyServerDisconnect(NetworkConnection conn) {
        base.OnLobbyServerDisconnect(conn);
        CheckReadyToBegin();
    }

    /// <summary>
    /// Callback what the server has to do once it creates a lobby player
    /// The server instantiates the player and toggles relevant UI for all players
    /// </summary>
    /// <param name="conn">The network connection. Currently not used</param>
    /// <param name="playerControllerId">The local player contoller Id. Currently not used</param>
    /// <returns>The instantiated lobby player object</returns>
    public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId) {
        GameObject obj = Instantiate(lobbyPlayerPrefab.gameObject);

        DLNetworkLobbyPlayer newPlayer = obj.GetComponent<DLNetworkLobbyPlayer>();
        newPlayer.ToggleReadyButton(numPlayers + 1 >= minPlayers);

        for (int i = 0; i < lobbySlots.Length; ++i) {
            DLNetworkLobbyPlayer p = (DLNetworkLobbyPlayer)lobbySlots[i];
            if (p != null) {
                p.ToggleReadyButton(numPlayers + 1 >= minPlayers);
                p.CmdUpdateReadyState(false);
            }
        }
        return obj;
    }

    // --- Client Callbacks ---

    /// <summary>
    /// Callback for handling client errors.
    /// It currently only sends the player out of the lobby.
    /// </summary>
    /// <param name="conn">The network connection</param>
    /// <param name="errorCode">The error code</param>
    public override void OnClientError(NetworkConnection conn, int errorCode) {
        base.OnClientError(conn, errorCode);
        GameObject obj = GameObject.FindGameObjectWithTag("MenuHandler");
        if (obj) {
            obj.GetComponent<MainMenuHandler>().NavigateBack();
        }
    }
}
