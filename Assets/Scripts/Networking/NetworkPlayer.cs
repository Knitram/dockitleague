﻿using System;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkPlayer : NetworkBehaviour {
    public event Action<NetworkPlayer> syncVarsChanged;
    // Server only event
    public event Action<NetworkPlayer> becameReady;

    public event Action gameDetailsReady;

    [SerializeField]
    protected GameObject playerPrefab;
    [SerializeField]
    protected GameObject lobbyPrefab;

    // Set by commands
    [SyncVar(hook = "OnMyName")]
    private string playerName = "";
    [SyncVar(hook = "OnMyTeam")]
    private TeamId playerTeamId = TeamId.Unassigned;
    [SyncVar(hook = "OnReadyChanged")]
    private bool ready = false;

    // Set on the server only
    [SyncVar(hook = "OnHasInitialized")]
    private bool isInitialized = false;
    [SyncVar]
    private int playerId;

    private NetworkManager networkManager;

    /// <summary>
    /// Gets this player's id
    /// </summary>
    public int PlayerId {
        get { return playerId; }
    }

    /// <summary>
    /// Gets this player's name
    /// </summary>
    public string PlayerName {
        get { return playerName; }
    }

    /// <summary>
    /// Gets this player's team id
    /// </summary>
    public TeamId PlayerTeamId {
        get { return playerTeamId; }
    }

    /// <summary>
    /// Gets whether this player has marked themselves as ready in the lobby
    /// </summary>
    public bool IsReady {
        get { return ready; }
    }

    /// <summary>
    /// Gets the player script associated with this player
    /// </summary>
    public Player PlayerInstance {
        get;
        set;
    }

    /// <summary>
    /// Gets the lobby object associated with this player
    /// </summary>
    public LobbyPlayer LobbyObject {
        get;
        private set;
    }

    /// <summary>
    /// Gets the local NetworkPlayer object
    /// </summary>
    public static NetworkPlayer LocalPlayerInstance {
        get;
        private set;
    }

    /// <summary>
    /// Set initial values
    /// </summary>
    [Client]
    public override void OnStartLocalPlayer() {
        base.OnStartLocalPlayer();
        Debug.Log("Local Network Player start");
        CmdSetInitialValues();

        LocalPlayerInstance = this;
    }

    /// <summary>
    /// Register us with the NetworkManager
    /// </summary>
    [Client]
    public override void OnStartClient() {
        DontDestroyOnLoad(this);

        if(networkManager == null) {
            networkManager = NetworkManager.Instance;
        }

        base.OnStartClient();
        Debug.Log("Client Network Player start");

        networkManager.RegisterNetworkPlayer(this);
    }

    /// <summary>
    /// Get network manager
    /// </summary>
    protected virtual void Start() {
        if(networkManager == null) {
            networkManager = NetworkManager.Instance;
        }
    }

    /// <summary>
    /// Deregister us with the manager
    /// </summary>
    public override void OnNetworkDestroy() {
        base.OnNetworkDestroy();
        Debug.Log("Client Network Player OnNetworkDestroy");

        if(LobbyObject != null) {
            Destroy(LobbyObject.gameObject);
        }

        if(networkManager != null) {
            networkManager.DeregisterNetworkPlayer(this);
        }
    }

    /// <summary>
    /// Fired when we enter the game scene
    /// </summary>
    [Client]
    public void OnEnterGameScene() {
        if(hasAuthority) {
            CmdClientReadyInScene();
        }
    }

    /// <summary>
    /// Fired when we return to the lobby scene, or are first created in the lobby
    /// </summary>
    [Client]
    public void OnEnterLobbyScene() {
        Debug.Log("OnEnterLobbyScene");
        if(isInitialized && LobbyObject == null) {
            CreateLobbyObject();
        }
    }


    [Server]
    public void ClearReady() {
        ready = false;
    }


    [Server]
    public void SetPlayerName(string newName) {
        playerName = newName;
    }

    [Server]
    public void SetPlayerId(int newPlayerId) {
        playerId = newPlayerId;
    }

    /// <summary>
    /// Clean up lobby object for us
    /// </summary>
    protected virtual void OnDestroy() {
        if(LobbyObject != null) {
            Destroy(LobbyObject.gameObject);
        }
    }


    /// <summary>
    /// Create our lobby object
    /// </summary>
    private void CreateLobbyObject() {
        LobbyObject = Instantiate(lobbyPrefab).GetComponent<LobbyPlayer>();
        LobbyObject.Init(this);
    }

    [Server]
    private void SwapTeam() {
        playerTeamId = (playerTeamId == TeamId.Red) ? TeamId.Blue : TeamId.Red;
    }

    [ClientRpc]
    public void RpcSetGameSettings(int mapIndex, int modeIndex) {
        GameSettings settings = networkManager.GetGameSettings();
        if(!isServer) {
            settings.SetMapIndex(mapIndex);
            settings.SetModeIndex(modeIndex);
        }
        if(gameDetailsReady != null && isLocalPlayer) {
            gameDetailsReady();
        }
    }

    [ClientRpc]
    public void RpcPrepareForLoad() {
        if(isLocalPlayer) {
            // Show loading screen
            LoadingModal loading = LoadingModal.Instance;

            if(loading != null) {
                loading.FadeIn();
            }
        }
    }

    #region Commands

    /// <summary>
    /// Create our player
    /// </summary>
    [Command]
    private void CmdClientReadyInScene() {
        Debug.Log("CmdClientReadyInScene");
        GameObject playerObject = Instantiate(playerPrefab);
        NetworkServer.SpawnWithClientAuthority(playerObject, connectionToClient);

        PlayerInstance = playerObject.GetComponent<Player>();
        PlayerInstance.SetPlayerId(playerId);
        RpcSetLocalPlayerInstance(playerObject);
    }

    [ClientRpc]
    private void RpcSetLocalPlayerInstance(GameObject localPlayerObject) {
        PlayerInstance = localPlayerObject.GetComponent<Player>();
    }

    [Command]
    private void CmdSetInitialValues() {
        playerTeamId = networkManager.GetInitialTeamId();
        isInitialized = true;
    }

    [Command]
    public void CmdTeamChange() {
        Debug.Log("CmdTeamChange");
        SwapTeam();
    }

    [Command]
    public void CmdNameChanged(string name) {
        Debug.Log("CmdNameChanged");
        playerName = name;
    }

    [Command]
    public void CmdSetReady() {
        Debug.Log("CmdSetReady");
        if(networkManager.hasSufficientPlayers) {
            ready = true;

            if(becameReady != null) {
                becameReady(this);
            }
        }
    }

    #endregion

    #region Syncvar callbacks

    private void OnMyName(string newName) {
        playerName = newName;

        if(syncVarsChanged != null) {
            syncVarsChanged(this);
        }
    }

    private void OnMyTeam(TeamId newTeamId) {
        playerTeamId = newTeamId;

        if(syncVarsChanged != null) {
            syncVarsChanged(this);
        }
    }

    private void OnReadyChanged(bool value) {
        ready = value;

        if(syncVarsChanged != null) {
            syncVarsChanged(this);
        }
    }

    private void OnHasInitialized(bool value) {
        if(!isInitialized && value) {
            isInitialized = value;
            CreateLobbyObject();

            if(isServer) {
                GameSettings settings = networkManager.GetGameSettings();
                RpcSetGameSettings(settings.MapIndex, settings.ModeIndex);
            }
        }
    }

    #endregion
}
