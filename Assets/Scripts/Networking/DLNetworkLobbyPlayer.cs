﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Based on https://www.assetstore.unity3d.com/en/#!/content/41836
public class DLNetworkLobbyPlayer : NetworkLobbyPlayer {
    // For now we'll just use the colors to set teams
    static Color[] Colors = new Color[] { Color.red, Color.blue };

    // These are children of the lobbyplayer object
    public Button colorButton;
    public InputField nameInput;
    public Button readyButton;
    public GameObject visuals;

    [SyncVar(hook = "OnNameChange")]
    public string playerName = "";
    [SyncVar(hook = "OnColorChange")]
    public Color playerColor = Color.white;
    [SyncVar(hook = "OnReadyStateChange")]
    public bool isReady = false;

    private DLNetworkManager networkManager;
    private LobbyHandler lobbyHandler;
    private MainMenuHandler mainMenuHandler;

    void Awake() {
        networkManager = (DLNetworkManager)NetworkManager.singleton;
        mainMenuHandler = GameObject.FindGameObjectWithTag("MenuHandler").GetComponent<MainMenuHandler>();
        lobbyHandler = mainMenuHandler.lobbyHandler;
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Callback that initialises all necessary data for when a player enters the lobby.
    /// This includes player name, player color, adding the player to a team and telling the network manager that a new player has joined
    /// </summary>
    public override void OnClientEnterLobby() {
        base.OnClientEnterLobby();

        if (networkManager != null) {
            networkManager.OnPlayerNumberModified(1);
        }

        if (lobbyHandler != null) {
            lobbyHandler.AddPlayer(this);
        }

        if (isLocalPlayer) {
            SetupLocalPlayer();   
        } else {
            SetupOtherPlayer();
        }

        lobbyHandler.DisplayLobby();
        
        OnNameChange(playerName);
        OnColorChange(playerColor);
    }

    /// <summary>
    /// Callback that simply sets up the client side parts of a lobby for the connecting player
    /// </summary>
    public override void OnStartAuthority() {
        base.OnStartAuthority();
        SetupLocalPlayer();
    }

    /// <summary>
    /// Takes a color as a parameter and changes the color of the "ready" button of this player
    /// This is primarily used to make all non local players appear uninteractable for the client. 
    /// </summary>
    /// <param name="c">The color we are changing to</param>
    private void ChangeReadyButtonColor(Color c) {
        ColorBlock b = readyButton.colors;
        b.normalColor = c;
        b.pressedColor = c;
        b.highlightedColor = c;
        b.disabledColor = c;
        readyButton.colors = b;
    }

    /// <summary>
    /// Like with ChangeReaduButtonColor(Color c) this function helps make the components of non-client players uninteractable 
    /// </summary>
    private void SetupOtherPlayer() {
        nameInput.interactable = false;

        colorButton.gameObject.SetActive(false);
        readyButton.GetComponentInChildren<Text>().text = isReady ? "Ready" : "Not Ready";
        readyButton.interactable = false;

        OnClientReady(isReady);
    }

    /// <summary>
    /// Setups the client.
    /// Adds listeners for button clicks and makes the UI for this player interactable
    /// </summary>
    private void SetupLocalPlayer() {
        nameInput.interactable = true;

        if (playerColor == Color.white) {
            CmdColorChange();
        }

        readyButton.GetComponentInChildren<Text>().text = "Not Ready";
        readyButton.interactable = true;
        colorButton.gameObject.SetActive(true);

        if (playerName == "" && lobbyHandler != null) {
            CmdNameChanged("Player " + (lobbyHandler.GetPlayerCount()));
        } else {
            Debug.LogError("ERROR: Failed to assign new player name. LobbyHandler reference might be gone");
        }

        colorButton.interactable = true;
        nameInput.interactable = true;

        nameInput.onEndEdit.RemoveAllListeners();
        nameInput.onEndEdit.AddListener(OnNameChanged);

        colorButton.onClick.RemoveAllListeners();
        colorButton.onClick.AddListener(OnColorClicked);

        readyButton.onClick.RemoveAllListeners();
        readyButton.onClick.AddListener(OnReadyClicked);

        StartCoroutine(mainMenuHandler.SetFirstSelectedGameObject(null));
    }

    /// <summary>
    /// Makes the local UI uninteractable once the player has chosen to be ready
    /// </summary>
    /// <param name="readyState">Whether the client is ready or not</param>
    public override void OnClientReady(bool readyState) {
        if (readyState) {
            Text textComponent = readyButton.GetComponentInChildren<Text>();
            textComponent.text = "Ready";
            readyButton.interactable = isLocalPlayer;
            colorButton.interactable = false;
            nameInput.interactable = false;
        } else {
            Text textComponent = readyButton.GetComponentInChildren<Text>();
            textComponent.text = "Not Ready";
            readyButton.interactable = isLocalPlayer;
            colorButton.interactable = isLocalPlayer;
            nameInput.interactable = isLocalPlayer;
        }
    }

    /// <summary>
    /// SyncVarHook for handling name changes
    /// </summary>
    /// <param name="newName">The new player name</param>
    public void OnNameChange(string newName) {
        playerName = newName;
        nameInput.text = playerName;
    }

    /// <summary>
    /// SyncVarHook for handling color changes
    /// </summary>
    /// <param name="newColor">The new team color</param>
    public void OnColorChange(Color newColor) {
        playerColor = newColor;
        colorButton.GetComponent<Image>().color = newColor;
        lobbyHandler.SetPlayerTeam(this);
    }

    /// <summary>
    /// SyncVarHook for handling ready states
    /// </summary>
    /// <param name="state">If the is player ready or not</param>
    public void OnReadyStateChange(bool state) {
        isReady = state;
        if (!isLocalPlayer) {
            SetupOtherPlayer();
        }

        if (isLocalPlayer && state) {
            SendReadyToBeginMessage();
        } else if (isLocalPlayer && !state){
            SendNotReadyToBeginMessage();
        }
    }

    // --- UI Handling ---

    /// <summary>
    /// A function that simply calls the CmdColorChange() command
    /// </summary>
    public void OnColorClicked() {
        CmdColorChange();
    }

    /// <summary>
    /// A simple function that tells the network that this player is ready to begin
    /// </summary>
    public void OnReadyClicked() {
        CmdUpdateReadyState(!isReady);
    }

    /// <summary>
    /// A simple function that calls the CmdNameChanged(str) command
    /// </summary>
    /// <param name="str">The new player name</param>
    public void OnNameChanged(string str) {
        CmdNameChanged(str);
    }

    /// <summary>
    /// Sets the state of the ready button on the UI to the parameter one
    /// </summary>
    /// <param name="enabled">The state of the button</param>
    public void ToggleReadyButton(bool enabled) {
        readyButton.gameObject.SetActive(enabled);
    }

    /// <summary>
    /// Returns the UI elements of a player
    /// </summary>
    /// <returns>The player visuals</returns>
    public GameObject GetVisuals() {
        return visuals;
    }

    // --- Server commands ---

    /// <summary>
    /// Updates the server when a player has chosen a new team/color
    /// </summary>
    [Command]
    public void CmdColorChange() {
        int idx = System.Array.IndexOf(Colors, playerColor);

        if (idx < 0) {
            idx = 0;
        }
        idx = (idx + 1) % Colors.Length;
        playerColor = Colors[idx];
    }

    /// <summary>
    /// Updates the server when a player has chosen a new name
    /// </summary>
    /// <param name="name">The new player name</param>
    [Command]
    public void CmdNameChanged(string name) {
        playerName = name;
    }

    /// <summary>
    /// Updates the server when a player is ready
    /// </summary>
    /// <param name="state">The ready state</param>
    [Command]
    public void CmdUpdateReadyState(bool state) {
        isReady = state;
    }

    /// <summary>
    /// Callback for when a lobby player leaves the lobby and gets destroyed
    /// It tells the lobbyHandler to remove this player and tells the networkManager that a player has left.
    /// </summary>
    public void OnDestroy() {
        if (lobbyHandler != null) {
            lobbyHandler.RemovePlayer(this);
        }
        if (networkManager != null) {
            networkManager.OnPlayerNumberModified(-1);
        }
    }
}
