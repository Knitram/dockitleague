﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handler for the player UI (Abilities, status modifiers, health).
/// </summary>
public class PlayerUIHandler : MonoBehaviour {
    public AbilityUI[] abilities;
    public Sprite emptySlot;

    public Transform[] statusBars;
    public GameObject statusPrefab;

    public Text currencyText;
    public Text animatedCurrencyText;

    private List<StatusUI> statusElements;

    public Text healthPercentageText;
    public Text healthRatioText;
    public Image healthMask;

    public Color currencyAddColor;
    public Color currencyRemoveColor;
    public float animatedTextTargetOffset = 75f;
    public IngameMenuHandler ingameMenuHandler;

    private Vector3 animatedTextStartPosition;

    void Awake() {
        statusElements = new List<StatusUI>();
        animatedTextStartPosition = animatedCurrencyText.GetComponent<RectTransform>().anchoredPosition;

        foreach(AbilityUI ab in abilities) {
            ab.Initialize(this);
        }
    }

    /// <summary>
    /// Initialize the AbilityUI with the new DockingKit abilities.
    /// </summary>
    /// <param name="newDockingKit">Reference to the new DockingKit.</param>
    public void SetDockingKitUI(DockingKit newDockingKit) {
        ClearAbilityUI();

        for(int i = 0; i < newDockingKit.abilities.Count; i++) {
            abilities[i].SetAbility(newDockingKit.abilities[i]);
        }
    }

    /// <summary>
    /// Resets the Ability UI to its original empty state.
    /// </summary>
    private void ClearAbilityUI() {
        foreach(AbilityUI ab in abilities) {
            ab.ClearAbility(emptySlot);
        }
    }

    /// <summary>
    /// Adds a StatusUI element to the PlayerUI.
    /// </summary>
    /// <param name="modifier">The modifier to be added.</param>
    /// <param name="duration">The initial duration of the status modifier.</param>
    /// <returns>The instantiated statusUI element.</returns>
    public StatusUI AddStatusModifier(Modifier modifier, float duration) {
        StatusUI statusUI = (Instantiate(statusPrefab) as GameObject).GetComponent<StatusUI>();
        statusUI.Initialize(this, modifier.icon, modifier.statusType, duration);
        statusUI.transform.SetParent(statusBars[(int)modifier.statusType], false);

        statusElements.Add(statusUI);
        return statusUI;
    }

    /// <summary>
    /// Removed the status modifier from the list of elements.
    /// </summary>
    /// <param name="statusModifier">The statusUI removed.</param>
    public void RemoveStatusModifier(StatusUI statusModifier) {
        statusElements.Remove(statusModifier);
    }

    /// <summary>
    /// Updates the HealthUI based on health and maxHealth.
    /// </summary>
    /// <param name="health">The current health.</param>
    /// <param name="maxHealth">The current max health.</param>
    public void SetCurrentHealth(float health, float maxHealth) {
        //Ceiling gives a better representation of health, especially in the 0-1 range (health < 0.5 would display as 0 for instance).
        health = Mathf.CeilToInt(health);

        float healthRatio = health / maxHealth;

        healthMask.fillAmount = healthRatio;
        healthPercentageText.text = healthRatio.ToString("0%");
        healthRatioText.text = string.Format("{0}/{1}", health.ToString("0"), maxHealth.ToString("0"));
    }

    /// <summary>
    /// Starts a coroutine that interpolates text containing the amount of currency earned/spent
    /// </summary>
    /// <param name="currencyDifference">The currency difference from the old total</param>
    public void PlayCurrencyChangeAnimation(float currencyDifference) {
        if (ingameMenuHandler.shopMenu.activeSelf) {
            ingameMenuHandler.CheckPriceAndEquipAvailability();
        }

        bool differenceIsNegative = (currencyDifference < 0);
        animatedCurrencyText.color = differenceIsNegative ? currencyRemoveColor : currencyAddColor;
        animatedCurrencyText.text = (differenceIsNegative ? "" : "+") + currencyDifference.ToString();

        var animatedTextTransform = animatedCurrencyText.GetComponent<RectTransform>();
        animatedTextTransform.anchoredPosition = animatedTextStartPosition;

        StopCoroutine("AnimatedTextPositionLerp");
        StartCoroutine(AnimatedTextPositionLerp(animatedCurrencyText.GetComponent<RectTransform>().anchoredPosition.y + animatedTextTargetOffset, 2f));
    }

    /// <summary>
    /// Coroutine that interpolates the currency earned/spent text's y position to the given target position
    /// </summary>
    /// <param name="targetPositionY">The target y position we interpolate towards</param>
    /// <param name="speed">The speed of the interpolation</param>
    private IEnumerator AnimatedTextPositionLerp(float targetPositionY, float speed) {
        animatedCurrencyText.gameObject.SetActive(true);
        var textTransform = animatedCurrencyText.GetComponent<RectTransform>();
        while (Mathf.Abs(targetPositionY - textTransform.anchoredPosition.y) > 5f) {
            textTransform.anchoredPosition = new Vector2(textTransform.anchoredPosition.x, 
                                                      Mathf.Lerp(textTransform.anchoredPosition.y, targetPositionY, speed * Time.deltaTime));
            yield return null;
        }

        animatedCurrencyText.gameObject.SetActive(false);
    }
}
