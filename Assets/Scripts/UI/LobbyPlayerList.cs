﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles the player list in the Lobby.
/// </summary>
public class LobbyPlayerList : MonoBehaviour {
    [SerializeField]
    private GameObject singleLobby;
    [SerializeField]
    private GameObject teamLobby;

    [SerializeField]
    private RectTransform playerListContentTransform;
    [SerializeField]
    private RectTransform team1PlayerListContentTransform;
    [SerializeField]
    private RectTransform team2PlayerListContentTransform;

    private NetworkManager networkManager;

    /// <summary>
    /// Subscribe to events on start
    /// </summary>
    protected virtual void Start() {
        networkManager = NetworkManager.Instance;
        if(networkManager != null) {
            networkManager.playerJoined += PlayerJoined;
            networkManager.playerLeft += PlayerLeft;
            networkManager.serverPlayersReadied += PlayersReadied;
        }
    }

    /// <summary>
    /// Unsubscribe to events on destroy
    /// </summary>
    protected virtual void OnDestroy() {
        if(networkManager != null) {
            networkManager.playerJoined -= PlayerJoined;
            networkManager.playerLeft -= PlayerLeft;
            networkManager.serverPlayersReadied -= PlayersReadied;
        }
    }

    //Add lobby player to UI
    public void AddPlayer(LobbyPlayer player, TeamId teamId) {
        if(teamId == TeamId.Red) {
            player.transform.SetParent(team1PlayerListContentTransform, false);
        } else if(teamId == TeamId.Blue) {
            player.transform.SetParent(team2PlayerListContentTransform, false);
        } else {
            //Debug.LogError("Trying to add player with unassigned TeamId.");
            player.transform.SetParent(playerListContentTransform, false);
        }
    }

    //Log player joining for tracing
    protected virtual void PlayerJoined(NetworkPlayer player) {
        Debug.LogFormat("Player joined {0}", player.name);
    }

    //Log player leaving for tracing
    protected virtual void PlayerLeft(NetworkPlayer player) {
        Debug.LogFormat("Player left {0}", player.name);
    }

    //When players are all ready progress
    protected virtual void PlayersReadied() {
        networkManager.ProgressToGameScene();
    }

    void OnEnable() {
        GameSettings settings = NetworkManager.Instance.GetGameSettings();
        if(settings.Mode.IsTeamMode()) {
            teamLobby.SetActive(true);
        } else {
            singleLobby.gameObject.SetActive(true);
        }
    }

    void OnDisable() {
        teamLobby.SetActive(false);
        singleLobby.gameObject.SetActive(false);
    }

    //On click of back button
    public void OnBackClick() {
        networkManager.Disconnect();
        MainMenuUI.Instance.ShowDefaultPanel();
    }
}
