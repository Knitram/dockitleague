﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MatchListHandler : MonoBehaviour {
    public GameObject dynamicMatchButtonPrefab;
    public int matchButtonOffset = 90;
    public MainMenuHandler mainMenuHandler;
    public GameObject lobbyObj;
    public GameObject lobbyVerfifPromptObj;
    public GameObject noMatchesFoundObj;

    private DLNetworkManager networkManager;
    private bool matchListUpdated;

    void OnGUI() {
        if (networkManager.matches != null && !matchListUpdated) {
            SetupMatchList();

            if (networkManager.matches.Count == 0) {
                noMatchesFoundObj.SetActive(true);
            } else {
                noMatchesFoundObj.SetActive(false);
            }
        }
    }

    void OnEnable() {
        networkManager = (DLNetworkManager)NetworkManager.singleton;
        networkManager.StartMatchMaker();
        ListOnlineMatches();
    }

    void OnDisable() {
        foreach (Button child in GetComponentsInChildren<Button>()) {
            Destroy(child.gameObject);
        }
        networkManager.matches = null;
        matchListUpdated = false;
        noMatchesFoundObj.SetActive(false);
    }

    /// <summary>
    /// Calls the match maker to list all matches for the player
    /// </summary>
    private void ListOnlineMatches() {
        networkManager.matchMaker.ListMatches(0, 20, "", false, 0, 0, networkManager.OnMatchList);
    }

    /// <summary>
    /// A button listener callback that makes the client join the match that has been selected.
    /// Also navigates to the lobby menu screen.
    /// </summary>
    /// <param name="buttonNumber">The index of the button</param>
    /// <param name="match">The match maker match</param>
    public void OnMatchButtonClick(int buttonNumber, UnityEngine.Networking.Match.MatchInfoSnapshot match) {
        networkManager.matchName = match.name;
        networkManager.matchMaker.JoinMatch(match.networkId, "", "", "", 0, 0, networkManager.OnMatchJoined);

        mainMenuHandler.NavigateTo(lobbyObj);
        mainMenuHandler.SetCurrentMenuVerificationPrompt(lobbyVerfifPromptObj);
    }

    /// <summary>
    /// Dynamically creates buttons for all available matches and attaches the listeners to each one.
    /// </summary>
    private void SetupMatchList() {
        // based on http://answers.unity3d.com/questions/875588/unity-ui-dynamic-buttons.html
        int deltaPosY = 0;

        // Add a button for each match
        for (int i = 0; i < networkManager.matches.Count; i++) {
            var match = networkManager.matches[i];
            GameObject matchButton = Instantiate(dynamicMatchButtonPrefab, new Vector3(0, deltaPosY, 0), Quaternion.identity);
            matchButton.transform.SetParent(transform, false);
            matchButton.transform.localScale = Vector3.one;

            Button tempButton = matchButton.GetComponent<Button>();
            int tempIndex = i;

            Text tempButtonText = tempButton.GetComponentInChildren<Text>();
            tempButtonText.text = "Join Match:" + match.name;

            tempButton.onClick.AddListener(() => OnMatchButtonClick(tempIndex, match));
            deltaPosY -= matchButtonOffset;
        }
        StartCoroutine(mainMenuHandler.SetFirstSelectedGameObject(null));
        matchListUpdated = true;
    }
}
