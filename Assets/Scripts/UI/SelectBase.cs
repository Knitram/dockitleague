﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectBase : MonoBehaviour {
    protected int currentIndex = 0;
    protected int listLength;

    public int GetCurrentIndex() {
        return currentIndex;
    }

    //Called by button
    public void OnNextClick() {
        currentIndex++;
        OnIndexChange();
    }

    //Called by button
    public void OnPreviousClick() {
        currentIndex--;
        OnIndexChange();
    }

    //Called to force currentIndex to be within the bounds and handle updating the UI
    protected void OnIndexChange() {
        HandleBounds();
        AssignByIndex();
    }

    //Base method for updating the UI to reflect the current index
    protected virtual void AssignByIndex() {
    }

    //Force the currentIndex to be within the list bounds
    protected void HandleBounds() {
        if(currentIndex < 0) {
            currentIndex = listLength - 1;
        }

        if(currentIndex >= listLength) {
            currentIndex = 0;
        }
    }
}
