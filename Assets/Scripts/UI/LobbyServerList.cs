﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

//UI view list of available games
public class LobbyServerList : MonoBehaviour {
    [SerializeField]
    private int gamesPerPage = 6;

    [SerializeField]
    private float listAutoRefreshTime = 60f;
    private float nextRefreshTime;

    [SerializeField]
    private Button nextButton;
    [SerializeField]
    private Button previousButton;

    [SerializeField]
    private Text pageNumber;

    [SerializeField]
    private RectTransform serverListRect;
    [SerializeField]
    private GameObject serverEntryPrefab;
    [SerializeField]
    private GameObject noServerFound;

    //Page tracking
    private int currentPage = 0;
    private int previousPage = 0;
    private int newPage = 0;

    static Color OddServerColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    static Color EvenServerColor = new Color(.94f, .94f, .94f, 1.0f);

    //Cached singletons
    private NetworkManager networkManager;
    private MainMenuUI menuUi;


    protected virtual void OnEnable() {
        //Cache singletons
        if(networkManager == null) {
            networkManager = NetworkManager.Instance;
        }
        if(menuUi == null) {
            menuUi = MainMenuUI.Instance;
        }

        //Reset pages
        currentPage = 0;
        previousPage = 0;

        ClearUi();

        //Disable NO SERVER FOUND error message
        noServerFound.SetActive(false);

        nextRefreshTime = Time.time;

        //Subscribe to network events
        if(networkManager != null) {
            networkManager.clientDisconnected += OnDisconnect;
            networkManager.clientError += OnError;
            networkManager.serverError += OnError;
            networkManager.matchDropped += OnDrop;
        }
    }

    protected void ClearUi() {
        foreach(Transform t in serverListRect) {
            Destroy(t.gameObject);
        }
    }

    protected virtual void OnDisable() {
        //Unsubscribe from network events
        if(networkManager != null) {
            networkManager.clientDisconnected -= OnDisconnect;
            networkManager.clientError -= OnError;
            networkManager.serverError -= OnError;
            networkManager.matchDropped -= OnDrop;
        }
    }

    //Network event
    protected virtual void OnError(UnityEngine.Networking.NetworkConnection conn, int errorCode) {
        if(menuUi != null) {
            menuUi.ShowDefaultPanel();
            menuUi.ShowInfoPopup("A connection error occurred", null);
        }

        if(networkManager != null) {
            networkManager.Disconnect();
        }
    }

    //Network event
    protected virtual void OnDisconnect(UnityEngine.Networking.NetworkConnection conn) {
        if(menuUi != null) {
            menuUi.ShowDefaultPanel();
            menuUi.ShowInfoPopup("Disconnected from server", null);
        }

        if(networkManager != null) {
            networkManager.Disconnect();
        }
    }

    //Network event
    protected virtual void OnDrop() {
        if(menuUi != null) {
            menuUi.ShowDefaultPanel();
            menuUi.ShowInfoPopup("Disconnected from server", null);
        }

        if(networkManager != null) {
            networkManager.Disconnect();
        }
    }

    //Check for refresh
    protected virtual void Update() {
        if(nextRefreshTime <= Time.time) {
            RequestPage(currentPage);

            nextRefreshTime = Time.time + listAutoRefreshTime;
        }
    }

    //On click of back button
    public void OnBackClick() {
        networkManager.Disconnect();
        menuUi.ShowDefaultPanel();
    }

    //Callback for request
    public void OnGuiMatchList(bool flag, string extraInfo, List<MatchInfoSnapshot> response) {
        //If no response do nothing
        if(response == null) {
            return;
        }

        nextButton.interactable = true;
        previousButton.interactable = true;

        previousPage = currentPage;
        currentPage = newPage;

        //if nothing is returned
        if(response.Count == 0) {
            //current page is 0 then set enable NO SERVER FOUND message
            if(currentPage == 0) {
                noServerFound.SetActive(true);
                ClearUi();
                previousButton.interactable = false;
                nextButton.interactable = false;
                pageNumber.enabled = false;
            }

            currentPage = previousPage;

            return;
        }

        //Prev button should not be interactable for first (zeroth) page
        previousButton.interactable = currentPage > 0;
        //Next button should not be interactable if the current page is not full
        nextButton.interactable = response.Count == gamesPerPage;

        noServerFound.SetActive(false);

        //Handle page number
        pageNumber.enabled = true;
        pageNumber.text = (currentPage + 1).ToString();

        //Clear all transforms
        foreach(Transform t in serverListRect)
            Destroy(t.gameObject);

        //Instantiate UI gameObjects
        for(int i = 0; i < response.Count; ++i) {
            GameObject o = Instantiate(serverEntryPrefab);

            o.GetComponent<LobbyServerEntry>().Populate(response[i], (i % 2 == 0) ? OddServerColor : EvenServerColor);

            o.transform.SetParent(serverListRect, false);
        }
    }

    //Called by button clicks
    public void ChangePage(int dir) {
        int newPage = Mathf.Max(0, currentPage + dir);
        this.newPage = newPage;

        //if we have no server currently displayed, need we need to refresh page0 first instead of trying to fetch any other page
        if(noServerFound.activeSelf)
            newPage = 0;

        RequestPage(newPage);
    }

    //Handle requests
    public void RequestPage(int page) {
        if(networkManager != null && networkManager.matchMaker != null) {
            nextButton.interactable = false;
            previousButton.interactable = false;

            Debug.Log("Requesting match list");
            networkManager.matchMaker.ListMatches(page, gamesPerPage, string.Empty, false, 0, 0, OnGuiMatchList);
        }
    }

    //We just set the autorefresh time to RIGHT NOW when this button is pushed, triggering all the refresh logic in the next Update tick.
    public void RefreshList() {
        nextRefreshTime = Time.time;
    }
}
