﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectMode : SelectBase {
    [SerializeField]
    private ModeList modeList;

    [SerializeField]
    private Text modeName;

    [SerializeField]
    private Text description;

    public ModeInfo GetSelectedMode() {
        return modeList[currentIndex];
    }

    private void Awake() {
        listLength = modeList.Count;
        OnIndexChange();
    }

    protected override void AssignByIndex() {
        ModeInfo info = modeList[currentIndex];
        modeName.text = info.GetModeName();
        description.text = info.GetDescription();
    }
}
