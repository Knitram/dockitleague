﻿using System;
using UnityEngine;

public enum MenuPage {
    Home,
    Lobby
}

/// <summary>
/// Handles main menu UI and transitions
/// </summary>
public class MainMenuUI : Singleton<MainMenuUI> {
    #region Static config

    public static MenuPage ReturnPage;

    #endregion

    #region Fields

    [SerializeField]
    private CanvasGroup defaultPanel;
    [SerializeField]
    private CanvasGroup createGamePanel;
    [SerializeField]
    private CanvasGroup lobbyPanel;
    [SerializeField]
    private CanvasGroup serverListPanel;
    [SerializeField]
    private InfoPanel infoPanel;

    [SerializeField]
    private LobbyPlayerList playerList;


    [SerializeField]
    private GameObject quitButton;

    private CanvasGroup currentPanel;

    private Action waitTask;
    private bool readyToFireTask;

    #endregion

    public LobbyPlayerList PlayerList {
        get {
            return playerList;
        }
    }


    #region Methods

    protected virtual void Update() {
        if(readyToFireTask) {
            bool canFire = false;

            LoadingModal modal = LoadingModal.Instance;
            if(modal != null && modal.readyToTransition) {
                modal.FadeOut();
                canFire = true;
            } else if(modal == null) {
                canFire = true;
            }

            if(canFire) {
                if(waitTask != null) {
                    waitTask();
                    waitTask = null;
                }

                readyToFireTask = false;
            }
        }
    }

    protected virtual void Start() {
        LoadingModal modal = LoadingModal.Instance;
        if(modal != null) {
            modal.FadeOut();
        }

        //Used to return to correct page on return to menu
        switch(ReturnPage) {
            case MenuPage.Home:
            default:
                ShowDefaultPanel();
                break;
            case MenuPage.Lobby:
                ShowLobbyPanel();
                break;
        }
    }

    //Convenience function for showing panels
    public void ShowPanel(CanvasGroup newPanel) {
        if(currentPanel != null) {
            currentPanel.gameObject.SetActive(false);
        }

        currentPanel = newPanel;
        if(currentPanel != null) {
            currentPanel.gameObject.SetActive(true);
        }
    }

    public void ShowDefaultPanel() {
        ShowPanel(defaultPanel);
    }

    public void ShowLobbyPanel() {
        ShowPanel(lobbyPanel);
    }

    public void ShowLobbyPanelForConnection() {
        ShowPanel(lobbyPanel);
        NetworkManager.Instance.gameModeUpdated -= ShowLobbyPanelForConnection;
        HideInfoPopup();
    }

    public void ShowServerListPanel() {
        ShowPanel(serverListPanel);
    }

    /// <summary>
    /// Shows the info popup with a callback
    /// </summary>
    /// <param name="label">Label.</param>
    /// <param name="callback">Callback.</param>
    public void ShowInfoPopup(string label, UnityEngine.Events.UnityAction callback) {
        if(infoPanel != null) {
            infoPanel.Display(label, callback, true);
        }
    }

    public void ShowInfoPopup(string label) {
        if(infoPanel != null) {
            infoPanel.Display(label, null, false);
        }
    }

    public void ShowConnectingModal(bool reconnectMatchmakingClient) {
        ShowInfoPopup("Connecting...", () =>
        {
            if(NetworkManager.InstanceExists) {
                if(reconnectMatchmakingClient) {
                    NetworkManager.Instance.Disconnect();
                    NetworkManager.Instance.StartMatchingmakingClient();
                } else {
                    NetworkManager.Instance.Disconnect();
                }
            }
        });
    }

    public void HideInfoPopup() {
        if(infoPanel != null) {
            infoPanel.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Wait for network to disconnect before performing an action
    /// </summary>
    public void DoIfNetworkReady(Action task) {
        if(task == null) {
            throw new ArgumentNullException("task");
        }

        NetworkManager netManager = NetworkManager.Instance;

        if(netManager.isNetworkActive) {
            waitTask = task;

            LoadingModal modal = LoadingModal.Instance;
            if(modal != null) {
                modal.FadeIn();
            }

            readyToFireTask = false;
            netManager.clientStopped += OnClientStopped;
        } else {
            task();
        }
    }

    //Event listener
    private void OnClientStopped() {
        NetworkManager netManager = NetworkManager.Instance;
        netManager.clientStopped -= OnClientStopped;
        readyToFireTask = true;
    }

    private void GoToFindGamePanel() {
        ShowServerListPanel();
        NetworkManager.Instance.StartMatchingmakingClient();
    }

    private void GoToCreateGamePanel() {
        ShowPanel(createGamePanel);
    }

    #endregion


    #region Button events

    public void OnCreateGameClicked() {
        DoIfNetworkReady(GoToCreateGamePanel);
    }

    public void OnFindGameClicked() {
        // Set network into matchmaking search mode
        DoIfNetworkReady(GoToFindGamePanel);
    }

    public void OnQuitGameClicked() {
        Application.Quit();
    }

    #endregion
}
