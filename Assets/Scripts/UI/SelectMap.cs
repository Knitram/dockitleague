﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectMap : SelectBase {
    [SerializeField]
    private MapList mapList;

    [SerializeField]
    private Image mapPreview;

    [SerializeField]
    private Text description;
    [SerializeField]
    private Text mapNamePrompt;

    private string mapName;

    public MapInfo GetSelectedMap() {
        return mapList[currentIndex];
    }

    private void Awake() {
        listLength = mapList.Count;
        OnIndexChange();
    }

    private void OnEnable() {
        AssignByIndex();
    }

    protected override void AssignByIndex() {
        MapInfo info = mapList[currentIndex];

        mapPreview.sprite = info.GetMapImage();
        description.text = info.GetDescription();
        mapName = info.GetName();

        mapNamePrompt.text = mapName.ToUpperInvariant();
    }
}
