﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Loading modal - used to handle loading fades
/// </summary>
[RequireComponent(typeof(FadingGroup))]
public class LoadingModal : MonoBehaviour {
    private FadingGroup fader;

    [SerializeField]
    private float fadeTime = 0.5f;

    public static LoadingModal Instance {
        get;
        private set;
    }

    public bool readyToTransition {
        get {
            return fader.GetCurrentFade() == Fade.None && gameObject.activeSelf;
        }
    }

    /// <summary>
    /// Getter for Fader - used in game manager
    /// </summary>
    /// <value>The fader.</value>
    public FadingGroup Fader {
        get {
            return fader;
        }
    }

    void Awake() {
        if(Instance != null) {
            Debug.Log("<color=lightblue>Trying to create a second instance of LoadingModal</color");
            Destroy(gameObject);
        } else {
            Instance = this;
        }

        fader = GetComponent<FadingGroup>();
    }

    void OnDestroy() {
        if(Instance == this) {
            Instance = null;
        }
    }

    /// <summary>
    /// Wraps fade in on FadingGroup
    /// </summary>
    public void FadeIn() {
        Show();
        fader.StartFade(Fade.In, fadeTime);
    }

    /// <summary>
    /// Wraps fade out on FadingGroup
    /// </summary>
    public void FadeOut() {
        Show();
        fader.StartFade(Fade.Out, fadeTime, CloseModal);
    }

    public void CloseModal() {
        gameObject.SetActive(false);
    }

    public void Show() {
        gameObject.SetActive(true);
    }
}
