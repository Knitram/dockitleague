﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class controls a generic modal object used for generic status popups in the UI.
/// </summary>
public class AnnouncerModal : Singleton<AnnouncerModal> {
    [SerializeField]
    private Text body;

    protected override void Awake() {
        base.Awake();
        gameObject.SetActive(false);
    }

    public void Show(string text) {
        gameObject.SetActive(true);

        body.text = text;
    }

    public void Hide() {
        gameObject.SetActive(false);
    }
}
