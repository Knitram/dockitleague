﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Governs the Create Game functionality in the main menu.
/// </summary>
public class CreateGame : MonoBehaviour {
    [SerializeField]
    //Internal reference to the InputField used to enter the server name.
    protected InputField matchNameInput;

    //Cached references to other UI singletons.
    private MainMenuUI menuUi;
    private NetworkManager networkManager;

    [SerializeField]
    private SelectMap mapSelect;
    [SerializeField]
    private SelectMode modeSelect;

    protected virtual void Start() {
        menuUi = MainMenuUI.Instance;
        networkManager = NetworkManager.Instance;
    }

    /// <summary>
    /// Back button method. Returns to main menu.
    /// </summary>
    public void OnBackClicked() {
        menuUi.ShowDefaultPanel();
    }

    /// <summary>
    /// Create button method. Validates entered server name and launches game server.
    /// </summary>
    public void OnCreateClicked() {
        if(string.IsNullOrEmpty(matchNameInput.text)) {
            menuUi.ShowInfoPopup("Server name cannot be empty!", null);
            return;
        }

        StartMatchmakingGame();
    }

    /// <summary>
    /// Populates game settings for broadcast to clients and attempts to start matchmaking server session.
    /// </summary>
    private void StartMatchmakingGame() {
        GameSettings settings = networkManager.GetGameSettings();
        settings.SetMapIndex(mapSelect.GetCurrentIndex());
        settings.SetModeIndex(modeSelect.GetCurrentIndex());

        menuUi.ShowConnectingModal(false);

        Debug.Log(GetGameName());
        networkManager.StartMatchmakingGame(GetGameName(), (success, matchInfo) =>
        {
            if(!success) {
                menuUi.ShowInfoPopup("Failed to create game.", null);
            } else {
                menuUi.HideInfoPopup();
                Debug.Log(menuUi);
                menuUi.ShowLobbyPanel();
            }
        });
    }

    //Returns a formatted string containing server name and game mode information.
    private string GetGameName() {
        return string.Format("|{0}| {1}", modeSelect.GetSelectedMode().GetAbbreviation(), matchNameInput.text);
    }
}
