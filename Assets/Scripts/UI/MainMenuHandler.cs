﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MainMenuHandler : MenuHandler {
    public Text hostRoomNameText;
    public LobbyHandler lobbyHandler;

    private DLNetworkManager networkManager;
    private Stack<MenuStackComponent> menuStack;

    new void Start() {
        base.Start();
        menuStack = new Stack<MenuStackComponent>();
        networkManager = (DLNetworkManager)NetworkManager.singleton;
    }

    void Update() {
        if(Input.GetButtonDown("Cancel")) {
            if (!currentActiveMenu.currentMenuHasVerificationPrompt) {
                NavigateBack();
            } else {
                currentActiveMenu.verificationPromptObj.SetActive(true);
                StartCoroutine(SetFirstSelectedGameObject(currentActiveMenu.verificationPromptObj));
            }
        }
    }

    /// <summary>
    /// Navigates to a given menu gameObject and places the current one in the stack
    /// </summary>
    /// <param name="nextMenu">The menu we are navigating to</param>
    public void NavigateTo(GameObject nextMenu) {
        currentActiveMenu.menuObject.SetActive(false);
        menuStack.Push(currentActiveMenu);
        currentActiveMenu = new MenuStackComponent(nextMenu, menuStackProperty.Empty, false, null);
        currentActiveMenu.menuObject.gameObject.SetActive(true);
        StartCoroutine(SetFirstSelectedGameObject(null));
    }

    /// <summary>
    /// Pops all menus from back stack until it hits a stopPop menu and navigates to that.
    /// </summary>
    public void NavigateBack() {
        if (menuStack.Count != 0) {
            while (menuStack.Count != 0 && menuStack.Peek().property == menuStackProperty.Empty) {
                menuStack.Pop();
            }

            var poppedObject = menuStack.Pop();
            HandleProperty(poppedObject.property);

            currentActiveMenu.menuObject.gameObject.SetActive(false);
            currentActiveMenu = poppedObject;
            currentActiveMenu.menuObject.gameObject.SetActive(true);
            StartCoroutine(SetFirstSelectedGameObject(null));
        }
    }

    /// <summary>
    /// Adds a property to the previous menu that is in the stack.
    /// This is mostly used as a workaround to the fact that the Unity Inspector's OnClick interface only supports none/single parameter functions
    /// </summary>
    /// <param name="enumId">The id of the property we are adding</param>
    public void AddPropertyToStackTop(int enumId) {
        menuStack.Peek().property = (menuStackProperty)enumId;
    }

    /// <summary>
    /// Uses the Unity match maker to create a new online match
    /// </summary>
    public void CreateOnlineMatch() {
        if (networkManager.matchInfo == null && networkManager.matchMaker != null) {
            if (hostRoomNameText.text != "") {
                networkManager.matchName = hostRoomNameText.text;
            }
            networkManager.matchMaker.CreateMatch(networkManager.matchName, (uint)networkManager.maxPlayers, true, "", "", "", 0, 0, networkManager.OnMatchCreate);
        }
    }

    /// <summary>
    /// Starts the Unity match maker
    /// </summary>
    public void StartMatchMaker() {
        networkManager.StartMatchMaker();
    }

    /// <summary>
    /// Handles the property of a popped MenuStackObject.
    /// This is currently being used to stop the match maker and server hosting once a player leaves the lobby.
    /// </summary>
    /// <param name="prop">The property that we are going to handle</param>
    private void HandleProperty(menuStackProperty prop) {
        if (prop == menuStackProperty.StopPop) {
            networkManager.StopMatchMaker();
            networkManager.StopHost();
        }
    }
}
