﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum menuStackProperty {
    Empty,
    StopPop
};

public class MenuStackComponent {
    public GameObject menuObject;
    public menuStackProperty property;
    public bool currentMenuHasVerificationPrompt;
    public GameObject verificationPromptObj;

    public MenuStackComponent(GameObject obj, menuStackProperty prop, bool hasVerPrompt, GameObject verPromptObj) {
        menuObject = obj;
        property = prop;
        currentMenuHasVerificationPrompt = hasVerPrompt;
        verificationPromptObj = verPromptObj;
    }
}

public abstract class MenuHandler : MonoBehaviour {
    public MenuStackComponent currentActiveMenu;
    public GameObject menuRoot;

    protected void Start() {
        currentActiveMenu = new MenuStackComponent(menuRoot == null ? null : menuRoot, menuStackProperty.StopPop, false, null);
    }

    /// <summary>
    /// Takes a verification prompt as parameter and connects it to the current menu.
    /// </summary>
    /// <param name="verifPrompt">The gameObject of the verification prompt</param>
    public void SetCurrentMenuVerificationPrompt(GameObject verifPrompt) {
        currentActiveMenu.currentMenuHasVerificationPrompt = true;
        currentActiveMenu.verificationPromptObj = verifPrompt;
    }

    /// <summary>
    /// Allows OnClick interfaces to use SetFirstSelectedGameObject().
    /// Useful when a menu has submenus or verification prompts and you need to return control to the user after using these
    /// </summary>
    public void OnClickSetFirstSelected() {
        StartCoroutine(SetFirstSelectedGameObject(null));
    }

    /// <summary>
    /// Sets a button as selected the next frame after it has been called.
    /// If null is passed it sets the first selected button it finds.
    /// If a specific gameObject is passed it will look for buttons on that one instead
    /// </summary>
    /// <param name="specific">A gameObject containing buttons</param>
    public IEnumerator SetFirstSelectedGameObject(GameObject specific) {
        Button button;
        if (specific == null) {
            button = currentActiveMenu.menuObject.GetComponentInChildren<Button>();
        } else {
            button = specific.GetComponentInChildren<Button>();
        }

        if (button != null) {
            // Somewhat of a necessary hack for setting a button as selected
            EventSystem.current.SetSelectedGameObject(null);
            yield return new WaitForEndOfFrame();
            EventSystem.current.SetSelectedGameObject(button.gameObject);
        }
    }
}
