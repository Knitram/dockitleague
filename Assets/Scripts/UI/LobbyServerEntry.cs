﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;
using System;

/// <summary>
/// Represents a server in the server list
/// </summary>
public class LobbyServerEntry : MonoBehaviour {
    //UI elements
    [SerializeField]
    protected Text serverInfoText;
    [SerializeField]
    protected Text modeText;
    [SerializeField]
    protected Text slotInfo;
    [SerializeField]
    protected Button joinButton;

    //The network manager
    protected NetworkManager networkManager;

    //Sets up the UI
    public void Populate(MatchInfoSnapshot match, Color c) {
        string name = match.name;

        string[] split = name.Split(new char[1] { '|' }, StringSplitOptions.RemoveEmptyEntries);

        serverInfoText.text = split[1].Replace(" ", string.Empty);
        modeText.text = split[0];

        slotInfo.text = string.Format("{0}/{1}", match.currentSize, match.maxSize);

        NetworkID networkId = match.networkId;

        joinButton.onClick.RemoveAllListeners();
        joinButton.onClick.AddListener(() => JoinMatch(networkId));

        joinButton.interactable = match.currentSize < match.maxSize;

        GetComponent<Image>().color = c;
    }

    //Load the network manager on enable
    protected virtual void OnEnable() {
        if(networkManager == null) {
            networkManager = NetworkManager.Instance;
        }
    }

    //Fired when player clicks join
    private void JoinMatch(NetworkID networkId) {
        MainMenuUI menuUi = MainMenuUI.Instance;

        menuUi.ShowConnectingModal(true);

        networkManager.JoinMatchmakingGame(networkId, (success, matchInfo) =>
        {
            if(!success) {
                menuUi.ShowInfoPopup("Failed to join game.", null);
            }else {
                menuUi.HideInfoPopup();
                menuUi.ShowInfoPopup("Entering lobby...");
                networkManager.gameModeUpdated += menuUi.ShowLobbyPanelForConnection;
            }
        });
    }
}
