﻿using UnityEngine;
using UnityEngine.UI;

//Player entry in the lobby. Handle selecting color/setting name & getting ready for the game
//Any LobbyHook can then grab it and pass those value to the game player prefab (see the Pong Example in the Samples Scenes)
public class LobbyPlayer : MonoBehaviour {
    [SerializeField]
    protected InputField nameInput;
    [SerializeField]
    protected Button readyButton;
    [SerializeField]
    protected Transform waitingLabel;
    [SerializeField]
    protected Transform readyLabel;

    [SerializeField]
    protected Button teamButton;
    [SerializeField]
    protected Text teamButtonText;

    private bool teamLobby;

    /// <summary>
    /// Associated NetworkPlayer object
    /// </summary>
    private NetworkPlayer networkPlayer;

    private NetworkManager networkManager;

    public void Init(NetworkPlayer netPlayer) {
        Debug.LogFormat("Initializing lobby player - Ready {0}", netPlayer.IsReady);
        networkPlayer = netPlayer;
        if(netPlayer != null) {
            netPlayer.syncVarsChanged += OnNetworkPlayerSyncvarChanged;
        }

        networkManager = NetworkManager.Instance;
        if(networkManager != null) {
            networkManager.playerJoined += PlayerJoined;
            networkManager.playerLeft += PlayerLeft;
        }

        teamLobby = (netPlayer.PlayerTeamId != TeamId.Unassigned);

        readyLabel.gameObject.SetActive(false);
        waitingLabel.gameObject.SetActive(false);
        readyButton.gameObject.SetActive(true);
        readyButton.interactable = networkManager.hasSufficientPlayers;

        if(netPlayer.hasAuthority) {
            SetupLocalPlayer();
        } else {
            SetupRemotePlayer();
        }

        UpdateValues();
    }

    public void RefreshJoinButton() {
        if(networkPlayer.IsReady) {
            // Toggle ready label
            waitingLabel.gameObject.SetActive(false);
            readyButton.gameObject.SetActive(false);
            readyLabel.gameObject.SetActive(true);

            // Make everything else non-interactive
            nameInput.interactable = false;
            nameInput.image.enabled = false;
            if(teamLobby) {
                teamButton.interactable = false;
                teamButton.gameObject.SetActive(false);
            }
        } else {
            // Toggle ready button
            if(networkPlayer.hasAuthority) {
                readyButton.gameObject.SetActive(true);
                readyButton.interactable = networkManager.hasSufficientPlayers;
            } else {
                waitingLabel.gameObject.SetActive(true);
            }
            readyLabel.gameObject.SetActive(false);
            
            nameInput.interactable = networkPlayer.hasAuthority;
            nameInput.image.enabled = networkPlayer.hasAuthority;

            if(teamLobby) {
                teamButton.interactable = networkPlayer.hasAuthority;
                teamButton.gameObject.SetActive(networkPlayer.hasAuthority);
            }
        }
    }

    protected virtual void PlayerJoined(NetworkPlayer player) {
        RefreshJoinButton();
    }

    protected virtual void PlayerLeft(NetworkPlayer player) {
        RefreshJoinButton();
    }

    protected virtual void OnDestroy() {
        if(networkPlayer != null) {
            networkPlayer.syncVarsChanged -= OnNetworkPlayerSyncvarChanged;
        }

        if(networkManager != null) {
            networkManager.playerJoined -= PlayerJoined;
            networkManager.playerLeft -= PlayerLeft;
        }
    }

    private void ChangeReadyButtonColor(Color c) {
        readyButton.image.color = c;
    }

    private void UpdateValues() {
        nameInput.text = networkPlayer.PlayerName;

        if(teamLobby) {
            teamButtonText.text = networkPlayer.PlayerTeamId == TeamId.Red ? ">" : "<";
        }

        MainMenuUI mainMenu = MainMenuUI.Instance;
        mainMenu.PlayerList.AddPlayer(this, networkPlayer.PlayerTeamId);

        RefreshJoinButton();
    }

    private void SetupRemotePlayer() {
        DeactivateInteractables();

        readyButton.gameObject.SetActive(false);
        waitingLabel.gameObject.SetActive(true);
    }

    private void SetupLocalPlayer() {
        nameInput.interactable = true;
        nameInput.image.enabled = true;

        readyButton.gameObject.SetActive(true);

        //we switch from simple name display to name input
        nameInput.onEndEdit.RemoveAllListeners();
        nameInput.onEndEdit.AddListener(OnNameChanged);

        readyButton.onClick.RemoveAllListeners();
        readyButton.onClick.AddListener(OnReadyClicked);

        if(teamLobby) {
            teamButton.gameObject.SetActive(true);
            teamButton.interactable = true;

            teamButton.onClick.RemoveAllListeners();
            teamButton.onClick.AddListener(OnTeamClicked);
        }
    }

    private void OnNetworkPlayerSyncvarChanged(NetworkPlayer player) {
        UpdateValues();
    }

    //===== UI Handler

    //Note that those handler use Command function, as we need to change the value on the server not locally
    //so that all client get the new value throught syncvar
    public void OnTeamClicked() {
        networkPlayer.CmdTeamChange();
    }

    public void OnReadyClicked() {
        networkPlayer.CmdSetReady();
        DeactivateInteractables();
    }

    public void OnNameChanged(string str) {
        networkPlayer.CmdNameChanged(str);
    }

    private void DeactivateInteractables() {
        nameInput.interactable = false;
        nameInput.image.enabled = false;

        if(teamLobby) {
            teamButton.interactable = false;
            teamButton.gameObject.SetActive(false);
        }
    }
}
