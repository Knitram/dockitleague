﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Linq;

enum ShopTextBoxTypes {
    Icons = 1,
    Name,
    Stats,
    Description
}

/// <summary>
/// Handles ingame menus like the Shop and "Pause" menu
/// </summary>
public class IngameMenuHandler : MenuHandler {
    public GameObject pauseMenu;
    public GameObject shopMenu;
    public GameObject shopDescriptionsContainer;
    public GameObject shopItemPrefab;
    public GameObject purchaseVerificationPrompt;

    private List<ShopItemInstance> shopItems = new List<ShopItemInstance>();
    private GameObject shopItemParent;
    private int currentlyHighlightedId = 0;
    private ShopItemInstance currentlySelectedShopItem;
    private NetworkPlayer localPlayerInstance;
    private PlayerCurrency localPlayerCurrency;
    private Docking localPlayerDocking;

    private bool shopAvailable = true;

    new void Start() {
        base.Start();
        shopItemParent = shopMenu.GetComponentInChildren<GridLayoutGroup>().gameObject;

        // Loading scriptable objects and orders them based on price
        List<ShopItemData> shopItemDataList = new List<ShopItemData>();
        foreach(var shopItemData in Resources.LoadAll("ShopItems")) {
            shopItemDataList.Add(shopItemData as ShopItemData);
        }
        // Based on http://stackoverflow.com/questions/7873295/how-to-sort-a-list-by-field
        shopItemDataList.Sort((a, b) => a.price.CompareTo(b.price));

        // Instantiating shop items
        foreach(var shopItemData in shopItemDataList) {
            GameObject shopItemInstanceObj = Instantiate(shopItemPrefab);
            shopItemInstanceObj.transform.SetParent(shopItemParent.transform, false);

            ShopItemInstance shopItemInstance = shopItemInstanceObj.GetComponent<ShopItemInstance>();
            shopItemInstance.Initialize(shopItemData as ShopItemData, this);

            shopItems.Add(shopItemInstance);
        }
    }

    void Update() {
        if(shopMenu.activeSelf && (Input.GetButtonDown("Ability0") || Input.GetButtonDown("Ability1"))) {
            // Moving the highlighted id either left or right depending on what button press is made
            currentlyHighlightedId = (currentlyHighlightedId + (Input.GetButtonDown("Ability0") ? -1 : 1)).LimitToRange(0, currentlySelectedShopItem.itemData.dockingKitDescriptions.Count - 1);

            if(currentlyHighlightedId == 0) {
                SetHighlightedDockingKitDescription();
            } else {
                SetHighlightedAbilityDescription();
            }
        }
    }

    public void ToggleShop() {
        if(!shopAvailable) {
            return;
        }

        OnShopDisplay();
        shopMenu.SetActive(!shopMenu.activeSelf);

        if (shopMenu.activeSelf) {
            SetFirstSelectedShopObject();
            purchaseVerificationPrompt.SetActive(false);
        }
    }

    public void OnGameStateChange(bool canShopBeActivated) {
        shopAvailable = canShopBeActivated;

        if(!shopAvailable) {
            shopMenu.SetActive(false);
        }
    }

    /// <summary>
    /// Gets called whenever the player activates the Shop UI.
    /// Caches references to the local player if not already cached.
    /// </summary>
    public void OnShopDisplay() {
        if(localPlayerInstance == null) {
            localPlayerInstance = NetworkPlayer.LocalPlayerInstance;
            localPlayerCurrency = localPlayerInstance.PlayerInstance.GetComponent<PlayerCurrency>();
            localPlayerDocking = localPlayerInstance.PlayerInstance.GetComponent<Docking>();
        }
        CheckPriceAndEquipAvailability();
    }

    /// <summary>
    /// Handles the updating of the shop UI as different docking kits are selected
    /// </summary>
    public void OnShopSelectionChange() {
        if(!shopMenu.activeSelf) {
            Debug.LogError("OnShopSelectionChange() was called while the shop was inactive");
            return;
        }
        var newShopItem = EventSystem.current.currentSelectedGameObject.GetComponent<ShopItemInstance>();
        if(currentlySelectedShopItem == newShopItem) {
            return;
        }

        currentlySelectedShopItem = newShopItem;
        if(currentlySelectedShopItem != null) {
            currentlyHighlightedId = 0;

            // Updating the ability icons
            for(int i = 0; i < currentlySelectedShopItem.itemData.dockingKitDescriptions.Count; i++) {
                var icon = shopDescriptionsContainer.transform.GetChild((int)ShopTextBoxTypes.Icons).GetChild(i).GetComponent<Image>();
                icon.sprite = currentlySelectedShopItem.itemData.dockingKitDescriptions[i].icon;
            }

            SetHighlightedDockingKitDescription();
        }
    }

    /// <summary>
    /// Displays the verification prompt for shop purchases
    /// </summary>
    public void DisplayVerificationPrompt() {
        // We need to make sure that the player has enough currency to buy the kit and that the player is not buying the currently equipped kit
        if(localPlayerCurrency.currency >= currentlySelectedShopItem.itemData.price && localPlayerDocking.dockingKitId != currentlySelectedShopItem.itemData.dockingKitId) {
            purchaseVerificationPrompt.SetActive(true);
            StartCoroutine(SetFirstSelectedGameObject(purchaseVerificationPrompt));
        }
    }

    /// <summary>
    /// Completes a shop purchase and tells docking to switch kit
    /// </summary>
    public void CompleteShopPurchase() {
        localPlayerDocking.CmdSetSwitchState(localPlayerDocking.dockingKitId != DockingKitId.Empty ? true : false);
        localPlayerDocking.CmdSetDockingKitId(currentlySelectedShopItem.itemData.dockingKitId);
        localPlayerCurrency.CmdAddCurrency(-currentlySelectedShopItem.itemData.price);

        SetLastSelectedShopObject();
    }

    /// <summary>
    /// Makes sure to set the selection of the first element in the shop as the menu is opened
    /// </summary>
    public void SetFirstSelectedShopObject() {
        StartCoroutine(SetFirstSelectedGameObject(shopItems[0].gameObject));
    }

    /// <summary>
    /// Can be used when going back from menus like the verification prompt to set the last highlighted shop item as selected again
    /// </summary>
    public void SetLastSelectedShopObject() {
        StartCoroutine(SetFirstSelectedGameObject(currentlySelectedShopItem.gameObject));
    }

    /// <summary>
    /// Simple function that calls the NetworkManager to disconnect from the game. 
    /// Can be called from UI buttons using their OnClick interface in the editor
    /// </summary>
    public void StopHost() {
        NetworkManager.singleton.StopHost();
    }

    /// <summary>
    /// Prepares a string containing the stats of the docking kit before updating the Shop UI with the docking kit's information
    /// </summary>
    private void SetHighlightedDockingKitDescription() {
        HighlightElement(currentlyHighlightedId);

        var dockingKit = currentlySelectedShopItem.itemData.dockingKitPrefab.GetComponent<DockingKit>();

        string healthText = "Health: " + dockingKit.maxHealth;
        string movementSpeedText = "Vehicle Speed: " + dockingKit.moveSpeed;
        string rotationSpeedText = "Rotation Speed: " + dockingKit.rotationSpeed;

        string statsText = healthText + "\n" + movementSpeedText + "\n" + rotationSpeedText;

        SetSelectedStrings(currentlySelectedShopItem.itemData.dockingKitDescriptions[currentlyHighlightedId].name,
                           statsText,
                           currentlySelectedShopItem.itemData.dockingKitDescriptions[currentlyHighlightedId].description);
    }

    /// <summary>
    /// Prepares a cooldown string from the ability in the kit and updates the Shop UI with the currently highlighted ability's information.
    /// </summary>
    private void SetHighlightedAbilityDescription() {
        HighlightElement(currentlyHighlightedId);

        // We need to offset the id back so it fits with the 4 abilities in the kit rather than the 5 descriptions we have
        var abilityCooldown = currentlySelectedShopItem.itemData.dockingKitPrefab.GetComponentsInChildren<Ability>()[currentlyHighlightedId - 1].cooldownDuration.ToString();
        string cooldownText = "Cooldown: " + abilityCooldown + "s";

        SetSelectedStrings(currentlySelectedShopItem.itemData.dockingKitDescriptions[currentlyHighlightedId].name,
                           cooldownText,
                           currentlySelectedShopItem.itemData.dockingKitDescriptions[currentlyHighlightedId].description);
    }

    /// <summary>
    /// Updates the text boxes in the Shop UI with the given parameters
    /// </summary>
    /// <param name="name">The name of the currently highlighted ability/docking kit</param>
    /// <param name="stats">The additional stats of the currently highlighted ability/docking kit</param>
    /// <param name="description">The description of the currently highlighted ability/docking kit</param>
    private void SetSelectedStrings(string name, string stats, string description) {
        shopDescriptionsContainer.transform.GetChild((int)ShopTextBoxTypes.Name).GetComponent<Text>().text = name;
        shopDescriptionsContainer.transform.GetChild((int)ShopTextBoxTypes.Stats).GetComponent<Text>().text = stats;
        shopDescriptionsContainer.transform.GetChild((int)ShopTextBoxTypes.Description).GetComponent<Text>().text = description;
    }

    /// <summary>
    /// Highlights the currently selected information tab for a docking kit.
    /// </summary>
    /// <param name="id">The id of the highlighted object. Has a range of [0, 4]</param>
    private void HighlightElement(int id) {
        for(int i = 0; i < shopDescriptionsContainer.transform.GetChild((int)ShopTextBoxTypes.Icons).childCount; i++) {
            shopDescriptionsContainer.transform.GetChild((int)ShopTextBoxTypes.Icons).GetChild(i).GetChild(0).gameObject.SetActive((id == i) ? true : false);
        }
    }

    /// <summary>
    /// Checks all shop item prices and adds a dark overlay to items that the player is unable to purchase.
    /// Also displays a "e" on the currently equipped docking kit
    /// </summary>
    public void CheckPriceAndEquipAvailability() {
        foreach(var shopItem in shopItems) {
            shopItem.unavailableOverlay.gameObject.SetActive((shopItem.itemData.price > localPlayerCurrency.currency) ? true : false);
            shopItem.isEquippedText.gameObject.SetActive((localPlayerDocking.dockingKitId == shopItem.itemData.dockingKitId) ? true : false);
        }
    }

    public void OnLeaveGameClicked() {
        NetworkManager netManager = NetworkManager.Instance;
        netManager.Disconnect();
        netManager.ReturnToMenu(MenuPage.Home);
    }
}