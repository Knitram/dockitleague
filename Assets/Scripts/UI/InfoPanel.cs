﻿using UnityEngine;
using UnityEngine.UI;

public class InfoPanel : MonoBehaviour {
    [SerializeField]
    private Text infoText;
    [SerializeField]
    private Text buttonText;
    [SerializeField]
    private Button button;

    public void Display(string info, UnityEngine.Events.UnityAction buttonClbk, bool displayButton = true) {
        infoText.text = info;

        button.gameObject.SetActive(displayButton);
        button.onClick.RemoveAllListeners();
        if(buttonClbk != null) {
            button.onClick.AddListener(buttonClbk);
        }

        button.onClick.AddListener(() =>
        {
            gameObject.SetActive(false);
        });

        gameObject.SetActive(true);
    }
}
