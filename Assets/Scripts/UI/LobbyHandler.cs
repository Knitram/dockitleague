﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LobbyHandler : NetworkBehaviour {
    public RectTransform redTeamPanel;
    public RectTransform blueTeamPanel;
    public GameObject waitingScreenObj;

    private List<DLNetworkLobbyPlayer> connectedPlayers;
    private List<DLNetworkLobbyPlayer> redTeamPlayers;
    private List<DLNetworkLobbyPlayer> blueTeamPlayers;

    void Start() {
        connectedPlayers = new List<DLNetworkLobbyPlayer>();
        redTeamPlayers = new List<DLNetworkLobbyPlayer>();
        blueTeamPlayers = new List<DLNetworkLobbyPlayer>();
    }

    /// <summary>
    /// Gets the amount of connected players to the lobby
    /// </summary>
    /// <returns>The number of connected players</returns>
    public int GetPlayerCount() {
        return connectedPlayers.Count;
    }

    /// <summary>
    /// Adds a player to the connectedPlayers list and then calls DecideEntryTeam(player)
    /// </summary>
    /// <param name="player">The player that we are adding</param>
    public void AddPlayer(DLNetworkLobbyPlayer player) {
        if (connectedPlayers.Contains(player)) {
            return;
        }

        connectedPlayers.Add(player);
        DecideEntryTeam(player);
    }

    /// <summary>
    /// Adds the player to the correct team list and puts sets the parent of the player's visuals to the correct team panel
    /// </summary>
    /// <param name="player">The player that we are adding</param>
    public void SetPlayerTeam(DLNetworkLobbyPlayer player) {
        RectTransform chosenTeam = (player.playerColor == Color.red) ? redTeamPanel : blueTeamPanel;
        
        if(chosenTeam == redTeamPanel) {
            if(blueTeamPlayers.Contains(player)) {
                blueTeamPlayers.Remove(player);
            }
            redTeamPlayers.Add(player);
        } else {
            if (redTeamPlayers.Contains(player)) {
                redTeamPlayers.Remove(player);
            }
            blueTeamPlayers.Add(player);
        }

        // Whenever someone changes teams all players are set to not ready
        ResetReadyStatus();

        // Unity is not happy with changing the lobbyPlayer's parent so we just put all the data on a child GameObject and fetch that one instead
        player.GetVisuals().transform.SetParent(chosenTeam, false);
    }
    
    /// <summary>
    /// Displays the lobby on the client and hides the "please wait while connecting" text
    /// </summary>
    [ClientCallback]
    public void DisplayLobby() { 
        redTeamPanel.parent.parent.gameObject.SetActive(true);
        waitingScreenObj.SetActive(false);
    }

    /// <summary>
    /// Does the opposite of DisplayLobby()
    /// </summary>
    [ClientCallback]
    public void ResetLocalLobby() {
        redTeamPanel.parent.parent.gameObject.SetActive(false);
        waitingScreenObj.SetActive(true);
    }

    /// <summary>
    /// Puts the player in the team with the fewest amount of players.
    /// If the player amount is equal, a random team is chosen. 
    /// </summary>
    /// <param name="player">The player that just connected</param>
    [ServerCallback]
    private void DecideEntryTeam(DLNetworkLobbyPlayer player) {
        RectTransform chosenTeam = null;

        if (redTeamPlayers.Count == blueTeamPlayers.Count) {
            chosenTeam = (Random.value < 0.5f) ? redTeamPanel : blueTeamPanel;
        } else if (redTeamPlayers.Count > blueTeamPlayers.Count) {
            chosenTeam = blueTeamPanel;
        } else {
            chosenTeam = redTeamPanel;
        }

        player.OnColorChange((chosenTeam == redTeamPanel) ? Color.red : Color.blue);
    }

   /// <summary>
   /// Sets the ready status of all players to not ready
   /// </summary>
    [ServerCallback]
    private void ResetReadyStatus() {
        foreach (DLNetworkLobbyPlayer lobbyPlayer in connectedPlayers) {
            lobbyPlayer.CmdUpdateReadyState(false);
        }
    }

    /// <summary>
    /// Removes a disconnecting player from the correct team and destroys the visuals of that player.
    /// </summary>
    /// <param name="player">The player that just disconnected</param>
    public void RemovePlayer(DLNetworkLobbyPlayer player) {
        connectedPlayers.Remove(player);
        Destroy(player.GetVisuals().gameObject);

        if (redTeamPlayers.Contains(player)) {
            redTeamPlayers.Remove(player);
        }
        else if (blueTeamPlayers.Contains(player)) {
            blueTeamPlayers.Remove(player);
        }
    }

    /// <summary>
    /// Returns a list of connected players
    /// </summary>
    /// <returns>A list of connected players</returns>
    public List<DLNetworkLobbyPlayer> GetConnectedPlayers() {
        return connectedPlayers;
    }
}
