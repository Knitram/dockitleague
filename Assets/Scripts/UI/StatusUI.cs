﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum StatusType {
    Buff = 0,
    Debuff
};

/// <summary>
/// Class for UI status modifiers.
/// </summary>
public class StatusUI : MonoBehaviour {
    public Color buffColor;
    public Color debuffColor;

    public Image frame;
    public Image darkMask;
    public Text durationText;
    public Image icon;

    private float timer;
    private float maxDuration;
    private PlayerUIHandler playerUIHandler;

    /// <summary>
    /// Initializes the UI element.
    /// </summary>
    /// <param name="playerUI">Reference to the PlayerUIHandler.</param>
    /// <param name="statusIcon">The sprite that will be displayed in the UI element</param>
    /// <param name="statusType">Status type, buff or debuff.</param>
    /// <param name="startDuration">The start duration of the status effect.</param>
    public void Initialize(PlayerUIHandler playerUI, Sprite statusIcon, StatusType statusType, float startDuration) {
        playerUIHandler = playerUI;
        maxDuration = startDuration;
        timer = startDuration;
        durationText.text = startDuration.ToString("0");
        icon.sprite = statusIcon;

        frame.color = statusType == StatusType.Buff ? buffColor : debuffColor;
    }

    /// <summary>
    /// Updates text and dark mask fill amount when timer is active.
    /// </summary>
    void Update() {
        timer -= Time.deltaTime;

        if(timer >= 0) {
            durationText.text = timer.ToString("0");
            darkMask.fillAmount = (timer / maxDuration);
        }
    }

    /// <summary>
    /// Sets the duration text of the UI element to the parameter.
    /// </summary>
    /// <param name="newDuration">The new duration we want to update with.</param>
    public void SetNewDuration(float newDuration) {
        maxDuration = newDuration;
        durationText.text = newDuration.ToString("0");
        timer = newDuration;
    }

    /// <summary>
    /// Remove and destroy this UI element.
    /// </summary>
    public void Remove() {
        playerUIHandler.RemoveStatusModifier(this);
        Destroy(gameObject);
    }
}
