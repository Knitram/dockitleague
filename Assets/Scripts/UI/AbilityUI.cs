﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles the update of the abilitys UI.
/// </summary>
[System.Serializable]
public class AbilityUI {
    public Image abilityIcon;
    public Image darkMask;
    private PlayerUIHandler playerUIHandler;

    private float cooldownDuration;
    private float cooldownTimeLeft;

    private Coroutine updateLoop;

    /// <summary>
    /// Initialize the ability UI.
    /// </summary>
    /// <param name="uiHandler">Reference to associated PlayerUIHandler.</param>
    public void Initialize(PlayerUIHandler uiHandler) {
        playerUIHandler = uiHandler;
        darkMask.enabled = true;
    }

    /// <summary>
    /// Update loop. Handles timer and dark mask update.
    /// </summary>
    private IEnumerator Update() {
        darkMask.fillAmount = 1f;
        darkMask.enabled = true;

        while(cooldownTimeLeft > 0f) {
            cooldownTimeLeft -= Time.deltaTime;
            darkMask.fillAmount = (cooldownTimeLeft / cooldownDuration);

            yield return null;
        }

        cooldownTimeLeft = 0f;
        darkMask.enabled = false;
    }

    /// <summary>
    /// Called on ability activation. Activates cooldown.
    /// </summary>
    public void Activate() {
        cooldownTimeLeft = cooldownDuration;

        if(updateLoop != null) {
            playerUIHandler.StopCoroutine(updateLoop);
        }
        updateLoop = playerUIHandler.StartCoroutine(Update());
    }

    /// <summary>
    /// Updates the current cooldown time with the new time.
    /// </summary>
    /// <param name="newTimeLeft">The new current cooldown time.</param>
    public void UpdateCooldown(float newTimeLeft) {
        cooldownTimeLeft = newTimeLeft;
    }

    /// <summary>
    /// Changes sprites and cooldown to the new ability.
    /// </summary>
    /// <param name="newAbility">Reference to the new ability.</param>
    public void SetAbility(Ability newAbility) {
        abilityIcon.sprite = newAbility.icon;
        darkMask.sprite = newAbility.icon;
        darkMask.enabled = false;

        cooldownDuration = newAbility.cooldownDuration;
    }

    /// <summary>
    /// Stops the update loop and resets the UI to its original empty state.
    /// </summary>
    /// <param name="emptySlot">Sprite used in an empty slot.</param>
    public void ClearAbility(Sprite emptySlot) {
        if(updateLoop != null) {
            playerUIHandler.StopCoroutine(updateLoop);
        }

        abilityIcon.sprite = emptySlot;
        darkMask.sprite = emptySlot;
        darkMask.enabled = true;
        darkMask.fillAmount = 1f;
    }
}
