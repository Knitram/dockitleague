﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum Fade {
    None,
    In,
    Out
}

[RequireComponent(typeof(CanvasGroup))]
public class FadingGroup : MonoBehaviour {
    private CanvasGroup canvasGroup;

    private Fade fadeState = Fade.None;

    private float fadeTime = 0f;
    private float fadeOutValue = 0f;

    private System.Action finishFadeCallback;

    public Fade GetCurrentFade() {
        return fadeState;
    }

    //Field to return alpha ratio to fade this tick.
    private float fadeStep {
        get {
            if(fadeTime == 0f) {
                return 1f;
            }

            return Time.unscaledDeltaTime / fadeTime;
        }
    }

    private void Awake() {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Update() {
        if(fadeState == Fade.Out) {
            FadeOut();
        } else if(fadeState == Fade.In) {
            FadeIn();
        }
    }

    private void FadeOut() {
        canvasGroup.alpha -= fadeStep;
        if(canvasGroup.alpha <= fadeOutValue + Mathf.Epsilon) {
            canvasGroup.alpha = fadeOutValue;
            if(fadeOutValue == 0) {
                gameObject.SetActive(false);
            }

            EndFade();
        }
    }

    private void FadeIn() {
        canvasGroup.alpha += fadeStep;
        if(canvasGroup.alpha >= 1 - Mathf.Epsilon) {
            canvasGroup.alpha = 1;
            EndFade();
        }
    }

    private void EndFade() {
        fadeState = Fade.None;
        FireEvent();
    }

    private void FireEvent() {
        if(finishFadeCallback != null) {
            finishFadeCallback.Invoke();
        }
    }

    /// <summary>
    /// Starts the fading of a panel.
    /// </summary>
    /// <param name="fade">The fade type to use.</param>
    /// <param name="fadeTime">Fade time.</param>
    /// <param name="finishFade">Delegate to fire once fade is complete.</param>
    /// <param name="reactivate">Whether to reactivate this gameobject for the purposes of the fade.</param>
    public void StartFade(Fade fade, float fTime, Action finishFade = null, bool reactivate = true) {
        fadeState = fade;
        fadeTime = fTime;
        finishFadeCallback = finishFade;
        fadeOutValue = 0f;
        if(reactivate) {
            gameObject.SetActive(true);
            canvasGroup.alpha = (fade == Fade.In ? 0f : 1f);
        }
    }

    /// <summary>
    /// Fades the panel to a given value.
    /// </summary>
    /// <param name="fadeTime">Fade time.</param>
    /// <param name="fadeOutValue">Value to fade to.</param>
    /// <param name="finishFade">Delegate to fire once fade is complete.</param>
    public void FadeOutToValue(float fTime, float fOutValue, Action finishFade = null) {
        fadeState = Fade.Out;
        fadeTime = fTime;
        fadeOutValue = fOutValue;
        finishFadeCallback = finishFade;
    }

    /// <summary>
    /// Starts a fade, and fires the provided event if the gameObject is disabled.
    /// </summary>
    /// <param name="fade">Fade type to use.</param>
    /// <param name="fadeTime">Fade time.</param>
    /// <param name="finishFade">Delegate to fire if object is disabled.</param>
    public void StartFadeOrFireEvent(Fade fade, float fadeTime, Action finishFade = null) {
        StartFade(fade, fadeTime, finishFade, false);
        if(!gameObject.activeInHierarchy) {
            FireEvent();
        }
    }

    /// <summary>
    /// Stops the fade, snapping the alpha and activating or deactivating the gameObject.
    /// </summary>
    /// <param name="setVisible">Whether the panel should be visible or invisible on stop.</param>
    public void StopFade(bool setVisible) {
        fadeState = Fade.None;
        gameObject.SetActive(setVisible);
        if(setVisible) {
            canvasGroup.alpha = 1f;
        } else {
            canvasGroup.alpha = 0f;
        }
    }
}
