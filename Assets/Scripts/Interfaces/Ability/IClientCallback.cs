﻿/// <summary>
/// Can recieve client callbacks from the Docking.
/// </summary>
public interface IClientCallback {
    /// <summary>
    /// Called from the Docking to give abilities a way to run code on every client.
    /// </summary>
    /// <param name="functionId">The id of the function to be run on every client.</param>
    void ClientCallback(int functionId);
}

/// <summary>
/// Can recieve client callbacks from the Docking with one parameter.
/// </summary>
public interface IClientCallback<T> {
    void ClientCallback(int functionId, T param);
}

/// <summary>
/// Can recieve client callbacks from the Docking with two parameters.
/// </summary>
public interface IClientCallback<T1, T2> {
    void ClientCallback(int functionId, T1 first, T2 second);
}