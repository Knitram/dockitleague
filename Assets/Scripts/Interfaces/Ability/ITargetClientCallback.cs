﻿/// <summary>
/// Can recieve target client callbacks from the Docking.
/// </summary>
public interface ITargetClientCallback {
    /// <summary>
    /// Called from the Docking to give abilities a way to run code on a target client.
    /// </summary>
    /// <param name="functionId">The id of the function to be run on the targeted client.</param>
    void TargetClientCallback(int functionId);
}

/// <summary>
/// Can recieve target client callbacks from the Docking with one parameter.
/// </summary>
public interface ITargetClientCallback<T> {
    void TargetClientCallback(int functionId, T param);
}

/// <summary>
/// Can recieve target client callbacks from the Docking with two parameters.
/// </summary>
public interface ITargetClientCallback<T1, T2> {
    void TargetClientCallback(int functionId, T1 first, T2 second);
}