﻿using UnityEngine;

/// <summary>
/// Can return reference to a spawnable prefab.
/// </summary>
public interface ISpawnableProvider {
    /// <summary>
    /// Used by the Docking to get the correct prefab to spawn from the abilities. Parameter only used if the ability has a list of prefabs.
    /// </summary>
    /// <param name="spawnableId">The Id of the spawnable object.</param>
    /// <returns>Reference to the prefab GameObject.</returns>
    GameObject GetSpawnablePrefab(int spawnableId);
}

/// <summary>
/// Can return reference to a spawnable prefab and catch the reference to the spawned object.
/// </summary>
public interface ISpawnableReferenceProvider : ISpawnableProvider {
    /// <summary>
    /// Called from the Docking to set up local references from spawned network objects.
    /// </summary>
    /// <param name="spawnedObject">Reference to spawned object.</param>
    void SetSpawnedObjectReference(GameObject spawnedObject);
}