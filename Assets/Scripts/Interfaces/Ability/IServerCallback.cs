﻿/// <summary>
/// Can recieve server callbacks from the Docking.
/// </summary>
public interface IServerCallback {
    /// <summary>
    /// Called from the Docking to give abilities a way to run server code.
    /// </summary>
    /// <param name="functionId">The id of the function to be run on the server.</param>
    void ServerCallback(int functionId);
}

/// <summary>
/// Can recieve server callbacks from the Docking with one parameter.
/// </summary>
public interface IServerCallback<T> {
    void ServerCallback(int functionId, T param);
}

/// <summary>
/// Can recieve server callbacks from the Docking with two parameters.
/// </summary>
public interface IServerCallback<T1, T2> {
    void ServerCallback(int functionId, T1 first, T2 second);
}