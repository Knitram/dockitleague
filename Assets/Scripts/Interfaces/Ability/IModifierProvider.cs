﻿/// <summary>
/// Can return reference to modifier info.
/// </summary>
public interface IModifierProvider {
    /// <summary>
    /// Used by the Docking to get the correct modifier from the abilities. Parameter only used if the ability has a list of modifiers.
    /// </summary>
    /// <param name="modifierId">The Id of the modifier info.</param>
    /// <returns>Reference to the ModifierInfo.</returns>
    ModifierInfo GetModifierInfo(int modifierId);
}