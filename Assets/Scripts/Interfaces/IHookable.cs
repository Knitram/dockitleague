﻿using UnityEngine;

/// <summary>
/// Used by spawnables that can be hooked.
/// </summary>
public interface IHookable {
    /// <summary>
    /// Hooks the spawnable.
    /// </summary>
    /// <param name="playerObject">The player that owns the hook.</param>
    /// <param name="hook">The hook transform.</param>
    void Hooked(GameObject playerObject, Transform hook);
}
