﻿using UnityEngine;

/// <summary>
/// Used by spawnables that can be redirected.
/// </summary>
public interface IRedirectable {
    /// <summary>
    /// Redirects direction of the spawnable.
    /// </summary>
    /// <param name="newDirection">The new direction.</param>
    /// <param name="newPlayerId">The player id of the new owner, -1 if current owner is kept.</param>
    /// <param name="newTeamId">The team id of the new owner, TeamId.Unassigned if current owner is kept.</param>
    void RedirectDirection(Vector3 newDirection, int newPlayerId = -1, TeamId newTeamId = TeamId.Unassigned);
}
