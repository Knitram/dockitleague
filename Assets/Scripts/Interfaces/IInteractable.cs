﻿/// <summary>
/// Used by objects that can receive interaction calls from PlayerInput.
/// </summary>
public interface IInteractable {
    /// <summary>
    /// Called when the object is interacted with.
    /// </summary>
    /// <param name="player">Reference to the Player interacting.</param>
    void Interact(Player player);
}
