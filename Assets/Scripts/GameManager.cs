﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public enum GameState {
    Inactive,
    TimedTransition,
    StartUp,
    Preplay,
    Preround,
    Playing,
    RoundEnd,
    EndGame,
    PostGame,
    CompleteGame,
    EveryoneBailed
}

public class GameManager : NetworkBehaviour {
    public List<GameObject> dockingKitPrefabs;
    public PlayerUIHandler playerUIHandler;
    public IngameMenuHandler ingameMenuHandler;

    /// <summary>
    /// Used for retrieving a DockingKit prefab from a DockingKitId.
    /// </summary>
    /// <param name="id">Index of DockingKit to return.</param>
    /// <returns>The DockingKit prefab for the given DockingKitId.</returns>
    public GameObject GetDockingKit(DockingKitId id) {
        return dockingKitPrefabs[(int)id];
    }

    static public GameManager Instance;

    //This list is ordered descending by player score.
    static public List<Player> Players = new List<Player>();

    //Current game state - starts inactive
    protected GameState gameState = GameState.Inactive;

    //Getter of current game state
    public GameState CurrentGameState {
        get { return gameState; }
    }

    //Transition state variables
    private float transitionTime = 0f;
    private GameState nextState;

    [SyncVar]
    private bool gameIsFinished = false;

    private GameModeProcessor modeProcessor;
    public GameModeProcessor ModeProcessor {
        get { return modeProcessor; }
    }

    //Number of players in game
    private int numberOfPlayers = 0;

    //if everyone bailing has been handled
    private bool allBailHandled = false;

    //if everyone has bailed
    private bool hasEveryoneBailed = false;

    public bool HasEveryoneBailed {
        get {
            return hasEveryoneBailed;
        }
    }

    //Cached network manager
    private NetworkManager networkManager;

    [SerializeField]
    private Text roundText;
    private int currentRound = 0;

    #region Initialization

    void Awake() {
        //Sets up the singleton instance
        Instance = this;

        networkManager = NetworkManager.Instance;

        //Subscribe to events on the Network Manager
        if(networkManager != null) {
            networkManager.clientDisconnected += OnDisconnect;
            networkManager.clientError += OnError;
            networkManager.serverError += OnError;
            networkManager.matchDropped += OnDrop;
        }
    }

    /// <summary>
    /// Unity message: OnDestroy
    /// </summary>
    private void OnDestroy() {
        //Unsubscribe
        if(networkManager != null) {
            networkManager.clientDisconnected -= OnDisconnect;
            networkManager.clientError -= OnError;
            networkManager.serverError -= OnError;
            networkManager.matchDropped -= OnDrop;
        }

        Players.Clear();
    }

    void Start() {
        if(isServer) {
            //Set the state to startup
            gameState = GameState.StartUp;

            //Instantiate the rules processor
            modeProcessor = Instantiate<GameModeProcessor>(networkManager.GetGameSettings().Mode.GetModeProcessor());
            modeProcessor.SetGameManager(this);
        }

        LoadingModal modal = LoadingModal.Instance;
        if(modal != null) {
            modal.FadeOut();
        }
    }

    /// <summary>
    /// Add a player from the lobby hook
    /// </summary>
    static public void AddPlayer(Player player) {
        if(Players.IndexOf(player) == -1) {
            Players.Add(player);
            //player.gameObject.SetActive(false);
            //player.MoveToSpawnLocation(SpawnManager.Instance.GetSpawnPointTransformByIndex(player.PlayerNumber));
        }
    }

    /// <summary>
    /// Removes the player.
    /// </summary>
    /// <param name="player">Player.</param>
    public void RemovePlayer(Player player) {
        Debug.Log("Removing player");

        int playerIndex = Players.IndexOf(player);

        if(playerIndex >= 0) {
            Players.RemoveAt(playerIndex);
            if(modeProcessor != null) {
                modeProcessor.PlayerDisconnected(player);
            }

            numberOfPlayers--;
        }

        if(Players.Count == 1 && !gameIsFinished && !allBailHandled) {
            HandleEveryoneBailed();
        }
    }

    #endregion

    /// <summary>
    /// Handles everyone bailed.
    /// </summary>
    public void HandleEveryoneBailed() {
        if(!NetworkManager.IsServer) {
            return;
        }

        if(NetworkManager.Instance.state != NetworkState.Inactive) {
            allBailHandled = true;
            RpcDisplayEveryoneBailed();
            hasEveryoneBailed = true;
            SetTimedTransition(GameState.EveryoneBailed, 3f);
        }
    }

    /// <summary>
    /// Rpcs the display everyone bailed.
    /// </summary>
    [ClientRpc]
    private void RpcDisplayEveryoneBailed() {
        //SetMessageText("GAME OVER", "Everyone left the game");
        Debug.Log("GAME OVER, everyone left the game.");
    }


    /// <summary>
    /// Exits the game.
    /// </summary>
    /// <param name="returnPage">Return page.</param>
    public void ExitGame(MenuPage returnPage) {
        for(int i = 0; i < Players.Count; i++) {
            Player player = Players[i];
            if(player != null) {
                Debug.Log("Destroying player!!!");
                NetworkPlayer netPlayer = player.NetworkPlayerInstance;
                if(netPlayer != null) {
                    netPlayer.PlayerInstance = null;
                }

                NetworkServer.Destroy(Players[i].gameObject);
            }
        }

        Players.Clear();
        networkManager.ReturnToMenu(returnPage);
    }



    /// <summary>
    /// Unity message: Update
    /// Runs only on server
    /// </summary>
    [ServerCallback]
    void Update() {
        HandleStateMachine();
    }

    #region STATE HANDLING

    /// <summary>
    /// Handles the state machine.
    /// </summary>
    private void HandleStateMachine() {
        switch(gameState) {
            case GameState.StartUp:
                StartUp();
                break;
            case GameState.TimedTransition:
                TimedTransition();
                break;
            case GameState.Preround:
                Preround();
                break;
            case GameState.Preplay:
                Preplay();
                break;
            case GameState.Playing:
                Playing();
                break;
            case GameState.RoundEnd:
                RoundEnd();
                break;
            case GameState.EndGame:
                EndGame();
                break;
            case GameState.CompleteGame:
                CompleteGame();
                break;
            case GameState.EveryoneBailed:
                EveryoneBailed();
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// State up state function
    /// </summary>
    protected void StartUp() {
        if(networkManager.AllPlayersReady()) {
            gameState = GameState.Preround;
            modeProcessor.StartGame();
            RpcGameStarted();

            // Reset all ready states for players again
            networkManager.ClearAllReadyStates();
        }
    }

    /// <summary>
    /// Time transition state function
    /// </summary>
    private void TimedTransition() {
        transitionTime -= Time.deltaTime;
        if(transitionTime <= 0f) {
            gameState = nextState;
        }
    }

    /// <summary>
    /// Preround state function
    /// </summary>
    private void Preround() {
        ServerResetAllPlayers();
        SpawnableFactory.Instance.CleanupSpawnableList();
        SpawnableFactory.Instance.CleanupPickupList();

        RoundStarting();
        SetTimedTransition(GameState.Preplay, 3f);
    }

    /// <summary>
    /// Preplay state function
    /// </summary>
    protected void Preplay() {
        gameState = GameState.Playing;
        RpcRoundPlaying();
    }

    /// <summary>
    /// Playing state function
    /// </summary>
    private void Playing() {
        if(modeProcessor.IsEndOfRound()) {
            gameState = GameState.RoundEnd;
        }
    }

    /// <summary>
    /// RoundEnd state function
    /// </summary>
    private void RoundEnd() {
        modeProcessor.HandleRoundEnd();
        RpcRoundEnd();

        if (modeProcessor.IsMatchOver) {
            SetTimedTransition(GameState.EndGame, 1f);
        } else {
            RpcRoundEnding(modeProcessor.GetRoundEndText());
            SetTimedTransition(GameState.Preround, 2f);
        }
        //SpawnableFactory.Instance.CleanupSpawnableList();
        //SpawnableFactory.Instance.CleanupPickupList();
    }

    /// <summary>
    /// Rpc for round end
    /// </summary>
    [ClientRpc]
    private void RpcRoundEnd() {
        ingameMenuHandler.OnGameStateChange(true);
    }

    /// <summary>
    /// EndGame state function
    /// </summary>
    private void EndGame() {
        // If there is a game winner, wait for certain amount or all player confirmed to start a game again
        gameIsFinished = true;

        RpcGameEnd(modeProcessor.GetGameOverText());
        modeProcessor.MatchEnd();

        gameState = GameState.PostGame;

        SetTimedTransition(GameState.CompleteGame, 3f);
    }

    private void CompleteGame() {
        modeProcessor.CompleteGame();
        gameState = GameState.Inactive;
    }

    /// <summary>
    /// EveryoneBailed state function
    /// </summary>
    private void EveryoneBailed() {
        networkManager.DisconnectAndReturnToMenu();

        gameState = GameState.Inactive;
    }

    /// <summary>
    /// Sets the timed transition
    /// </summary>
    /// <param name="state">Next state</param>
    /// <param name="transTime">Transition time</param>
    private void SetTimedTransition(GameState state, float transTime) {
        nextState = state;
        transitionTime = transTime;
        gameState = GameState.TimedTransition;
    }

    #endregion

    /// <summary>
    /// Starts the round
    /// </summary>
    private void RoundStarting() {
        modeProcessor.StartRound();
        RpcRoundStarting();
    }

    /// <summary>
    /// Rpc for game started
    /// </summary>
    [ClientRpc]
    private void RpcGameStarted() {

    }

    /// <summary>
    /// Rpcs for round started
    /// </summary>
    [ClientRpc]
    private void RpcRoundStarting() {
        DisablePlayerControl();

        currentRound++;
        roundText.text = currentRound.ToString();

        AnnouncerModal.Instance.Show("ROUND STARTING");
    }

    /// <summary>
    /// Rpc for Round Playing
    /// </summary>
    [ClientRpc]
    void RpcRoundPlaying() {
        Debug.Log("PLAYING");
        AnnouncerModal.Instance.Hide();
        ingameMenuHandler.OnGameStateChange(true);

        EnablePlayerControl();
    }

    /// <summary>
    /// Rpc for Round Ending
    /// </summary>
    /// <param name="winnerText">Winner text</param>
    [ClientRpc]
    private void RpcRoundEnding(string winnerText) {
        AnnouncerModal.Instance.Show(winnerText);
    }

    /// <summary>
    /// Rpc for Game End
    /// </summary>
    [ClientRpc]
    private void RpcGameEnd(string gameWinner) {
        AnnouncerModal.Instance.Show(gameWinner);
        DisablePlayerControl();
        gameIsFinished = true;

        MainMenuUI.ReturnPage = MenuPage.Lobby;
    }

    /// <summary>
    /// Handles the kill
    /// </summary>
    /// <param name="killed">Killed</param>
    public void HandleKill(Player killed) {
        Player killer = GetPlayerByNetIdNumber(killed.PlayerHealthInstance.LastDamagedByPlayerNetId);

        if(killer != null) {
            if(killer.PlayerNumber == killed.PlayerNumber) {
                modeProcessor.HandleSuicide(killer);
                //RpcAnnounceKill(m_KillLogPhrases.GetRandomSuicidePhrase(killer.playerName, killer.playerColor));
                RpcAnnounceKill(string.Format("{0} killed themselves.", killer.PlayerName));
            } else {
                modeProcessor.HandleKillerScore(killer, killed);
                //RpcAnnounceKill(m_KillLogPhrases.GetRandomKillPhrase(killer.playerName, killer.playerColor, killed.playerName, killed.playerColor));
                RpcAnnounceKill(string.Format("{0} killed {1}.", killer.PlayerName, killed.PlayerName));
            }
        }
    }

    /// <summary>
    /// Rpc wrapper for InGameNotificationManager
    /// </summary>
    /// <param name="msg">Message</param>
    [ClientRpc]
    private void RpcAnnounceKill(string msg) {
        //InGameNotificationManager.s_Instance.Notify(msg);
        Debug.LogWarning(msg);
    }

    /// <summary>
    /// Resets all the players on the server
    /// </summary>
    [Server]
    public void ServerResetAllPlayers() {
        Debug.Log("ServerResetAllPlayers");
        SpawnManager.Instance.CleanupSpawnPoints();
        for(int i = 0; i < Players.Count; i++) {
            RespawnPlayer(Players[i].PlayerNumber, Players[i].GetPlayerTeamId());
        }
    }

    #region Respawn

    /// <summary>
    /// Respawns the player
    /// </summary>
    /// <param name="playerNumber">Player number</param>
    public void RespawnPlayer(int playerNumber, TeamId playerTeamId) {
        if(!modeProcessor.IsMatchOver) {
            RpcRespawnPlayer(playerNumber, SpawnManager.Instance.GetRandomEmptySpawnPointIndex(playerTeamId));
        }
    }

    /// <summary>
    /// Rpc for respawning the player
    /// </summary>
    /// <param name="playerNumber">Player number</param>
    /// <param name="spawnPointIndex">Spawn point index</param>
    [ClientRpc]
    public void RpcRespawnPlayer(int playerNumber, int spawnPointIndex) {
        Player player = GetPlayerByNumber(playerNumber);

        if(player == null) {
            Debug.LogError("NOT FOUND");
            return;
        }

        StartCoroutine(LocalRespawn(player, SpawnManager.Instance.GetSpawnPointTransformByIndex(spawnPointIndex)));
    }

    /// <summary>
    /// Locals the respawn
    /// </summary>
    /// <param name="player">Player</param>
    /// <param name="respawnPoint">Respawn point</param>
    private IEnumerator LocalRespawn(Player player, Transform respawnPoint) {
        player.Prespawn();
        yield return new WaitForSeconds(0.5f);
        player.RespawnReposition(respawnPoint.position, respawnPoint.rotation);
        yield return new WaitForSeconds(0.5f);
        player.RespawnReactivate();
    }

    #endregion

    /// <summary>
    /// Clients the ready
    /// </summary>
    public void ClientReady() {
        numberOfPlayers++;
    }

    /// <summary>
    /// Enables the player control
    /// </summary>
    public void EnablePlayerControl() {
        for(int i = 0; i < Players.Count; i++) {
            Players[i].EnableControl();
        }
    }

    /// <summary>
    /// Disables the player control
    /// </summary>
    public void DisablePlayerControl() {
        for(int i = 0; i < Players.Count; i++) {
            Players[i].DisableControl();
        }
    }

    /// <summary>
    /// Gets the player by the player number
    /// </summary>
    /// <returns>The player with given player number</returns>
    /// <param name="playerNumber">Player number</param>
    private Player GetPlayerByNumber(int playerNumber) {
        int length = Players.Count;
        for(int i = 0; i < length; i++) {
            Player player = Players[i];
            if(player.PlayerNumber == playerNumber) {
                return player;
            }
        }

        Debug.LogWarning("Could NOT find player!");
        return null;
    }

    /// <summary>
    /// Gets the player by the network identity number
    /// </summary>
    /// <returns>The player with given number</returns>
    /// <param name="playerNumber">Player network id number</param>
    private Player GetPlayerByNetIdNumber(int playerNetIdNumber) {
        int length = Players.Count;
        for(int i = 0; i < length; i++) {
            Player player = Players[i];
            if(player.netId.Value == playerNetIdNumber) {
                return player;
            }
        }

        Debug.LogWarning("Could NOT find player!");
        return null;
    }

    #region Networking Issues Listeners

    /// <summary>
    /// Convenience function for showing error panel
    /// </summary>
    private void ShowErrorPanel() {
        //TimedModal.s_Instance.SetupTimer(2f, networkManager.DisconnectAndReturnToMenu);
        //TimedModal.s_Instance.Show();
        networkManager.DisconnectAndReturnToMenu();
    }

    /// <summary>
    /// Raised by disconnect event
    /// </summary>
    /// <param name="connection">Connection</param>
    private void OnDisconnect(NetworkConnection connection) {
        ShowErrorPanel();
    }

    /// <summary>
    /// Raised by error event
    /// </summary>
    /// <param name="connection">Connection</param>
    /// <param name="errorCode">Error code</param>
    private void OnError(NetworkConnection connection, int errorCode) {
        ShowErrorPanel();
    }

    /// <summary>
    /// Raised by drop event
    /// </summary>
    private void OnDrop() {
        ShowErrorPanel();
    }

    #endregion
}
