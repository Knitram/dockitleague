﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMover : MonoBehaviour {
    public float timeToUse;
    public bool backwardsWhenDone;
    public Vector3 startPoint;
    public Vector3 endPoint;
    private float timer;
    private float direction = 1f;

	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime / timeToUse * direction;
        transform.localPosition = Vector3.Lerp(startPoint, endPoint, timer);
        if(timer >= 1f && direction > 0f) {
            if(backwardsWhenDone) {
                direction = -1;
            } else {
                timer = 0f;
            }
        } else if(timer <= 0f) {
            direction = 1;
        }
	}
}
