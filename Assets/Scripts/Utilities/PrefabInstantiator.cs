﻿using UnityEngine;

public static class PrefabInstantiator {
    public static GameObject InstantiateComboPrefab(Transform transformReference, ElementalContainer.ComboableElements element) {
        return Object.Instantiate(Resources.Load<GameObject>(ElementalContainer.prefabFilePaths[(int)element]),
                                                            transformReference.position,
                                                            transformReference.transform.rotation);
    }

    public static GameObject InstantiatePlayerComboPrefab(Transform transformReference) {
        return Object.Instantiate(Resources.Load<GameObject>("PlayerMultiplier/Multiplier"),
                                                            transformReference.position,
                                                            transformReference.transform.rotation);
    }
}