﻿using UnityEngine;


// Based on http://catlikecoding.com/unity/tutorials/curves-and-splines/
public static class BezierCurve {
    /// <summary>
    /// Takes 4 points and a time value, returns an interpolated position on the cubic bezier curve generated by the parameters
    /// </summary>
    /// <param name="p0">The start position of the curve</param>
    /// <param name="p1">The handle of the start position</param>
    /// <param name="p2">The handle of the end position</param>
    /// <param name="p3">The end position of the curve</param>
    /// <param name="t">The time value of the interpolation in the range of [0, 1]</param>
    /// <returns>A Vector3 containing the interpolated point</returns>
    public static Vector3 CubicInterpolation(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t) {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        // This is the formula for calculating a point in a cubic bezier curve given four positions and a time value
        return Mathf.Pow(oneMinusT, 3) * p0 + 
            3f * Mathf.Pow(oneMinusT, 2) * t * p1 + 
            3f * oneMinusT * Mathf.Pow(t, 2) * p2 + 
            Mathf.Pow(t, 3) * p3;
    }
}
