﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class ElementalContainer {
    public enum ComboableElements {
        Fire,
        Ice,
        Electricity,
        Water,
        Empty
    };

    public enum WaterTypes {
        Ice,
        Empty,
        Normal,
        Electric
    };

    public const float ICE_MULTIPLIER = 1.25f;
    public const float ELECTRIC_DAMAGE_WHEN_WET_MULTIPLIER = 1.5f;
    public const float PLAYER_COMBO_MULTIPLIER = 2f;

    public static readonly IDictionary<int, string> prefabFilePaths = new Dictionary<int, string> {
        {(int)ComboableElements.Fire, "GenericCombos/BurnParticles" },
        {(int)ComboableElements.Electricity, "GenericCombos/ElectricParticles" },
        {(int)ComboableElements.Ice, "GenericCombos/IceCube" },
        {(int)ComboableElements.Water, "GenericCombos/WaterDroplets" }
    };
}