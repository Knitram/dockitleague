﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InterpolationUtilities {
    // Based on pages 45-52 in http://pages.cpsc.ucalgary.ca/~jungle/587/pdf/5-interpolation.pdf
    /// <summary>
    /// Takes a time parameter t and does a parabolic ease to accelerate towards ta and decelerate after td.
    /// This means that t accelerates from 0, achieves a constant velocity and then decelerates back to 1  
    /// </summary>
    /// <param name="t">the input time</param>
    /// <param name="ta">the time where acceleration stops</param>
    /// <param name="td">the time where deceleration starts</param>
    /// <returns>A eased t</returns>
    public static float ParabolicEase(float t, float ta, float td) {
        float constantVelocity = 2 / (1 + td - ta);
        float easedT;

        if(t < ta) {
            easedT = constantVelocity * t * t / (2 * ta);
        } else {
            easedT = constantVelocity * ta / 2;
            if(t < td) {
                easedT += (t - ta) * constantVelocity;
            } else {
                easedT += (td - ta) * constantVelocity;
                easedT += (t - t * t / 2 - td + td * t / 2) * constantVelocity / (1 - td);
            }
        }

        return easedT;
    }
}
