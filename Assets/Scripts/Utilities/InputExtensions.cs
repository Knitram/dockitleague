﻿
/// <summary>
/// Utility class where additional type methods can be specified
/// </summary>
public static class InputExtensions {
    // http://stackoverflow.com/questions/3176602/how-to-force-a-number-to-be-in-a-range-in-c
    public static int LimitToRange(
        this int value, int inclusiveMinimum, int inclusiveMaximum) {
        if (value < inclusiveMinimum) { return inclusiveMinimum; }
        if (value > inclusiveMaximum) { return inclusiveMaximum; }
        return value;
    }
}