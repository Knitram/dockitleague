﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpinner : MonoBehaviour {
    public float rotationSpeed = 10f;
    public Vector3 axis = new Vector3(0, 1, 0);
    private float angle = 0;
	
	void Update () {
        angle += Time.deltaTime * rotationSpeed;
        if (angle % 360 == 0) {
            angle = 0;
        }
        transform.localEulerAngles = axis * angle;
    }

    void OnDisable() {
        angle = 0;
    }
}
