﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FogCamera : MonoBehaviour {
    public Shader replacementShader;
    public Color fogColor;

    /// <summary>
    /// Replaces all shaders with the replacementShader for this Camera.
    /// </summary>
    void OnEnable () {
        if(replacementShader != null) {
            Shader.SetGlobalVector("_FogColor", fogColor);
            GetComponent<Camera>().SetReplacementShader(replacementShader, "RenderType");
        }
    }

    /// <summary>
    /// Disables the replacement for this Camera.
    /// </summary>
    void OnDisable () {
        Shader.DisableKeyword("_FogColor");
        GetComponent<Camera>().ResetReplacementShader();
    }
}
