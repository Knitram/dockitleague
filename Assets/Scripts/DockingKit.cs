﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DockingKitId { Empty = -1, Marksman, Test, Brawler, Bomber, Trapper, Tank, Boomerang, Sniper, Support }


/// <summary>
/// Handles the interaction between the Docking and the abilities.
/// </summary>
public class DockingKit : MonoBehaviour {
    public float moveSpeed = 60f;
    public float rotationSpeed = 6f;
    public float maxHealth = 100f;

    public List<Ability> abilities;

    private Docking docking;
    private Animator animator;

    /// <summary>
    /// Initialization that happens locally on every client.
    /// </summary>
    /// <param name="dock">Reference to the associated Docking.</param>
    public void Initialize (Docking dock) {
        animator = GetComponent<Animator>();
        docking = dock;

        for(int i = 0; i < abilities.Count; i++) {
            abilities[i].Initialize(dock, animator, i);
        }
    }

    /// <summary>
    /// Initialization that only happens for the local player (Player controlling this docking kit).
    /// </summary>
    public void OnLocalPlayerInitialization(PlayerUIHandler playerUIHandler) {
        docking.SetDockingKitStats(this);

        for(int i = 0; i < abilities.Count; i++) {
            abilities[i].InitializeLocalPlayer(playerUIHandler.abilities[i]);
        }
    }

    /// <summary>
    /// Initialization called for the local player (Player controlling this docking kit) on docking.
    /// </summary>
    /// <param name="dockingTime">The time used to dock. (Immobile duration)</param>
    public void OnLocalPlayerDocking(float dockingTime, PlayerUIHandler playerUIHandler) {
        StartCoroutine(LocalClientDockingProcess(dockingTime));
        docking.SetDockingKitStats(this);

        for(int i = 0; i < abilities.Count; i++) {
            abilities[i].InitializeLocalPlayer(playerUIHandler.abilities[i]);
        }
    }

    /// <summary>
    /// Called for every client when undocking.
    /// </summary>
    /// <param name="dockingDuration">The time used to undock. (Immobile duration)</param>
    /// <param name="spawnPickupId">The DockingKitId of the pickup to be spawned on undocking.</param>
    /// <param name="spawnPickup">Whether to spawn the pickup.</param>
    public void OnUndocking(float dockingDuration, DockingKitId spawnPickupId, bool spawnPickup = true) {
        if(docking.hasAuthority) {
            StartCoroutine(LocalClientDockingProcess(dockingDuration, spawnPickupId, spawnPickup));
        } else {
            StartCoroutine(ClientDockingProcess(dockingDuration, true));
        }
    }

    /// <summary>
    /// IEnumerator for handling the docking duration for the local player.
    /// </summary>
    /// <param name="dockingDuration">The time used.</param>
    /// <param name="spawnPickupId">The DockingKitId of the pickup to be spawned on undocking.</param>
    private IEnumerator LocalClientDockingProcess(float dockingDuration, DockingKitId spawnPickupId = DockingKitId.Empty, bool spawnPickup = true) {
        //TODO: Insert docking animation here.
        docking.SetPlayerInputRestriction(true, InputType.Movement, InputType.Rotation);
        yield return new WaitForSeconds(dockingDuration);

        docking.SetPlayerInputRestriction(false, InputType.Movement, InputType.Rotation);
        if(spawnPickupId != DockingKitId.Empty) {
            if (spawnPickup) {
                docking.CmdSpawnDockingKitPickup(spawnPickupId);
            }
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// IEnumerator for handling the docking duration for all clients except the local player.
    /// </summary>
    /// <param name="dockingDuration">The time used.</param>
    /// <param name="undocking">Delete DockingKit GameObject on completion.</param>
    private IEnumerator ClientDockingProcess(float dockingDuration, bool undocking) {
        //TODO: Insert docking animation here.
        yield return new WaitForSeconds(dockingDuration);

        if(undocking) {
            Destroy(gameObject);
        }
    }

    public void OnRemoveKit(DockingKitId spawnPickupId = DockingKitId.Empty) {
        if(docking.hasAuthority && spawnPickupId != DockingKitId.Empty) {
            docking.CmdSpawnDockingKitPickup(spawnPickupId);
        }
        Destroy(gameObject);
    }

    /// <summary>
    /// Called when the ability button is initially pressed or released.
    /// </summary>
    /// <remarks>ButtonDown may be called without ButtonUp running afterwards, handle this in Ability.CancelAbility (if the ability is locked in between). 
    /// ButtonUp may be called without ButtonDown running first (if the ability is unlocked in between).
    /// </remarks>
    /// <param name="abilityId">Index of the ability where the button state changed.</param>
    /// <param name="down">If this was the initial press.</param>
    public void OnAbilityButtonChange(int abilityId, bool down) {
        if(abilities[abilityId].AbilityLock) {
            //Debug.Log("Ability " + abilityNumber + " locked.");
            return;
        }

        if(down) {
            abilities[abilityId].ButtonDown();
        } else {
            abilities[abilityId].ButtonUp();
        }
    }

    /// <summary>
    /// Cancels all the abilities in this docking kit.
    /// </summary>
    public void CancelAbilities() {
        foreach(Ability ab in abilities) {
            ab.CancelAbility();
        }
    }

    /// <summary>
    /// Used by Abilities to lock abilities in this docking kit.
    /// </summary>
    /// <param name="state">To lock or unlock.</param>
    /// <param name="abilityNumbers">Toggles lock for these abilities.</param>
    public void SetAbilityLock(bool state, params int[] abilityNumbers) {
        for(int i = 0; i < abilityNumbers.Length; i++) {
            abilities[abilityNumbers[i]].AbilityLock = state;
        }
    }
}
