﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="NewShopItem")]
public class ShopItemData : ScriptableObject {
    public string itemName;
    public Sprite icon;
    public GameObject dockingKitPrefab;
    public int price;
    public DockingKitId dockingKitId;

    public List<DockingKitDescriptions> dockingKitDescriptions = new List<DockingKitDescriptions>(5);
}

[System.Serializable]
public struct DockingKitDescriptions {
    public Sprite icon;
    public string name;

    [TextArea]
    public string description;
}