﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerCurrency : NetworkBehaviour {
    [SyncVar(hook = "OnCurrencyChange")]
    public int currency = 0;

    public AudioClip currencyGained;
    public AudioClip currencyLost;

    private PlayerUIHandler playerUIHandler;
    private Player player;
    private AudioSource audioSource;

    public void Initialize(Player pl) {
        player = pl;

        audioSource = GetComponent<AudioSource>();
        audioSource.enabled = false;
        playerUIHandler = GameObject.FindGameObjectWithTag("UIRoot").GetComponent<PlayerUIHandler>();
        OnCurrencyChange(currency);
        audioSource.enabled = true;
    }

    void Update() {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.C) && player.NetworkPlayerInstance.isLocalPlayer) {
            CmdAddCurrency(10);
        }
#endif
    }

    /// <summary>
    /// Command for adding a new amount to the currency.
    /// This will automatically trigger the OnCurrencyChange hook
    /// </summary>
    /// <param name="amount">The amount we add/decrease from the currency total</param>
    [Command]
    public void CmdAddCurrency(int amount) {
        currency += amount;

        if(currency < 0) {
            currency = 0;
        }
    }

    /// <summary>
    /// SyncVarHook for when the player's currency changes.
    /// Updates text and tells the PlayerUIHandler to play respective animations
    /// </summary>
    /// <param name="newAmount"></param>
    private void OnCurrencyChange(int newAmount) {
        float difference = Mathf.Abs(newAmount) - Mathf.Abs(currency);
        currency = newAmount;

        if (player.NetworkPlayerInstance.isLocalPlayer) {
            playerUIHandler.currencyText.text = newAmount.ToString();
            audioSource.PlayOneShot((difference > 0) ? currencyGained : currencyLost);

            if (difference != 0) {
                playerUIHandler.PlayCurrencyChangeAnimation(difference);
            }
        }
    }
}