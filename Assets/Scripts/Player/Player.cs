﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

[System.Serializable]
public class ToggleEvent : UnityEvent<bool> { }

/// <summary>
/// Handles the initialization for the local and remote events for each Player.
/// </summary>
public class Player : NetworkBehaviour {

    public ToggleEvent onToggleShared;
    public ToggleEvent onToggleLocal;
    public ToggleEvent onToggleRemote;

    #region Fields

    public Material redPlayer;
    public Material bluePlayer;
    public Material unassignedPlayer;

    public List<SpriteRenderer> playerVisuals;

    private FieldOfView fieldOfView;

    //Synced player ID, -1 means it has not changed yet (as the lowest valid player id is 0)
    [SyncVar(hook = "OnPlayerIdChanged")]
    private int playerId = -1;

    [SyncVar]
    private int score = 0;

    #endregion

    #region Properties

    public NetworkPlayer NetworkPlayerInstance {
        get;
        protected set;
    }

    public PlayerCamera PlayerCameraInstance {
        get;
        protected set;
    }

    public Docking DockingInstance {
        get;
        protected set;
    }

    public PlayerInput PlayerInputInstance {
        get;
        protected set;
    }

    public PlayerHealth PlayerHealthInstance {
        get;
        protected set;
    }

    public PlayerStatus PlayerStatusInstance {
        get;
        protected set;
    }

    public PlayerCurrency PlayerCurrencyInstance {
        get;
        protected set;
    }

    public string PlayerName {
        get { return NetworkPlayerInstance.PlayerName; }
    }

    public int PlayerNumber {
        get { return playerId; }
    }

    public bool RemovedPlayer {
        get;
        private set;
    }

    public bool Ready {
        get { return NetworkPlayerInstance.IsReady; }
    }

    public bool Initialized {
        get;
        private set;
    }

    public int Score {
        get { return score; }
    }

    #endregion

    #region Methods

    public override void OnStartClient() {
        base.OnStartClient();

        if(!Initialized && playerId >= 0) {
            Initialize();
        }
    }

    private void Initialize() {
        Initialize(NetworkManager.Instance.GetPlayerById(playerId));
    }

    /// <summary>
    /// Set up this player with the correct properties
    /// </summary>
    private void Initialize(NetworkPlayer newPlayer) {
        if(Initialized) {
            return;
        }
        Initialized = true;

        NetworkPlayerInstance = newPlayer;

        if(NetworkPlayerInstance.hasAuthority) {
            PlayerInputInstance = GetComponent<PlayerInput>();
            PlayerInputInstance.Initialize(this);

            PlayerCameraInstance = Camera.main.transform.parent.GetComponent<PlayerCamera>();
            fieldOfView = GetComponent<FieldOfView>();

            NetworkPlayerInstance.CmdSetReady();
        }

        // Get references to the components. 
        PlayerHealthInstance = GetComponent<PlayerHealth>();
        PlayerCurrencyInstance = GetComponent<PlayerCurrency>();
        DockingInstance = GetComponent<Docking>();
        PlayerStatusInstance = GetComponent<PlayerStatus>();

        // Initialize components
        PlayerHealthInstance.Initialize(this);
        PlayerCurrencyInstance.Initialize(this);
        DockingInstance.Initialize(this);
        PlayerStatusInstance.Initialize();

        GameManager.AddPlayer(this);

        EnablePlayer();

        if(NetworkPlayerInstance.PlayerTeamId == TeamId.Red) {
            playerVisuals.ForEach(sp => sp.material = redPlayer);
        } else if(NetworkPlayerInstance.PlayerTeamId == TeamId.Blue) {
            playerVisuals.ForEach(sp => sp.material = bluePlayer);
        } else {
            playerVisuals.ForEach(sp => sp.material = unassignedPlayer);
        }
    }

    private void EnablePlayer() {
        onToggleShared.Invoke(true);

        if(NetworkPlayerInstance.hasAuthority) {
            PlayerCameraInstance.SetPlayerTransform(transform, true);
            onToggleLocal.Invoke(true);
        } else {
            onToggleRemote.Invoke(true);
        }
    }

    private void DisablePlayer() {
        onToggleShared.Invoke(false);

        if(hasAuthority) {
            //Camera.main.GetComponent<PlayerCamera>().SetPlayerTransform(null);
            onToggleLocal.Invoke(false);
        } else {
            onToggleRemote.Invoke(false);
        }
    }

    public override void OnNetworkDestroy() {
        base.OnNetworkDestroy();
        if(NetworkPlayerInstance != null) {
            NetworkPlayerInstance.PlayerInstance = null;
        }

        GameManager.Instance.RemovePlayer(this);
    }

    public void DisableControl() {
        if(hasAuthority) {
            PlayerInputInstance.SetInputActive(false);
        }
    }

    public void EnableControl() {
        if(hasAuthority) {
            PlayerInputInstance.SetInputActive(true);
        }
    }

    public void SetPlayerActive(bool state) {
        playerVisuals.ForEach(sr => sr.enabled = state);
        GetComponent<Collider>().enabled = state;

        if(hasAuthority) {
            PlayerInputInstance.SetInputActive(state);
        }
    }

    /// <summary>
    /// Prespawning, used by round based modes to ensure the player is in the correct state prior to running spawn flow
    /// </summary>
    public void Prespawn() {
        PlayerHealthInstance.SetDefaults();
        //gameObject.SetActive(false);
    }

    public void RespawnReposition(Vector3 position, Quaternion rotation) {
        if(RemovedPlayer) {
            return;
        }

        PlayerHealthInstance.SetDefaults();
        GetComponent<Rigidbody>().position = position;
        transform.position = position;

        GetComponent<Rigidbody>().rotation = rotation;
        transform.rotation = rotation;
    }

    /// <summary>
    /// Reactivates the player as part of the spawn process.
    /// </summary>
    public void RespawnReactivate() {
        if(RemovedPlayer) {
            return;
        }
        //gameObject.SetActive(true);
        if(hasAuthority) {
            PlayerInputInstance.SetDefaults();
        }
    }

    /// <summary>
    /// Convenience function for increasing the player score
    /// </summary>
    public void IncrementScore() {
        score++;
    }

    /// <summary>
    /// Convenience function for decreasing the player score
    /// </summary>
    public void DecrementScore() {
        score--;
    }

    public FieldOfView GetPlayerFOV() {
        return fieldOfView;
    }

    public GameObject GetPlayerMask() {
        GameObject mask = this.transform.Find("Mask").Find("ViewMask").gameObject;
        return mask;
    }

    public TeamId GetPlayerTeamId() {
        return NetworkPlayerInstance.PlayerTeamId;
    }

    public void MarkPlayerAsRemoved() {
        RemovedPlayer = true;
    }

    #region SyncVar Hooks

    private void OnPlayerIdChanged(int newPlayerId) {
        playerId = newPlayerId;
        Initialize();
    }

    #endregion

    #region Networking
    
    [Server]
    public void SetPlayerId(int id) {
        playerId = id;
    }

    /// <summary>
    /// TargetRpc for adding force to the player rigidbody. Needed because the local player has authority, and needs to be the one adding force.
    /// </summary>
    /// <param name="connection">Needed so TargetRpc finds the correct client.</param>
    /// <param name="strength">The force applied.</param>
    /// <param name="mode">The force mode used.</param>
    [TargetRpc]
    public void TargetAddForce(NetworkConnection connection, float strength, ForceMode mode, Vector3 towardsPosition) {
        GetComponent<Rigidbody>().AddForce((towardsPosition - transform.position).normalized * strength, mode);
    }

    /// <summary>
    /// TargetRpc for adding force to the player rigidbody where the force origin relative to the player matters
    /// </summary>
    /// <param name="connection">Needed so TargetRpc finds the correct client.</param>
    /// <param name="strength">Amount of force applied.</param>
    /// <param name="mode">The force mode used.</param>
    /// <param name="forceOrigin">Origin of the force.</param>
    [TargetRpc]
    public void TargetAddForce2(NetworkConnection connection, float strength, ForceMode mode, Vector3 forceOrigin) {
        GetComponent<Rigidbody>().AddForce((transform.position - forceOrigin).normalized * strength, mode);
    }

    /// <summary>
    /// TargetRpc for adding explosion force to the player rigidbody.
    /// </summary>
    /// <param name="connection">Needed so TargetRpc finds the correct client.</param>
    /// <param name="explosionForce">Amount of force in the explosion</param>
    /// <param name="explosionOrigin">Center of the explosion</param>
    /// <param name="explosionRadius">Radius of the explosion</param>
    [TargetRpc]
    public void TargetAddExplosionForce(NetworkConnection connection, float explosionForce, Vector3 explosionOrigin, float explosionRadius) {
        GetComponent<Rigidbody>().AddExplosionForce(explosionForce, explosionOrigin, explosionRadius);
    }

    /// <summary>
    /// Command called from PlayerInput when interacting with networked interactable objects.
    /// </summary>
    /// <param name="interactableObject">The networked gameobject interacted with.</param>
    [Command]
    public void CmdInteract(GameObject interactableObject) {
        IInteractable interactable = interactableObject.GetComponent<IInteractable>();
        if(interactable != null) {
            interactable.Interact(this);
        }
    }

    #endregion

    #endregion
}
