﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Handles the modifiers and status effects for the player. 
/// </summary>
public class PlayerStatus : NetworkBehaviour {
    //TEMP
    public ModifierInfo stun;
    public ModifierInfo root;
    public ModifierInfo silence;
    public ModifierInfo dot;

    private List<ModifierInstanceServer> serverModifiers;
    private List<ModifierInstanceClient> clientModifiers;

    private int newModifierId;

    private PlayerUIHandler playerUIHandler;

    public void Initialize() {
        if(isServer) {
            serverModifiers = new List<ModifierInstanceServer>();
        }
        clientModifiers = new List<ModifierInstanceClient>();

        playerUIHandler = GameManager.Instance.playerUIHandler;
    }

#if UNITY_EDITOR
    //TEMP For testing
    void Update() {
        if(!(isServer && hasAuthority)) {
            return;
        }
        
        //TEMP Used for testing.
        if(Input.GetKeyDown(KeyCode.T)) {
            ApplyModifier(stun);
        } else if(Input.GetKeyDown(KeyCode.Y)) {
            ApplyModifier(root);
        } else if(Input.GetKeyDown(KeyCode.U)) {
            ApplyModifier(silence);
        } else if(Input.GetKeyDown(KeyCode.I)) {
            ApplyModifier(dot);
        } else if(Input.GetKeyDown(KeyCode.P)) {
            RemoveAllModifiers();
        }
    }
#endif

    /// <summary>
    /// ServerCallback for applying a modifier. Searches through list of current modifiers if the modifier is unique.
    /// </summary>
    /// <param name="modifierInfo">The information needed to apply the modifier.</param>
    /// <param name="abilityId">The Id of the ability that applied the modifier if any, -1 otherwise.</param>
    [Server]
    public void ApplyModifier(ModifierInfo modifierInfo, int abilityId = -1) {
        if(modifierInfo.modifier.unique) {
            //Search list.
            foreach(ModifierInstanceServer mod in serverModifiers) {
                if(mod.GetModifier() == modifierInfo.modifier) {
                    mod.MaxDuration(modifierInfo.duration);
                    return;
                }
            }
        }
      
        //Create new if not unique/in list.
        serverModifiers.Add(new ModifierInstanceServer(modifierInfo, this, newModifierId, abilityId));
        RpcApplyModifier(modifierInfo.modifier.name, newModifierId, abilityId, modifierInfo.duration);
        newModifierId++;
    }

    /// <summary>
    /// Removes the modifier instance passed to it. Called by the ModifierInstance when the modifier has ended.
    /// </summary>
    /// <param name="instance">The ModifierInstance that should be removed.</param>
    /// <param name="sync">Should this be synced to the clients.</param>
    [Server]
    public void RemoveModifier(ModifierInstanceServer instance, bool sync = true) {
        serverModifiers.Remove(instance);

        if(sync) {
            RpcRemoveModifier(instance.GetModifierId());
        }
    }

    /// <summary>
    /// Removes the first instance equal to the modifier passed in. Used by abilities through the Docking.
    /// </summary>
    /// <param name="modifier">The modifier to remove.</param>
    /// <remarks>Works as long as abilities only self apply unique instances of modifiers, as this only removes based on modifier type (not unique id).</remarks>
    [Server]
    public void RemoveModifier(Modifier modifier) {
        foreach(ModifierInstanceServer mod in serverModifiers) {
            if(mod.GetModifier() == modifier) {
                mod.OnEnd();
                return;
            }
        }
    }

    /// <summary>
    /// Iterates through modifier list and stops everything.
    /// </summary>
    [Server]
    public void RemoveAllModifiers() {
        //Iterate backwards because we're removing elements while traversing.
        for(int i = serverModifiers.Count - 1; i >= 0; i--) {
            serverModifiers[i].OnEnd();
        }
    }

    /// <summary>
    /// Iterates through modifier list and stops ability (self applied) modifiers. (Modifiers with a valid abilityId).
    /// </summary>
    public void RemoveAllAbilityModifiers() {
        if(isServer) {
            //Iterate backwards because we're removing elements while traversing.
            for(int i = serverModifiers.Count - 1; i >= 0; i--) {
                if(serverModifiers[i].GetAbilityId() >= 0) {
                    serverModifiers[i].OnCancel();
                }
            }
        }

        for(int i = clientModifiers.Count - 1; i >= 0; i--) {
            if(clientModifiers[i].GetAbilityId() >= 0) {
                clientModifiers[i].OnEnd();
                clientModifiers.Remove(clientModifiers[i]);
            }
        }
    }

    /// <summary>
    /// Iterates through modifier list and removes debuffs.
    /// </summary>
    public void RemoveAllDebuffModifiers() {
        if(isServer) {
            //Iterate backwards because we're removing elements while traversing.
            for (int i = serverModifiers.Count - 1; i >= 0; i--) {
                Debug.Log("Server modifier: " + serverModifiers[i].GetModifier().statusType);
                if (serverModifiers[i].GetModifier().statusType == StatusType.Debuff) {
                    serverModifiers[i].OnEnd();
                    serverModifiers.Remove(serverModifiers[i]);
                }
            }
        }
    }

    /// <summary>
    /// ClientRpc called when a new modifier is applied. Used to synchronize modifiers.
    /// </summary>
    /// <param name="modifierName">Used to lookup correct modifier.</param>
    /// <param name="modifierId">Unique id for this modifier instance.</param>
    /// <param name="abilityId">The Id of the ability that applied the modifier if any, -1 otherwise.</param>
    /// <param name="duration">The duration of this modifier instance.</param>
    [ClientRpc]
    private void RpcApplyModifier(string modifierName, int modifierId, int abilityId, float duration) {
        Modifier newModifier = Modifier.GetModifierAsset(modifierName);
        clientModifiers.Add(new ModifierInstanceClient(newModifier, this, playerUIHandler, modifierId, abilityId, duration));
    }

    /// <summary>
    /// ClientRpc called when a modifier is removed. Used to synchronize modifiers.
    /// </summary>
    /// <param name="modifierId">Used to find correct modifier instance.</param>
    [ClientRpc]
    private void RpcRemoveModifier(int modifierId) {
        foreach(ModifierInstanceClient mod in clientModifiers) {
            if(mod.GetModifierId() == modifierId) {
                mod.OnEnd();
                clientModifiers.Remove(mod);
                return;
            }
        }
    }

    /// <summary>
    /// TargetRpc for updating UI elements duration.
    /// </summary>
    /// <param name="connection">Needed so TargetRpc finds the correct client.</param>
    /// <param name="modifierId">Used to find correct modifier instance.</param>
    /// <param name="newDuration">The new duration.</param>
    [TargetRpc]
    public void TargetSetUIDuration(NetworkConnection connection, int modifierId, float newDuration) {
        foreach(ModifierInstanceClient mod in clientModifiers) {
            if(mod.GetModifierId() == modifierId) {
                mod.SetNewDuration(newDuration);
                return;
            }
        }
    }
}
