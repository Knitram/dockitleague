﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles all Camera interactions.
/// </summary>
public class PlayerCamera : MonoBehaviour {
    public float height = 25f;
    public float defaultMoveSpeed = 20f;
    public float defaultScaleSpeed = 2.5f;

    private Camera[] cameras;

    private Transform playerTarget;
    private Transform target;
    private Transform cameraTransform;

    private float moveSpeed;
    private float scaleSpeed;

    private bool smoothFollow;
    private bool smoothReturn;

    private float defaultOrthoSize;
    private Coroutine orthoScaleCoroutine;

    void Start() {
        cameraTransform = transform;
        cameras = GetComponentsInChildren<Camera>();
        defaultOrthoSize = cameras[0].orthographicSize;
    }

    void LateUpdate() {
        if(target != null) {
            Vector3 desiredPos = new Vector3(target.position.x, height, target.position.z);
            cameraTransform.position = smoothFollow ? Vector3.MoveTowards(cameraTransform.position, desiredPos, moveSpeed * Time.deltaTime) : desiredPos;

            if(smoothReturn && (cameraTransform.position - desiredPos).sqrMagnitude < 0.01f) {
                smoothFollow = false;
                smoothReturn = false;
            }
        } else if(playerTarget != null) {
            //If temporary target was destroyed, return to the player.
            ReturnToPlayer(true);
        }
    }

    /// <summary>
    /// Lerps the orthographicSize on the cameras to the given size.
    /// </summary>
    /// <param name="targetSize">The new orthographicSize.</param>
    IEnumerator LerpOrthoSize(float targetSize) {
        float newSize = cameras[0].orthographicSize;

        while(Mathf.Abs(newSize - targetSize) > 0.1f) {
            newSize = Mathf.Lerp(newSize, targetSize, scaleSpeed * Time.deltaTime);
            foreach(Camera c in cameras) {
                c.orthographicSize = newSize;
            }

            yield return null;
        }

        newSize = targetSize;
        foreach(Camera c in cameras) {
            c.orthographicSize = newSize;
        }
    }

    /// <summary>
    /// Sets the associated player transform.
    /// </summary>
    /// <param name="newPlayerTarget">The new player transform.</param>
    /// <param name="returnToPlayer">Whether to move the camera to this transform.</param>
    /// <param name="smoothReturn">Whether the return is smooth or instant.</param>
    public void SetPlayerTransform(Transform newPlayerTarget, bool returnToPlayer = false, bool smoothReturn = false) {
        playerTarget = newPlayerTarget;

        if(returnToPlayer) {
            ReturnToPlayer(smoothReturn);
        }
    }

    /// <summary>
    /// Set temporary target to follow, which will override the player transform. This will use the default move speed.
    /// </summary>
    /// <param name="newTarget">The new transform to follow.</param>
    /// <param name="smoothing">Whether to smoothly follow target.</param>
    public void SetTarget(Transform newTarget, bool smoothing = false) {
        target = newTarget;
        smoothFollow = smoothing;
        moveSpeed = defaultMoveSpeed;
    }

    /// <summary>
    /// Set temporary target to follow using custom move speed, which will override the player transform.
    /// </summary>
    /// <param name="newTarget">The new transform to follow.</param>
    /// <param name="smoothing">Whether to smoothly follow target.</param>
    /// <param name="speed">The move speed utilized.</param>
    public void SetTarget(Transform t, bool smoothing, float speed) {
        target = t;
        smoothFollow = smoothing;
        moveSpeed = speed;
    }

    /// <summary>
    /// Set the orthographicSize for the cameras, will lerp between current and targetSize using the default speed.
    /// </summary>
    /// <param name="targetSize">The new orthographicSize.</param>
    public void SetOrthoSizeTarget(float targetSize) {
        scaleSpeed = defaultScaleSpeed;

        if(orthoScaleCoroutine != null) {
            StopCoroutine(orthoScaleCoroutine);
        }
        orthoScaleCoroutine = StartCoroutine(LerpOrthoSize(targetSize));
    }

    /// <summary>
    /// Set the orthographicSize for the cameras, will lerp between current and targetSize using the given speed.
    /// </summary>
    /// <param name="targetSize">The new orthographicSize.</param>
    /// <param name="speed">The lerp speed utilized.</param>
    public void SetOrthoSizeTarget(float targetSize, float speed) {
        scaleSpeed = speed;

        if(orthoScaleCoroutine != null) {
            StopCoroutine(orthoScaleCoroutine);
        }
        orthoScaleCoroutine = StartCoroutine(LerpOrthoSize(targetSize));
    }

    /// <summary>
    /// Call for returning to the player transform using the default speed.
    /// </summary>
    /// <param name="smooth">Whether the return is smooth or instant.</param>
    public void ReturnToPlayer(bool smooth) {
        target = playerTarget;

        moveSpeed = defaultMoveSpeed;

        smoothFollow = smooth;
        smoothReturn = smooth;

        if(smooth) {
            if(orthoScaleCoroutine != null) {
                StopCoroutine(orthoScaleCoroutine);
            }
            orthoScaleCoroutine = StartCoroutine(LerpOrthoSize(defaultOrthoSize));
        } else {
            foreach(Camera c in cameras) {
                c.orthographicSize = defaultOrthoSize;
            }
        }
    }

    /// <summary>
    /// Call for returning to the player transform using the given speed.
    /// </summary>
    /// <param name="smooth">Whether the return is smooth or instant.</param>
    /// <param name="speed">The move speed utilized.</param>
    public void ReturnToPlayer(bool smooth, float speed) {
        target = playerTarget;

        moveSpeed = speed;

        smoothFollow = smooth;
        smoothReturn = smooth;

        if(smooth) {
            if(orthoScaleCoroutine != null) {
                StopCoroutine(orthoScaleCoroutine);
            }
            orthoScaleCoroutine = StartCoroutine(LerpOrthoSize(defaultOrthoSize));
        } else {
            foreach(Camera c in cameras) {
                c.orthographicSize = defaultOrthoSize;
            }
        }
    }
}
