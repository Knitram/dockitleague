﻿using System.Collections.Generic;
using UnityEngine;

public enum InputType { Movement, Rotation, Abilities, Docking, Interact }

/// <summary>
/// Handles all player inputs.
/// </summary>
public class PlayerInput : MonoBehaviour {
    public string moveHorizontal = "Horizontal";
    public string moveVertical = "Vertical";
    public string rotateHorizontal = "HorizontalRotation";
    public string rotateVertical = "VerticalRotation";

    public string dock = "Dock";
    public string undock = "Undock";

    public string interact = "Interact";

    public string[] abilityButtons;
    private bool[] abilityButtonIsPressed;
    private int[] inputRestrictions;

    public float moveSpeed;
    public float rotationSpeed;

    public IngameMenuHandler menuHandler;

    private Vector3 directionVector;
    private Vector2 rotationVector;
    private float angle;
    private bool inputEnabled;

    private Rigidbody rbody;
    private Docking docking;
    private Player player;

    private List<GameObject> interactables;

    public void Initialize(Player pl){
        if(!pl.NetworkPlayerInstance.isLocalPlayer) {
            //enabled = false;
            return;
        }

        rbody = GetComponent<Rigidbody>();
        docking = GetComponent<Docking>();
        player = pl;

        abilityButtonIsPressed = new bool[abilityButtons.Length];
        inputRestrictions = new int[System.Enum.GetNames(typeof(InputType)).Length];

        inputEnabled = false;

        interactables = new List<GameObject>();

        menuHandler = GameObject.FindGameObjectWithTag("MenuHandler").GetComponent<IngameMenuHandler>();
    }

    public void SetDefaults() {
        directionVector = Vector3.zero;
        rotationVector = Vector2.zero;

        for(int i = 0; i < abilityButtonIsPressed.Length; i++) {
            abilityButtonIsPressed[i] = false;
        }
        for(int i = 0; i < inputRestrictions.Length; i++) {
            inputRestrictions[i] = 0;
        }
        interactables.Clear();
    }
    
    public void SetInputActive(bool state) {
        inputEnabled = state;
        directionVector = Vector3.zero;
        rotationVector = Vector2.zero;
    }

    /// <summary>
    /// Read inputs every frame.
    /// </summary>
    void Update() {
        //Pause menu input.
        if ((Input.GetButtonDown("Pause") || (menuHandler.pauseMenu.activeSelf && Input.GetButtonDown("Cancel"))) && !menuHandler.shopMenu.activeSelf) {
            menuHandler.pauseMenu.SetActive(!menuHandler.pauseMenu.activeSelf);

            if (menuHandler.pauseMenu.activeSelf) {
                StartCoroutine(menuHandler.SetFirstSelectedGameObject(menuHandler.pauseMenu));
            }
        }

        if(Input.GetButtonDown("Shop") && !menuHandler.pauseMenu.activeSelf || (menuHandler.shopMenu.activeSelf && Input.GetButtonDown("Cancel") && !menuHandler.purchaseVerificationPrompt.activeSelf)) {
            menuHandler.ToggleShop();
        } else if(Input.GetButtonDown("Cancel") && menuHandler.purchaseVerificationPrompt.activeSelf) {
            menuHandler.purchaseVerificationPrompt.SetActive(false);
            menuHandler.SetLastSelectedShopObject();
        }

        if (menuHandler.pauseMenu.activeSelf || menuHandler.shopMenu.activeSelf || !inputEnabled) {
            return;
        }

        //Ability buttons input.
        if(inputRestrictions[(int)InputType.Abilities] == 0) {
            for(int i = 0; i < abilityButtons.Length; i++) {
                InputAbilityButton(i);
            }
        }

        //Docking buttons input.
        if(inputRestrictions[(int)InputType.Docking] == 0) {
            if(Input.GetButtonDown(dock)) {
                docking.OnDockingButtonDown();
            } else if(Input.GetButtonDown(undock)) {
                docking.OnUndockingButtonDown();
            }
        }

        //Interaction button input.
        if(inputRestrictions[(int)InputType.Interact] == 0) {
            if(Input.GetButtonDown(interact) && interactables.Count > 0) {
                player.CmdInteract(interactables[0]);
                interactables.RemoveAt(0);
            }
        }

        //Movement input.
        directionVector = new Vector3(Input.GetAxisRaw(moveHorizontal), 0f, Input.GetAxisRaw(moveVertical));
        if(directionVector.sqrMagnitude > 0.1f) {
            float directionLength = directionVector.magnitude;
            directionVector = directionVector / directionLength;
            directionLength = Mathf.Min(1f, directionLength);

            //Make the input vector more sensitive towards the extremes and less sensitive in the middle
            //This makes it easier to control slow speeds when using analog sticks
            directionLength = directionLength * directionLength;

            //Multiply the normalized direction vector by the modified length
            directionVector *= directionLength;
        }
        
        //Rotation input.
        rotationVector = new Vector2(Input.GetAxisRaw(rotateHorizontal), Input.GetAxisRaw(rotateVertical));
    }

    /// <summary>
    /// Handling RigidBody in FixedUpdate because it interacts with the physics simulation.
    /// </summary>
    void FixedUpdate() {
        if(menuHandler.pauseMenu.activeSelf || menuHandler.shopMenu.activeSelf || !inputEnabled) {
            return;
        }

        if (inputRestrictions[(int)InputType.Movement] == 0 && directionVector.sqrMagnitude > 0.1f) {
            rbody.AddForce(directionVector * moveSpeed);
        }

        if(inputRestrictions[(int)InputType.Rotation] == 0 && rotationVector.sqrMagnitude > 0.1f) {
            //Flipped x & y in Atan2 because Unitys rotations are 90 degrees off.
            angle = Mathf.LerpAngle(angle, Mathf.Atan2(rotationVector.x, rotationVector.y) * Mathf.Rad2Deg, rotationSpeed * Time.deltaTime);
            rbody.rotation = Quaternion.Euler(0f, angle, 0f);
        }
    }

    /// <summary>
    /// If not pressed, checks for button presses. If already pressed, checks for button releases.
    /// </summary>
    /// <param name="index">Index of ability button.</param>
    private void InputAbilityButton(int index) {
        //GetAxis also works for button presses.
        if(!abilityButtonIsPressed[index]) {
            //GetButtonDown.
            if(Input.GetAxisRaw(abilityButtons[index]) > 0.02f) {
                abilityButtonIsPressed[index] = true;
                docking.OnAbilityButtonChange(index, true);
            }
        } else {
            //GetButtonUp.
            if(Input.GetAxisRaw(abilityButtons[index]) < 0.02f) {
                abilityButtonIsPressed[index] = false;
                docking.OnAbilityButtonChange(index, false);
            }
        }
    }

    /// <summary>
    /// directionVector is set every frame based on the movement axis from player input.
    /// </summary>
    /// <returns>The direction vector.</returns>
    public Vector3 GetDirectionVector() {
        return directionVector;
    }

    /// <summary>
    /// rotationVector is set every frame based on the rotation axis from player input.
    /// </summary>
    /// <returns>The rotation vector.</returns>
    public Vector2 GetRotationVector() {
        return rotationVector;
    }

    /// <summary>
    /// Used by the local player to self restrict input type. Using int stacks for situations where one modifier removes the restriction, but the restriction is still active by another.
    /// </summary>
    /// <param name="state">The new state of the input restriction.</param>
    /// <param name="types">The types to set restriction for.</param>
    public void SetInputRestrictions(bool state, InputType[] inputTypes) {
        foreach(InputType type in inputTypes) {
            inputRestrictions[(int)type] = state ? inputRestrictions[(int)type] + 1 : Mathf.Max(inputRestrictions[(int)type] - 1, 0);
        }
    }

    void OnTriggerEnter(Collider other) {
        //OnTrigger callbacks happen even if the script is disabled, thus we need to return if we're not the local player.
        if(!enabled) {
            return;
        }

        IInteractable interactable = other.GetComponent<IInteractable>();
        if(interactable != null) {
            interactables.Add(other.gameObject);
        }
    }

    void OnTriggerExit(Collider other) {
        if(!enabled) {
            return;
        }

        IInteractable interactable = other.GetComponent<IInteractable>();
        if(interactable != null) {
            interactables.Remove(other.gameObject);
        }
    }
}
