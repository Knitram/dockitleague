﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Handles functionality related to the player health.
/// </summary>
[RequireComponent(typeof(Player))]
public class PlayerHealth : NetworkBehaviour {
    [SyncVar(hook = "OnHealthChange")]
    private float currentHealth;
    [SyncVar(hook = "OnMaxHealthChange")]
    public float maxHealth = 100f;

    public float damageMultiplier = 1f;

    public SpriteRenderer damageHealthObject;

    public AudioClip damageTakenSound;
    public AudioClip healSound;
    public AudioClip deathSound;

    private bool isDead;

    public float flashSpeed = 8f;
    private Coroutine damageFlash;
    private Coroutine alphaLerp;

    private Player player;
    private PlayerUIHandler playerUIHandler;

    private AudioSource audioSource;

    //Field that stores the index of the last player to do damage to this player.
    private int lastDamagedByPlayerNetId = -1;
    public int LastDamagedByPlayerNetId {
        get {
            return lastDamagedByPlayerNetId;
        }
    }

    /// <summary>
    /// Initializes this object.
    /// </summary>
    /// <param name="pl">Reference to the associated player.</param>
    public void Initialize(Player pl) {
        player = pl;

        currentHealth = maxHealth;
        SetDamageColor();
        audioSource = GetComponent<AudioSource>();

        if(pl.NetworkPlayerInstance.isLocalPlayer) {
            playerUIHandler = GameManager.Instance.playerUIHandler;
            playerUIHandler.SetCurrentHealth(currentHealth, maxHealth);
        }
    }

    /// <summary>
    /// Set the default/initial state of this object.
    /// </summary>
    public void SetDefaults() {
        isDead = false;
        currentHealth = maxHealth;
        SetDamageColor();
        SetPlayerActive(true);
    }

    /// <summary>
    /// Command called when a DockingKit changes the maxHealth.
    /// </summary>
    /// <param name="newMaxHealth"></param>
    [Command]
    public void CmdSetMaxHealth(float newMaxHealth) {
        float healthRatio = currentHealth / maxHealth;

        maxHealth = newMaxHealth;
        currentHealth = maxHealth * healthRatio;
    }

    /// <summary>
    /// Command called when the player receives damageMultiplier change, multiplicative.
    /// </summary>
    /// <param name="multiplier">change to multiplier</param>
    [Command]
    public void CmdSetDamageMultiplier(float multiplier) {
        damageMultiplier *= multiplier;
    }

    /// <summary>
    /// SyncVar hook for when the health changes.
    /// </summary>
    /// <param name="newHealth">The new health.</param>
    private void OnHealthChange(float newHealth) {
        currentHealth = newHealth;

        if(hasAuthority && playerUIHandler != null) {
            playerUIHandler.SetCurrentHealth(currentHealth, maxHealth);
        }
    }

    /// <summary>
    /// SyncVar hook for when the max health changes.
    /// </summary>
    /// <param name="newHealth">The new max health.</param>
    private void OnMaxHealthChange(float newMaxHealth) {
        maxHealth = newMaxHealth;

        if(hasAuthority && playerUIHandler != null) {
            playerUIHandler.SetCurrentHealth(currentHealth, maxHealth);
        }
    }
    
    /// <summary>
    /// ClientRpc called whenever the player takes damage.
    /// </summary>
    [ClientRpc]
    private void RpcOnDamage() {
        if(damageFlash != null) {
            StopCoroutine(damageFlash);
        }
        damageFlash = StartCoroutine(DamageColorFlash());
        audioSource.PlayOneShot(damageTakenSound);
    }

    /// <summary>
    /// Server call that happens when a player dies.
    /// </summary>
    [Server]
    private void OnDeath() {
        isDead = true;
        currentHealth = 0f;
        
        GetComponent<PlayerStatus>().RemoveAllModifiers();
        GetComponent<Docking>().RemoveDockingKit(true);

        RpcOnDeath();
        Debug.Log("RIP");
        GameManager.Instance.ModeProcessor.PlayerDies(player);
    }

    /// <summary>
    /// Rpc for synchronizing player death.
    /// </summary>
    [ClientRpc]
    private void RpcOnDeath() {
        SetPlayerActive(false);
        audioSource.PlayOneShot(deathSound);
    }

    /// <summary>
    /// ServerCallback called when the player takes damage.
    /// </summary>
    /// <param name="damage">The amount of damage taken.</param>
    /// <param name="playerNetId">The player doing the damage.</param>
    [Server]
    public void TakeDamage(float damage, uint playerNetId) {
        if(isDead) {
            return;
        }
        lastDamagedByPlayerNetId = (int)playerNetId;
        TakeDamage(damage);
    }

    /// <summary>
    /// ServerCallback called when the player takes damage.
    /// </summary>
    /// <param name="damage">The amount of damage taken.</param>
    [Server]
    public void TakeDamage(float damage) {
        if(isDead) {
            return;
        }

        currentHealth -= damage * damageMultiplier;
        RpcOnDamage();

        if(currentHealth <= 0f) {
            OnDeath();
        }
    }

    /// <summary>
    /// Rpc that gets called whenever the player is healed
    /// </summary>
    [ClientRpc]
    private void RpcOnHeal() {
        if(player.NetworkPlayerInstance.isLocalPlayer) {
            audioSource.PlayOneShot(healSound);
        }
    }

    /// <summary>
    /// ServerCallback called when the player receives health.
    /// </summary>
    /// <param name="healing">The amount of health received.</param>
    [Server]
    public void Heal(float healing) {
        if(isDead) {
            return;
        }

        currentHealth = Mathf.Min(currentHealth + healing, maxHealth);
        RpcOnHeal();
    }

    /// <summary>
    /// Set the active state of the player GameObject.
    /// </summary>
    /// <param name="active">GameObject active state.</param>
    private void SetPlayerActive(bool active) {
        //gameObject.SetActive(active);
        damageHealthObject.enabled = active;
        player.SetPlayerActive(active);
    }

    /// <summary>
    /// Sets the damage color object based on the health to maxhealth ratio.
    /// </summary>
    private void SetDamageColor() {
        damageHealthObject.color = Color.Lerp(Color.red, Color.green, (currentHealth / maxHealth));
    }

    /// <summary>
    /// Used whenever the player takes damage. Lerps alpha to 0, sets damage color, lerps alpha back to 1.
    /// </summary>
    private IEnumerator DamageColorFlash() {
        if(alphaLerp != null) {
            StopCoroutine(alphaLerp);
        }
        alphaLerp = StartCoroutine(AlphaLerp(0f));
        yield return alphaLerp;

        SetDamageColor();

        if(alphaLerp != null) {
            StopCoroutine(alphaLerp);
        }
        alphaLerp = StartCoroutine(AlphaLerp(1f));
        yield return alphaLerp;
    }

    /// <summary>
    /// Lerps the damageHealthObject alpha from current to targetAlpha.
    /// </summary>
    /// <param name="targetAlpha">What the alpha will lerp to.</param>
    private IEnumerator AlphaLerp(float targetAlpha) {
        Color damageColor = damageHealthObject.color;

        while(Mathf.Abs(targetAlpha - damageColor.a) > 0.1f) {
            damageColor.a = Mathf.Lerp(damageColor.a, targetAlpha, flashSpeed * Time.deltaTime);
            damageHealthObject.color = damageColor;
            yield return null;
        }

        damageColor.a = targetAlpha;
        damageHealthObject.color = damageColor;
    }
}
