﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Handles the DockingKit interactions for each Player.
/// </summary>
public class Docking : NetworkBehaviour {
    public GameObject dockingKitPickupPrefab;
    public DockingKit basicDockingKit;

    public float dockingTime = 2f;

    private GameManager gameManager;
    private PlayerInput playerInput;
    private PlayerHealth playerHealth;
    private PlayerStatus playerStatus;
    private Player player;

    [SyncVar(hook = "OnDockingKitIdChange")]
    public DockingKitId dockingKitId = DockingKitId.Empty;
    private DockingKit dockingKit;

    private DockingKitPickup hoverDockingKit;

    private bool switchingKit = false;

    /// <summary>
    /// Called when this object is activated on a client. Sets up the initial state and references.
    /// </summary>
    /*public override void OnStartClient() {
        gameManager = GameManager.Instance;
        playerStatus = GetComponent<PlayerStatus>();
        player = GetComponent<Player>();

        basicDockingKit.Initialize(this);
        dockingKit = basicDockingKit;

        //Sets docking kits for players already on the server.
        if(!isServer) {
            OnDockingKitIdChange(dockingKitId);
        }
    }

    /// <summary>
    /// Called when the local player object has been set up. Sets up local references.
    /// </summary>
    public override void OnStartLocalPlayer() {
        playerInput = GetComponent<PlayerInput>();
        playerHealth = GetComponent<PlayerHealth>();
        dockingKit.OnLocalPlayerInitialization(gameManager.playerUIHandler);
        gameManager.playerUIHandler.SetDockingKitUI(dockingKit);
    }*/

    public void Initialize(Player pl) {
        gameManager = GameManager.Instance;
        playerStatus = GetComponent<PlayerStatus>();
        player = pl;

        basicDockingKit.Initialize(this);
        dockingKit = basicDockingKit;

        if(pl.NetworkPlayerInstance.hasAuthority) {
            playerInput = GetComponent<PlayerInput>();
            playerHealth = GetComponent<PlayerHealth>();
            dockingKit.OnLocalPlayerInitialization(gameManager.playerUIHandler);
            gameManager.playerUIHandler.SetDockingKitUI(dockingKit);
        } else {
            //Sets docking kits for players already on the server.
            if(!isServer) {
                OnDockingKitIdChange(dockingKitId);
            }
        }
    }

    public void SetDefaults() {
        //RemoveDockingKit();
    }

    public NetworkConnection GetConnectionToClient() {
        return player.NetworkPlayerInstance.connectionToClient;
    }

    /// <summary>
    /// Check if the other player is damagable by this player. Unassigned team id means teams aren't used.
    /// </summary>
    /// <param name="otherPlayer">The other player.</param>
    /// <returns>True if damagable, false otherwise.</returns>
    public bool CheckDamagable(Player otherPlayer) {
        return (netId != otherPlayer.netId && (player.GetPlayerTeamId() == TeamId.Unassigned || player.GetPlayerTeamId() != otherPlayer.GetPlayerTeamId()));
    }

#if UNITY_EDITOR 
    //TEMP Used for testing.
    void Update() {
        if(hasAuthority) {
            if(Input.GetKeyDown(KeyCode.C)) {
                dockingKit.CancelAbilities();
            }

            if(Input.GetKeyDown(KeyCode.Alpha1)) {
                CmdSetDockingKitId(DockingKitId.Marksman);
            } else if(Input.GetKeyDown(KeyCode.Alpha2)) {
                CmdSetDockingKitId(DockingKitId.Test);
            } else if (Input.GetKeyDown(KeyCode.Alpha3)) {
                CmdSetDockingKitId(DockingKitId.Brawler);
            } else if (Input.GetKeyDown(KeyCode.Alpha4)) {
                CmdSetDockingKitId(DockingKitId.Bomber);
            } else if(Input.GetKeyDown(KeyCode.Alpha5)) {
                CmdSetDockingKitId(DockingKitId.Trapper);
            } else if(Input.GetKeyDown(KeyCode.Alpha6)) {
                CmdSetDockingKitId(DockingKitId.Tank);
            } else if(Input.GetKeyDown(KeyCode.Alpha7)) {
                CmdSetDockingKitId(DockingKitId.Boomerang);
            } else if(Input.GetKeyDown(KeyCode.Alpha8)) {
                CmdSetDockingKitId(DockingKitId.Sniper);
            } else if(Input.GetKeyDown(KeyCode.Alpha9)) {
                CmdSetDockingKitId(DockingKitId.Support);
            }
        }
    }
#endif

    void OnTriggerEnter(Collider other) {
        if(!hasAuthority) {
            return;
        }

        DockingKitPickup pickup = other.GetComponent<DockingKitPickup>();
        if(pickup != null) {
            hoverDockingKit = pickup;
        }
    }

    void OnTriggerExit(Collider other) {
        if(!hasAuthority) {
            return;
        }

        DockingKitPickup pickup = other.GetComponent<DockingKitPickup>();
        if(pickup != null && pickup == hoverDockingKit) {
            hoverDockingKit = null;
        }
    }

    /// <summary>
    /// Command which sets the SyncVar "dockingKitId". This is synchronized to all clients.
    /// </summary>
    /// <param name="newKit">The new DockingKitId.</param>
    [Command]
    public void CmdSetDockingKitId(DockingKitId newKit) {
        dockingKitId = newKit;
    }

    /// <summary>
    /// Command for setting the state of the switchingKit member.
    /// switchingKit determines whether we want to spawn a pickup on undocking or not
    /// </summary>
    /// <param name="state">The new state of the bool</param>
    [Command]
    public void CmdSetSwitchState(bool state) {
        switchingKit = state;
        RpcSetSwitchState(state);
    }

    /// <summary>
    /// ClientRpc for synchronizing the switchingKit state
    /// </summary>
    /// <param name="state">The new state of the bool</param>
    [ClientRpc]
    public void RpcSetSwitchState(bool state) {
        switchingKit = state;
    }

    /// <summary>
    /// SyncVar hook for "dockingKitId". Updates the SyncVar locally and dockingKit locally.
    /// </summary>
    /// <param name="newKitId">The new DockingKitId.</param>
    private void OnDockingKitIdChange(DockingKitId newKitId) {
        if(dockingKit != null && dockingKit != basicDockingKit) {
            UndockKit();
        }

        switchingKit = false;
        dockingKitId = newKitId;

        SetDockingKit(dockingKitId);
    }

    /// <summary>
    /// Spawns in the DockingKit locally for the given new DockingKitId. Updates UI when called for local player.
    /// </summary>
    /// <param name="newKitId">The new DockingKitId.</param>
    public void SetDockingKit(DockingKitId newKitId) {
        if(newKitId != DockingKitId.Empty) {
            GameObject dkObj = Instantiate(gameManager.GetDockingKit(newKitId), Vector3.zero, Quaternion.identity) as GameObject;
            dkObj.transform.parent = transform;
            dkObj.transform.localPosition = Vector3.zero;
            dkObj.transform.localEulerAngles = Vector3.zero;

            dockingKit = dkObj.GetComponent<DockingKit>();
            dockingKit.Initialize(this);
        } else {
            dockingKit = basicDockingKit;
        }

        if(hasAuthority) {
            dockingKit.OnLocalPlayerDocking(dockingTime, gameManager.playerUIHandler);
            gameManager.playerUIHandler.SetDockingKitUI(dockingKit);
        }
    }

    /// <summary>
    /// Get the active DockingKit for this Docking.
    /// </summary>
    /// <returns>The current DockingKit.</returns>
    public DockingKit GetDockingKit() {
        return dockingKit;
    }

    /// <summary>
    /// Called before changing to a new DockingKitId. Undocks current DockingKit.
    /// </summary>
    private void UndockKit() {
        playerStatus.RemoveAllAbilityModifiers();
        dockingKit.OnUndocking(dockingTime, dockingKitId, !switchingKit);
        dockingKit = null;
    }

    /// <summary>
    /// Remove the current docking kit.
    /// </summary>
    /// <param name="spawnPickup">Whether to spawn a pickup</param>
    [Server]
    public void RemoveDockingKit(bool spawnPickup = false) {
        if(dockingKit != null && dockingKit != basicDockingKit && dockingKitId != DockingKitId.Empty) {
            playerStatus.RemoveAllAbilityModifiers();
            dockingKit.OnRemoveKit(spawnPickup ? dockingKitId : DockingKitId.Empty);
            dockingKit = null;
        }

        dockingKitId = DockingKitId.Empty;
    }

    /// <summary>
    /// Updates the stats given by the current DockingKit.
    /// </summary>
    /// <param name="kit">Which DockingKit to retrieve the stats from.</param>
    public void SetDockingKitStats(DockingKit kit) {
        playerInput.moveSpeed = kit.moveSpeed;
        playerInput.rotationSpeed = kit.rotationSpeed;

        playerHealth.CmdSetMaxHealth(kit.maxHealth);
    }

    /// <summary>
    /// Passes the parameters along to the PlayerInput if called by the local player.
    /// </summary>
    /// <param name="state">The new state of the input restriction.</param>
    /// <param name="inputTypes">The types to set restriction for.</param>
    public void SetPlayerInputRestriction(bool state, params InputType[] inputTypes) {
        playerInput.SetInputRestrictions(state, inputTypes);
    }

    /// <summary>
    /// Called when the dock button is pressed.
    /// </summary>
    public void OnDockingButtonDown() {
        if(hoverDockingKit != null) {
            CmdOnPlayerDocking(hoverDockingKit.gameObject);
        }
    }

    /// <summary>
    /// Command called when the local player wants to dock to a DockingKitPickup.
    /// </summary>
    /// <param name="pickup">Reference to the networked pickup object.</param>
    [Command]
    public void CmdOnPlayerDocking(GameObject pickup) {
        if(pickup != null) {
            DockingKitPickup kitPickup = pickup.GetComponent<DockingKitPickup>();
            if(kitPickup != null) {
                kitPickup.OnPlayerDocking(gameObject);
            }
        }
    }

    /// <summary>
    /// Called when the undock button is pressed.
    /// </summary>
    public void OnUndockingButtonDown() {
        if(dockingKitId != DockingKitId.Empty) {
            CmdSetDockingKitId(DockingKitId.Empty);
        }
    }

    /// <summary>
    /// Called when the ability button is initially pressed or released.
    /// </summary>
    /// <param name="abilityId">Index of the ability where the button state changed.</param>
    /// <param name="down">If this was the initial press.</param>
    public void OnAbilityButtonChange(int abilityId, bool down) {
        if(dockingKit != null && abilityId < dockingKit.abilities.Count) {
            dockingKit.OnAbilityButtonChange(abilityId, down);
        }
    }

    /// <summary>
    /// Cancels all the abilities in the current docking kit.
    /// </summary>
    public void CancelAbilities() {
        dockingKit.CancelAbilities();
    }

    /// <summary>
    /// Command for activating an ability. Synchronizes activation to all clients.
    /// </summary>
    /// <param name="abilityId">Index of the ability to activate.</param>
    /// <param name="state">If the ability should be activated or deactivated.</param>
    [Command]
    public void CmdSetActive(int abilityId, bool state) {
        RpcSetActive(abilityId, state);
        dockingKit.abilities[abilityId].SetActive(state);
    }

    /// <summary>
    /// ClientRpc for activating an ability. Runs locally on every client. Returns immediately for the local player, as the activation already happened locally.
    /// </summary>
    /// <param name="abilityId">Index of the ability to activate.</param>
    /// <param name="state">If the ability should be activated or deactivated.</param>
    [ClientRpc]
    public void RpcSetActive(int abilityId, bool state) {
        if(hasAuthority || isServer) {
            return;
        }
        //Debug.Log("SetActive remotely");
        dockingKit.abilities[abilityId].SetActive(state);
    }

    /// <summary>
    /// Command for spawning prefab objects. Used by the abilities.
    /// </summary>
    /// <param name="abilityId">Index of the ability calling the Command.</param>
    /// <param name="prefabId">Index of the prefab to spawn from the ability.</param>
    /// <param name="position">Position of the new object.</param>
    /// <param name="rotation">Orientation of the new object (in eulerAngles).</param>
    [Command]
    public void CmdSpawnObject(int abilityId, int prefabId, Vector3 position, Vector3 rotation) {
        ISpawnableProvider ability = dockingKit.abilities[abilityId] as ISpawnableProvider;
        if(ability != null) {
            SpawnableFactory.Instance.SpawnObject(ability.GetSpawnablePrefab(prefabId), position, rotation, netId.Value, player.GetPlayerTeamId());
        }
    }

    /// <summary>
    /// Command for spawning prefab objects. Used by the abilities. Returns a reference to the spawned GameObject to the client/ability that called the Command.
    /// </summary>
    /// <param name="abilityId">Index of the ability calling the Command.</param>
    /// <param name="prefabId">Index of the prefab to spawn from the ability.</param>
    /// <param name="position">Position of the new object.</param>
    /// <param name="rotation">Orientation of the new object (in eulerAngles).</param>
    [Command]
    public void CmdSpawnObjectReference(int abilityId, int prefabId, Vector3 position, Vector3 rotation) {
        ISpawnableReferenceProvider ability = dockingKit.abilities[abilityId] as ISpawnableReferenceProvider;
        if(ability != null) {
            SpawnableObject spawnObject = SpawnableFactory.Instance.SpawnObject(ability.GetSpawnablePrefab(prefabId), position, rotation, netId.Value, player.GetPlayerTeamId());
            TargetSetSpawnObjectReference(GetConnectionToClient(), spawnObject.gameObject, abilityId);
        }
    }

    /// <summary>
    /// TargetRpc for getting the reference to a spawned object.
    /// </summary>
    /// <param name="connection">Needed so TargetRpc finds the correct client.</param>
    /// <param name="spawnedObject">Reference to the GameObject spawned.</param>
    /// <param name="abilityId">The id of the ability that called the spawn command.</param>
    [TargetRpc]
    public void TargetSetSpawnObjectReference(NetworkConnection connection, GameObject spawnedObject, int abilityId) {
        ISpawnableReferenceProvider ability = dockingKit.abilities[abilityId] as ISpawnableReferenceProvider;
        if(ability != null) {
            ability.SetSpawnedObjectReference(spawnedObject);
        }
    }

    /// <summary>
    /// Command used to destroy objects by objects that don't have authority to Command themselves.
    /// </summary>
    /// <param name="destroyGameObject">The reference to the object to be destroyed.</param>
    [Command]
    public void CmdDestroyObject(GameObject destroyGameObject) {
        NetworkServer.Destroy(destroyGameObject);
    }

    /// <summary>
    /// Command for spawning docking kit pickup on undocking.
    /// </summary>
    /// <param name="kitId">Which docking kit to spawn.</param>
    [Command]
    public void CmdSpawnDockingKitPickup(DockingKitId kitId) {
        SpawnableFactory.Instance.SpawnDockingKitPickup(kitId, transform.position, transform.rotation);
    }

    /// <summary>
    /// Command called by abilities by the local player to apply or remove a modifier.
    /// </summary>
    /// <param name="abilityId">The id of the ability that applied the modifier.</param>
    /// <param name="modifierIndex">The index of the modifier.</param>
    /// <param name="apply">If the modifier should be applied or removed.</param>
    [Command]
    public void CmdSetModifier(int abilityId, int modifierId, bool apply) {
        IModifierProvider ability = dockingKit.abilities[abilityId] as IModifierProvider;
        if(ability != null) {
            if(apply) {
                playerStatus.ApplyModifier(ability.GetModifierInfo(modifierId), abilityId);
            } else {
                playerStatus.RemoveModifier(ability.GetModifierInfo(modifierId).modifier);
            }
        }
    }

    /// <summary>
    /// Called by Modifiers OnClient functions to change the state of the modifier on each client.
    /// </summary>
    /// <param name="abilityId">The id of the ability that applied the modifier.</param>
    /// <param name="state">The active state of the modifier.</param>
    public void SetModifier(int abilityId, bool state) {
        dockingKit.abilities[abilityId].SetModifier(state);
    }

    /// <summary>
    /// Command used by abilities to run code on the server, as they're not NetworkBehaviour (or has authority) to call commands.
    /// </summary>
    /// <param name="abilityId">The id of the ability calling the command.</param>
    /// <param name="functionId">The id of the function to be run on the server.</param>
    [Command]
    public void CmdServerCallback(int abilityId, int functionId) {
        IServerCallback ability = dockingKit.abilities[abilityId] as IServerCallback;
        if(ability != null) {
            ability.ServerCallback(functionId);
        }
    }

    [Command]
    public void CmdServerCallbackTwoVector3(int abilityId, int functionId, Vector3 firstVec3, Vector3 secondVec3) {
        IServerCallback<Vector3, Vector3> ability = dockingKit.abilities[abilityId] as IServerCallback<Vector3, Vector3>;
        if(ability != null) {
            ability.ServerCallback(functionId, firstVec3, secondVec3);
        }
    }

    [Command]
    public void CmdServerCallbackGameObject(int abilityId, int functionId, GameObject go) {
        IServerCallback<GameObject> ability = dockingKit.abilities[abilityId] as IServerCallback<GameObject>;
        if(ability != null) {
            ability.ServerCallback(functionId, go);
        }
    }

    [Command]
    public void CmdServerCallbackFloat(int abilityId, int functionId, float param) {
        IServerCallback<float> ability = dockingKit.abilities[abilityId] as IServerCallback<float>;
        if(ability != null) {
            ability.ServerCallback(functionId, param);
        }
    }

    [Command]
    public void CmdServerCallbackBool(int abilitiyId, int functionId, bool param) {
        IServerCallback<bool> ability = dockingKit.abilities[abilitiyId] as IServerCallback<bool>;
        if(ability != null) {
            ability.ServerCallback(functionId, param);
        }
    }

    [Command]
    public void CmdServerCallbackGameObjectFloat(int abilityId, int functionId, GameObject param1, float param2) {
        IServerCallback<GameObject, float> ability = dockingKit.abilities[abilityId] as IServerCallback<GameObject, float>;
        if(ability != null) {
            ability.ServerCallback(functionId, param1, param2);
        }
    }

    /// <summary>
    /// ClientRpc used by abilities to run code on every client, as they're not NetworkBehaviour (or has authority) to call client rpcs.
    /// </summary>
    /// <param name="abilityId">The id of the ability calling the rpc.</param>
    /// <param name="functionId">The id of the function to be run on every client.</param>
    [ClientRpc]
    public void RpcClientCallback(int abilityId, int functionId) {
        IClientCallback ability = dockingKit.abilities[abilityId] as IClientCallback;
        if(ability != null) {
            ability.ClientCallback(functionId);
        }
    }

    [ClientRpc]
    public void RpcClientCallbackVector3(int abilityId, int functionId, Vector3 firstVec3) {
        IClientCallback<Vector3> ability = dockingKit.abilities[abilityId] as IClientCallback<Vector3>;
        if(ability != null) {
            ability.ClientCallback(functionId, firstVec3);
        }
    }

    [ClientRpc]
    public void RpcClientCallbackTwoVector3(int abilityId, int functionId, Vector3 firstVec3, Vector3 secondVec3) {
        IClientCallback<Vector3, Vector3> ability = dockingKit.abilities[abilityId] as IClientCallback<Vector3, Vector3>;
        if(ability != null) {
            ability.ClientCallback(functionId, firstVec3, secondVec3);
        }
    }
    
    [ClientRpc]
    public void RpcClientCallbackGameObject(int abilityId, int functionId, GameObject go) {
        IClientCallback<GameObject> ability = dockingKit.abilities[abilityId] as IClientCallback<GameObject>;
        if(ability != null) {
            ability.ClientCallback(functionId, go);
        }
    }

    [ClientRpc]
    public void RpcClientCallbackFloat(int abilityId, int functionId, float param) {
        IClientCallback<float> ability = dockingKit.abilities[abilityId] as IClientCallback<float>;
        if(ability != null) {
            ability.ClientCallback(functionId, param);
        }
    }

    [ClientRpc]
    public void RpcClientCallbackBool(int abilityId, int functionId, bool param) {
        IClientCallback<bool> ability = dockingKit.abilities[abilityId] as IClientCallback<bool>;
        if (ability != null) {
            ability.ClientCallback(functionId, param);
        }
    }

    /// <summary>
    /// TargetRpc used by abilities to run code on a target client, as they're not NetworkBehaviour (or has authority) to call target rpcs.
    /// </summary>
    /// <param name="connection">Needed so TargetRpc finds the correct client.</param>
    /// <param name="abilityId">The id of the ability calling the target rpc.</param>
    /// <param name="functionId">The id of the function to be run on the targeted client.</param>
    [TargetRpc]
    public void TargetClientCallback(NetworkConnection connection, int abilityId, int functionId) {
        ITargetClientCallback ability = dockingKit.abilities[abilityId] as ITargetClientCallback;
        if(ability != null) {
            ability.TargetClientCallback(functionId);
        }
    }

    /// <summary>
    /// TargetRpc for reducing the cooldown an ability by a certain amount.
    /// </summary>
    /// <param name="connection">The NetworkConnection associated with the player given the reduction.</param>
    /// <param name="abilityId">The id of the ability to get cooldown reduction.</param>
    /// <param name="reductionAmount">The amount deducted for the current cooldown.</param>
    [TargetRpc]
    public void TargetReduceCooldown(NetworkConnection connection, int abilityId, float reductionAmount) {
        dockingKit.abilities[abilityId].ReduceCooldown(reductionAmount);
    }

    [Command]
    public void CmdSetElement(ElementalContainer.ComboableElements element, int abilityId) {
        RpcSetElement(element, abilityId);
        dockingKit.abilities[abilityId].SetElement(element);
    }

    [ClientRpc]
    public void RpcSetElement(ElementalContainer.ComboableElements element, int abilityId) {
        dockingKit.abilities[abilityId].SetElement(element);
    }
}
