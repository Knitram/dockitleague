﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Game mode rules processor for the team deathmatch game mode
/// </summary>
public class TeamDeathmatch : GameModeProcessor {
    [SerializeField]
    private int roundsToWin = 5;

    private Team redTeam;
    private Team blueTeam;

    private Team roundWinner;

    private Team winningTeam;

    /// <summary>
    /// Gets the score target.
    /// </summary>
    /// <value>The score target.</value>
    public override int ScoreWinTarget {
        get { return roundsToWin; }
    }

    /// <summary>
    /// Function called on game start
    /// </summary>
    public override void StartGame() {
        redTeam = new Team(TeamId.Red);
        blueTeam = new Team(TeamId.Blue);

        Debug.Log("StartGame");
    }

    /// <summary>
    /// Function called on round start
    /// </summary>
    public override void StartRound() {
        redTeam.Reset(GameManager.Players);
        blueTeam.Reset(GameManager.Players);

        Debug.Log("StartRound");
        RegenerateHudScoreList();
    }

    /// <summary>
    /// Handles the death of a player - the player is removed from the local list
    /// </summary>
    /// <param name="player">Player.</param>
    public override void PlayerDies(Player player) {
        base.PlayerDies(player);

        if(player.GetPlayerTeamId() == TeamId.Red) {
            redTeam.PlayerDies(player);
        } else if(player.GetPlayerTeamId() == TeamId.Blue) {
            blueTeam.PlayerDies(player);
        } else {
            Debug.LogError("ERROR: Player with unassigned teamId killed.");
        }

        Debug.Log("PlayerDies: " + player.PlayerName);
    }

    /// <summary>
    /// Called when a player disconnects - removed from the local list
    /// </summary>
    /// <param name="player">The player that disconnects</param>
    public override void PlayerDisconnected(Player player) {
        base.PlayerDisconnected(player);


        if(player.GetPlayerTeamId() == TeamId.Red) {
            redTeam.PlayerDisconnected(player);
        } else if(player.GetPlayerTeamId() == TeamId.Blue) {
            blueTeam.PlayerDisconnected(player);
        } else {
            Debug.LogWarning("Warning: Player with unassigned teamId disconnected.");
        }
    }

    /// <summary>
    /// Determines whether it is end of round - if a team has 0 alive
    /// </summary>
    /// <returns>true</returns>
    /// <c>false</c>
    public override bool IsEndOfRound() {
        return !redTeam.IsTeamAlive() || !blueTeam.IsTeamAlive();
    }

    /// <summary>
    /// Handles the round end.
    /// </summary>
    public override void HandleRoundEnd() {
        // Clear the winner from the previous round.
        roundWinner = null;
        Debug.Log("RoundEnd");
        // See if there is a winner now the round is over.
        roundWinner = GetRoundWinner();

        // If there is a winner, increment their score.
        if(roundWinner != null) {
            roundWinner.IncrementScore();

            if(roundWinner.GetScore() >= roundsToWin) {
                winningTeam = roundWinner;
                isMatchOver = true;
            }
        }
        /*
        if(!isMatchOver) {
            gameManager.ServerResetAllPlayers();
        }*/
    }

    /// <summary>
    /// Gets the round end text - winner or draw if appropriate
    /// </summary>
    /// <returns>The round end text.</returns>
    public override string GetRoundEndText() {
        string message = "DRAW!";
        if(roundWinner != null) {
            message = string.Format("{0} team wins the round!", roundWinner.GetTeamName());
        }

        return message;
    }

    /// <summary>
    /// Gets the game over text - winner or draw if appropriate
    /// </summary>
    /// <returns>The game over end text.</returns>
    public override string GetGameOverText() {
        string message = "DRAW!";
        if(winningTeam != null) {
            message = string.Format("{0} team wins the game!", winningTeam.GetTeamName());
        }

        return message;
    }


    // This function is to find out if there is a winner of the round.
    private Team GetRoundWinner() {
        if(redTeam.IsTeamAlive()) {
            return redTeam;
        }

        return blueTeam;
    }
}
