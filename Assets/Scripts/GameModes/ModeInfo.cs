﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ModeInfo {
    [SerializeField]
    private string modeName;
    [SerializeField]
    private string modeAbbreviation;
    [SerializeField]
    private string modeDescription;

    [SerializeField]
    private GameModeProcessor modeProcessor;

    [SerializeField]
    private bool teamMode;

    [SerializeField]
    private int minPlayers;

    public int Index {
        get;
        set;
    }

    public ModeInfo(string name, string description) {
        modeName = name;
        modeDescription = description;
    }

    public ModeInfo(string name, string description, GameModeProcessor processor) {
        modeName = name;
        modeDescription = description;
        modeProcessor = processor;
    }

    public string GetModeName() {
        return modeName;
    }

    public string GetAbbreviation() {
        return modeAbbreviation;
    }

    public string GetDescription() {
        return modeDescription;
    }

    public GameModeProcessor GetModeProcessor() {
        return modeProcessor;
    }

    public bool IsTeamMode() {
        return teamMode;
    }

    public int GetMinimumPlayers() {
        return minPlayers;
    }
}
