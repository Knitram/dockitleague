﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Game mode rules processor for the deathmatch game mode
/// </summary>
public class Deathmatch : GameModeProcessor {
    [SerializeField]
    private int roundsToWin = 5;

    private int deadPlayers = 0;

    private List<Player> players;

    private Player roundWinner;

    /// <summary>
    /// Gets the score target.
    /// </summary>
    /// <value>The score target.</value>
    public override int ScoreWinTarget {
        get { return roundsToWin; }
    }

    /// <summary>
    /// Function called on round start
    /// </summary>
    public override void StartRound() {
        deadPlayers = 0;
        players = new List<Player>(GameManager.Players);
        Debug.Log("StartRound");
        RegenerateHudScoreList();
    }

    /// <summary>
    /// Handles the death of a player - the player is removed from the local list
    /// </summary>
    /// <param name="player">Player.</param>
    public override void PlayerDies(Player player) {
        base.PlayerDies(player);
        players.Remove(player);
        deadPlayers++;
        Debug.Log("PlayerDies: " + player.PlayerName);
    }

    /// <summary>
    /// Called when a player disconnects - removed from the local list
    /// </summary>
    /// <param name="player">The player that disconnects</param>
    public override void PlayerDisconnected(Player player) {
        base.PlayerDisconnected(player);

        int index = players.IndexOf(player);
        if(index != -1) {
            players.RemoveAt(index);
        }

        RegenerateHudScoreList();
    }

    /// <summary>
    /// Determines whether it is end of round - if there is one or no players
    /// </summary>
    /// <returns>true</returns>
    /// <c>false</c>
    public override bool IsEndOfRound() {
        return deadPlayers >= GameManager.Players.Count - 1;
    }

    /// <summary>
    /// Handles the round end.
    /// </summary>
    public override void HandleRoundEnd() {
        // Clear the winner from the previous round.
        roundWinner = null;
        Debug.Log("RoundEnd");
        // See if there is a winner now the round is over.
        roundWinner = GetRoundWinner();

        // If there is a winner, increment their score.
        if(roundWinner != null) {
            roundWinner.IncrementScore();

            if(roundWinner.Score >= roundsToWin) {
                winner = roundWinner;
                isMatchOver = true;
            }
        }
        /*
        if(!isMatchOver) {
            gameManager.ServerResetAllPlayers();
        }*/
    }

    /// <summary>
    /// Gets the round end text - winner or draw if appropriate
    /// </summary>
    /// <returns>The round end text.</returns>
    public override string GetRoundEndText() {
        string message = "DRAW!";
        if(roundWinner != null) {
            message = string.Format("{0} wins the round!", players[0].PlayerName);
        }

        return message;
    }

    /// <summary>
    /// Gets the game over text - winner or draw if appropriate
    /// </summary>
    /// <returns>The game over text.</returns>
    public override string GetGameOverText() {
        string message = "DRAW!";
        if(winner != null) {
            message = string.Format("{0} wins the game!", winner.PlayerName);
        }

        return message;
    }


    // This function is to find out if there is a winner of the round.
    // This function is called with the assumption that 1 or fewer players are currently active.
    private Player GetRoundWinner() {
        if(players.Count == 0) {
            return null;
        }

        return players[0];
    }
}
