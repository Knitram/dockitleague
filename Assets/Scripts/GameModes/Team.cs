﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Team {
    private TeamId teamId;

    private List<Player> players;
    private int deadPlayers = 0;

    private int score;

    public Team(TeamId tId) {
        teamId = tId;
        players = new List<Player>();
    }

    public void Reset(List<Player> playerList) {
        players.Clear();

        foreach(Player pl in playerList) {
            if(pl.GetPlayerTeamId() == teamId) {
                players.Add(pl);
            }
        }
    }

    public void PlayerDies(Player player) {
        players.Remove(player);
        deadPlayers++;
    }

    public void PlayerDisconnected(Player player) {
        int index = players.IndexOf(player);
        if(index != -1) {
            players.RemoveAt(index);
        }
    }

    public bool IsTeamAlive() {
        return players.Count > 0;
    }

    public int GetScore() {
        return score;
    }

    public void IncrementScore() {
        score++;
    }

    public string GetTeamName() {
        return (teamId == TeamId.Red) ? "Red" : "Blue";
    }
}
