﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ModeList", menuName = "Data List/Mode")]
public class ModeList : ScriptableObject {
    [SerializeField]
    private List<ModeInfo> modes;

    //Indexer implementation.
    public ModeInfo this[int index] {
        get { return modes[index]; }
    }

    public int Count {
        get { return modes.Count; }
    }
}
