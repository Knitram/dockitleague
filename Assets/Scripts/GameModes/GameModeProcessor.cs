﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Game mode rules processor - a base class for all game modes.
/// </summary>
public class GameModeProcessor : MonoBehaviour {
    //The transition time from once the game ends until the game exits
    public static float endGameTransitionTime = 10f;

    [SerializeField]
    protected MenuPage returnPage;

    public MenuPage ReturnPage {
        get { return returnPage; }
    }

    protected GameManager gameManager;

    protected Player winner;

    protected bool isMatchOver = false;
    public bool IsMatchOver {
        get { return isMatchOver; }
    }

    //The color provider - this is used in the lobby to provide different color selection implementation based on the game mode (i.e. individual versus team-based modes)
    //protected IColorProvider m_ColorProvider;

    public virtual int ScoreWinTarget {
        get { return 0; }
    }

    public virtual bool HasWinner {
        get { return winner != null; }
    }

    /// <summary>
    /// Gets the round message.
    /// </summary>
    /// <returns>The round message.</returns>
    public virtual string GetRoundMessage() {
        return string.Empty;
    }

    /// <summary>
    /// Sets the game manager.
    /// </summary>
    /// <param name="gameManager">Game manager.</param>
    public void SetGameManager(GameManager gManager) {
        if(gameManager == null) {
            gameManager = gManager;
        }
    }

    /// <summary>
    /// Determines whether it is end of round.
    /// </summary>
    /// <returns><c>true</c> if is end of round; otherwise, <c>false</c>.</returns>
    public virtual bool IsEndOfRound() {
        return false;
    }

    /// <summary>
    /// Called on game start
    /// </summary>
    public virtual void StartGame() {
    }

    /// <summary>
    /// Called on round start
    /// </summary>
    public virtual void StartRound() {
    }

    /// <summary>
    /// Called on Match end
    /// </summary>
    public virtual void MatchEnd() {
    }

    /// <summary>
    /// Handles the death of a player
    /// </summary>
    /// <param name="player">Player.</param>
    public virtual void PlayerDies(Player player) {
        gameManager.HandleKill(player);
    }

    /// <summary>
    /// Handles the killer score - this differs per game mode
    /// </summary>
    /// <param name="killer">Player that did the killing</param>
    /// <param name="killed">Player that was killed</param>
    public virtual void HandleKillerScore(Player killer, Player killed) {
    }

    /// <summary>
    /// Handles the player's suicide - this differs per game mode
    /// </summary>
    /// <param name="killer">The player that kill themself</param>
    public virtual void HandleSuicide(Player killer) {
    }

    /// <summary>
    /// Called when a player disconnects
    /// </summary>
    /// <param name="player">The player that disconnects</param>
    public virtual void PlayerDisconnected(Player player) {
    }

    /// <summary>
    /// Handles the round end.
    /// </summary>
    public virtual void HandleRoundEnd() {
    }

    /// <summary>
    /// Gets the round end text.
    /// </summary>
    /// <returns>The round end text.</returns>
    public virtual string GetRoundEndText() {
        return string.Empty;
    }

    /// <summary>
    /// Gets the game over text.
    /// </summary>
    /// <returns>The game over text.</returns>
    public virtual string GetGameOverText() {
        return string.Empty;
    }

    /*
    /// <summary>
    /// Returns elements for constructing the leaderboard
    /// </summary>
    /// <returns>The leaderboard elements.</returns>
    public virtual List<LeaderboardElement> GetLeaderboardElements() {
        List<LeaderboardElement> leaderboardElements = new List<LeaderboardElement>();

        List<TankManager> matchTanks = GameManager.s_Tanks;
        int tankCount = matchTanks.Count;

        for(int i = 0; i < tankCount; ++i) {
            TankManager currentTank = matchTanks[i];
            LeaderboardElement leaderboardElement = new LeaderboardElement(currentTank.playerName, currentTank.playerColor, currentTank.score);
            leaderboardElements.Add(leaderboardElement);
        }

        leaderboardElements.Sort(LeaderboardSort);
        return leaderboardElements;
    }

    /// <summary>
    /// Used for sorting the leaderboard
    /// </summary>
    /// <returns>The sort.</returns>
    /// <param name="player1">Player1.</param>
    /// <param name="player2">Player2.</param>
    protected int LeaderboardSort(LeaderboardElement player1, LeaderboardElement player2) {
        return player2.score - player1.score;
    }

    /// <summary>
    /// Gets the color provider.
    /// </summary>
    /// <returns>The color provider.</returns>
    public IColorProvider GetColorProvider() {
        SetupColorProvider();
        return m_ColorProvider;
    }

    /// <summary>
    /// Setups the color provider.
    /// </summary>
    protected virtual void SetupColorProvider() {
        if(m_ColorProvider == null) {
            m_ColorProvider = new PlayerColorProvider();
        }
    }*/

    /// <summary>
    /// Handles bailing (i.e. leaving the game)
    /// </summary>
    public virtual void Bail() {
        gameManager.ExitGame(returnPage);
    }

    /// <summary>
    /// Handles the game being complete (including the transitions)
    /// </summary>
    public virtual void CompleteGame() {
        gameManager.ExitGame(returnPage);
    }


    //Generates two arrays of player colours and their game scores, and passes them to the GameManager to update client HUDs.
    //The GameManager has pre-sorted the tanks by score by the time this is called.
    public virtual void RegenerateHudScoreList() {
        if(gameManager == null) {
            return;
        }

        int totalPlayers = GameManager.Players.Count;

        TeamId[] teamList = new TeamId[totalPlayers];
        int[] scoreList = new int[totalPlayers];

        for(int i = 0; i < totalPlayers; i++) {
            teamList[i] = GameManager.Players[i].GetPlayerTeamId();
            scoreList[i] = GameManager.Players[i].Score;
        }

        //gameManager.UpdateHudScore(teamList, scoreList);
    }

}
