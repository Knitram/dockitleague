﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SpawnableFactory : Singleton<SpawnableFactory> {
    public GameObject dockingKitPickupPrefab;

    private List<SpawnableObject> spawnedObjects;
    private List<GameObject> dockingKitPickups;

    protected override void Awake() {
        base.Awake();
        spawnedObjects = new List<SpawnableObject>();
        dockingKitPickups = new List<GameObject>();
    }

    public SpawnableObject SpawnObject(GameObject obj, Vector3 position, Vector3 rotation, uint player, TeamId team) {
        SpawnableObject spawnObject = (Instantiate(obj, position, Quaternion.Euler(rotation)) as GameObject).GetComponent<SpawnableObject>();
        NetworkServer.Spawn(spawnObject.gameObject);

        spawnObject.SetOwner(player, team);

        spawnedObjects.Add(spawnObject);
        return spawnObject;
    }

    public void SpawnDockingKitPickup(DockingKitId kitId, Vector3 position, Quaternion rotation) {
        GameObject instObj = Instantiate(dockingKitPickupPrefab, position, rotation) as GameObject;
        NetworkServer.Spawn(instObj);

        instObj.GetComponent<DockingKitPickup>().dockingKitId = kitId;
        dockingKitPickups.Add(instObj);
    }

    public void SpawnableDestroyed(SpawnableObject spawnObject) {
        spawnedObjects.Remove(spawnObject);
    }

    public void CleanupSpawnableList() {
        spawnedObjects.ForEach(so => {
            if(so != null) {
                NetworkServer.Destroy(so.gameObject);
            }
        });
        spawnedObjects.Clear();
    }

    public void CleanupPickupList() {
        dockingKitPickups.ForEach(pu => {
            if(pu != null) {
                NetworkServer.Destroy(pu);
            }
        });
        dockingKitPickups.Clear();
    }
}
