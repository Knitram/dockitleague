﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(LineRenderer))]
public class LineRendererEditor : Editor  {
    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        LineRenderer lineRenderer = (LineRenderer)target;
        lineRenderer.sortingLayerID = DrawSortingLayersPopup(lineRenderer.sortingLayerID);
        lineRenderer.sortingOrder = EditorGUILayout.IntField("Order in Layer", lineRenderer.sortingOrder);
    }

    private int DrawSortingLayersPopup(int layerID) {
        SortingLayer[] layers = SortingLayer.layers;

        List<string> names = new List<string>();
        foreach(SortingLayer sl in layers) {
            names.Add(sl.name);
        }

        //Set to first layer if invalid. (E.g. if layer has been deleted)
        if(!SortingLayer.IsValid(layerID)) {
            layerID = layers[0].id;
        }

        int layerValue = SortingLayer.GetLayerValueFromID(layerID);
        int newLayerID = EditorGUILayout.Popup("Sorting Order", layerValue, names.ToArray());

        return layers[newLayerID].id;
    }
}
